/*******************************************************************
  uLan Communication - C interface library

  ul_netbase.c  - basic network and module constructions

  (C) Copyright 2002-2004 by Pavel Pisa - Originator

  The uLan C interface library can be used, copied and modified
  under next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <time.h>
#include <ul_lib/ulan.h>
#include <ul_gsa.h>
#include <ul_gsacust.h>
#include <ul_netbase.h>

#undef DEBUG

#define UL_DY_RQ_MAX 6

UL_ATTR_WEAK int ul_dy_cycle_subfn;

UL_ATTR_WEAK ul_sn_array_t ul_sn_array;

GSA_CUST_DEC_SCOPE(UL_ATTR_UNUSED static,
	ul_msg_filt_set_priv, ul_msg_filt_set_t, ul_msg_filt_t, int,
	filts, filt_cmd, gsa_cmp_int)

GSA_CUST_IMP(ul_msg_filt_set_priv, ul_msg_filt_set_t, ul_msg_filt_t, int,
	filts, filt_cmd, gsa_cmp_int, GSA_FAFTER)

GSA_CUST_DEC_SCOPE(UL_ATTR_UNUSED static,
	ul_net_mod_priv, ul_net_info_t, ul_mod_info_t, int,
	modules, mod_adr, gsa_cmp_int)

GSA_CUST_IMP(ul_net_mod_priv, ul_net_info_t, ul_mod_info_t, int,
	modules, mod_adr, gsa_cmp_int, GSA_FAFTER)

GSA_CUST_DEC_SCOPE(UL_ATTR_UNUSED static,
	ul_sn_mod_priv, ul_sn_array_t, ul_mod_info_t, unsigned long,
	modules, mod_sn, gsa_cmp_int)

GSA_CUST_IMP(ul_sn_mod_priv, ul_sn_array_t, ul_mod_info_t, unsigned long,
	modules, mod_sn, gsa_cmp_int, GSA_FAFTER)

int
ul_stat_dy_adr_asg(ul_net_stat_t *stat) {
  if (!stat) return -1;
  stat->dy_adr_asg_cnt++;
  time(&stat->dy_adr_asg_last);
  return 0;
}

int
ul_stat_st_adr_req(ul_net_stat_t *stat) {
  if (!stat) return -1;
  stat->st_adr_req_cnt++;
  time(&stat->st_adr_req_last);
  return 0;
}

int ul_net_new(ul_net_info_t **pnet, char *dev_name)
{
  ul_net_info_t *net;
  ul_fd_t ul_fd;
  *pnet=NULL;
 #ifdef UL_DEV_NAME
  if(!dev_name) dev_name=UL_DEV_NAME;
 #endif /*UL_DEV_NAME*/
  ul_fd=ul_open(dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    return -1;
  net=(ul_net_info_t*)malloc(sizeof(ul_net_info_t));
  memset(net,0,sizeof(ul_net_info_t));
  net->ul_fd=ul_fd;
  net->dev_name=strdup(dev_name);
  ul_net_mod_priv_init_array_field(net);
  ul_msg_filt_set_priv_init_array_field(&net->msg_filts);
  *pnet=net;
  return 0;
}

ul_mod_info_t*
ul_net_mod_find(ul_net_info_t *net, int adr)
{
  return ul_net_mod_priv_find_first(net,&adr);
}

ul_mod_info_t*
ul_net_mod_find_indx(ul_net_info_t *net, int adr, int *pindx)
{
  unsigned indx = ul_net_mod_priv_find_first_indx(net,&adr);
  *pindx = indx;
  return ul_net_mod_priv_at(net, indx);
}

ul_mod_info_t*
ul_net_mod_find_after_indx(ul_net_info_t *net, int adr, int *pindx)
{
  unsigned indx = ul_net_mod_priv_find_after_indx(net,&adr);
  *pindx = indx;
  return ul_net_mod_priv_at(net, indx);
}

ul_msg_filt_t*
ul_net_filt_find(ul_net_info_t *net, int cmd)
{
  return ul_msg_filt_set_priv_find_first(&net->msg_filts,&cmd);
}

ul_msg_filt_t*
ul_mod_filt_find(ul_mod_info_t *mod, int cmd)
{
  return ul_msg_filt_set_priv_find_first(&mod->msg_filts,&cmd);
}

int
ul_net_filt_to_drv(ul_net_info_t *net, int cmd, int adr)
{
  int res;
  ul_msginfo msginfo;
  int cmd_indx=cmd/8;
  uchar cmd_msk=1<<(cmd%8);
  if((cmd<0)||(cmd>0xff)) return -1;
  if(net->filts_in_drv[cmd_indx]&cmd_msk) return 0;
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.cmd=cmd;
  res=ul_addfilt(net->ul_fd,&msginfo);
  if(res>=0)
    net->filts_in_drv[cmd_indx]|=cmd_msk;
  return res;
}

int
ul_net_filt_test(ul_net_info_t *net, int cmd, int adr)
{
  return net->filts_in_drv[cmd/8]&(1<<(cmd%8));
}

int
ul_net_filt_add(ul_net_info_t *net, ul_msg_filt_t *filt)
{
  int res;
  filt->mod=NULL;
  filt->net=NULL;
  res=ul_msg_filt_set_priv_insert(&net->msg_filts,filt);
  if(res>=0) {
    filt->net=net;
    ul_net_filt_to_drv(net,filt->filt_cmd,0);
  }
  return res;
}

int
ul_mod_filt_add(ul_mod_info_t *mod, ul_msg_filt_t *filt)
{
  int res;
  filt->mod=NULL;
  filt->net=NULL;
  res=ul_msg_filt_set_priv_insert(&mod->msg_filts,filt);
  if(res>=0) {
    filt->mod=mod;
    if(mod->net)
      ul_net_filt_to_drv(mod->net,filt->filt_cmd,mod->mod_adr);
  }
  return res;
}

void
ul_filt_detach(ul_msg_filt_t *filt)
{
  if(filt->net) ul_msg_filt_set_priv_delete(&filt->net->msg_filts,filt);
  if(filt->mod) ul_msg_filt_set_priv_delete(&filt->mod->msg_filts,filt);
}

int
ul_net_mod_insert(ul_net_info_t *net, ul_mod_info_t *mod)
{
  int res;
  res=ul_net_mod_priv_insert(net,mod);
  if(res>=0)
    mod->net=net;
   else
    mod->net=NULL;
  return res;
}

int
ul_net_mod_delete(ul_net_info_t *net, ul_mod_info_t *mod)
{
  mod->net=NULL;
  return ul_net_mod_priv_delete(net,mod);
}

void
ul_net_mod_print(ul_net_info_t *net)
{
  int mindx;
  ul_mod_info_t *mod;
  for(mindx=0;(mod=ul_net_mod_priv_at(net,mindx))!=NULL;mindx++){
    printf("%02d# %02d: %04lX\n",mindx,mod->mod_adr,mod->mod_sn);
  }
}

int
ul_net_rec_dispatch(ul_net_info_t *net, ul_msg_buf_t *msg_buf, ul_mevent_t *mevent,
                    ul_msg_filt_set_t *filts, int cmd, int *frames)
{
  unsigned      findx;
  ul_msg_filt_t *filt;

  findx=ul_msg_filt_set_priv_find_first_indx(filts,&cmd);
  if((filt=ul_msg_filt_set_priv_at(filts,findx))==NULL)
    return 0;

  do{
    if(!mevent->ev_type) return 2;
    if(!(*frames)&&net)
      if((*frames=ul_msg_buf_rd_rest(msg_buf,net->ul_fd))<=0) return -1;
    filt->filt_fnc(filt,mevent,filt->context);
    if(filt==ul_msg_filt_set_priv_at(filts,findx)) findx++;
    filt=ul_msg_filt_set_priv_at(filts,findx);
    if(filt==NULL)
      return 1;
  }while(filt->filt_cmd==cmd);

  return 1;
}

int
ul_net_do_rec_msg(ul_net_info_t *net)
{
  ul_msg_buf_t  msg_buf;
  ul_mevent_t   mevent;
  int           frames=0;
  int           mod_adr;
  ul_mod_info_t *mod;
  int           mindx;
  int           ret=0;
  int           proc_fl=0;

  /* read message informations */
  ul_msg_buf_init(&msg_buf);

  if(ul_acceptmsg(net->ul_fd,&msg_buf.msginfo)<0){
    ul_msg_buf_destroy(&msg_buf);
    return -1;
  }

  mevent.u.msg.buf=&msg_buf;
 #ifdef UL_BFL_PROC
  if(msg_buf.msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))
    proc_fl=1;
 #endif
 #ifdef UL_BFL_M2IN
  if(msg_buf.msginfo.flg&(UL_BFL_M2IN|UL_BFL_FAIL))
    proc_fl=1;
 #endif
  if(proc_fl){
    mod_adr=msg_buf.msginfo.dadr;
    mevent.ev_type=msg_buf.msginfo.flg&UL_BFL_FAIL?
                        ULEV_MSG_ERR:ULEV_MSG_PROC;
    mevent.ev_info=0;

    if((msg_buf.msginfo.dadr==msg_buf.msginfo.sadr)&&
       (mevent.ev_type==ULEV_MSG_PROC))
           mevent.ev_type=ULEV_MSG_REC;
  }else{
    mod_adr=msg_buf.msginfo.sadr;
    mevent.ev_type=ULEV_MSG_REC;
    mevent.ev_info=0;
  }

  /* process module registered filters */

  if((mod=ul_net_mod_find_indx(net,mod_adr,&mindx))) do{
    if(!mevent.ev_type) break;
    if((ret=ul_net_rec_dispatch(net,&msg_buf,&mevent,&mod->msg_filts,
                                msg_buf.msginfo.cmd,&frames))<0)
      goto rd_error;

    if(msg_buf.msginfo.cmd!=0)
      if((ret=ul_net_rec_dispatch(net,&msg_buf,&mevent,&mod->msg_filts,0,&frames))<0)
        goto rd_error;

    if(mod==ul_net_mod_priv_at(net,mindx)) mindx++;
    if((mod=ul_net_mod_priv_at(net,mindx))==NULL) break;
  }while(mod->mod_adr==mod_adr);

  /* process network registered filters */

  if(mevent.ev_type){
    if((ret=ul_net_rec_dispatch(net,&msg_buf,&mevent,&net->msg_filts,
                                msg_buf.msginfo.cmd,&frames))<0)
      goto rd_error;
    if(msg_buf.msginfo.cmd!=0)
      if((ret=ul_net_rec_dispatch(net,&msg_buf,&mevent,&net->msg_filts,0,&frames))<0)
        goto rd_error;
  }

 rd_error:

  ul_msg_buf_destroy(&msg_buf);
  ul_freemsg(net->ul_fd);
  return ret;
}

int
ul_mod_desc_set(ul_mod_info_t *mod, void *des_new, int des_len)
{
  char *des, *mt, *p;
  int i;
  if(!des_new) des_len=0;
  else {
    if(!des_len) des_len=strlen(des_new);
    if(mod->mod_des){
      if(strlen(mod->mod_des)<=des_len)
        if(!strncmp(des_new,mod->mod_des,des_len))
          return 0;
    }
  }
  if(mod->mod_des) free(mod->mod_des);
  mod->mod_des=NULL;
  if(!des_new) return 0;
  if((des=malloc(des_len+1))==NULL) return -1;
  des[des_len]=0;
  memcpy(des,des_new,des_len);
  mod->mod_des=des;
  if(mod->mod_type) free(mod->mod_type);
  mod->mod_type=NULL;
  p=strstr(des,".mt ");
  if(!p) return 0;
  p+=4;
  for(i=0;p[i]>' ';i++);
  if((mt=malloc(i+1))==NULL) return -1;
  mt[i]=0;
  memcpy(mt,p,i);
  mod->mod_type=mt;
  /*printf("mod_type \"%s\"\n",mt);*/
  if(!(mod->flg&ULMI_DYADR)){
    /* check if module supporting dynamic addressing has been found */
    /* by manual request or by other mystery occasions */
    p=des;
    while(*p){
      p=strstr(p,".dy");
      if(!p) break;
      p+=3;
      if(!*p || *p==' '){
        mod->flg|=ULMI_DYADR;
	break;
      }
    }
  }
  return 1;
}

int
ul_net_desc_rec(struct ul_msg_filt *filt,struct ul_mevent* event, void *context)
{
  int adr, len;
  uchar *data;
  ul_net_info_t *net=filt->net;
  ul_mod_info_t *mod;
  switch(event->ev_type){
    case ULEV_MSG_PROC:
      if(!net) break;
      adr=ul_msg_event_dadr(event);
      if(!adr) break;
      len=ul_msg_event_tail_len(event);
      data=ul_msg_event_tail_data(event);
      if((!len)||(!data)) break;
      mod=ul_net_mod_find(net,adr);
      if(!mod){
        /* !!!!! */
        break;
      }
      ul_mod_desc_set(mod,data,len);
      break;
  }
  return 0;
}

int
ul_net_query_desc(ul_net_info_t *net, int adr, int mode)
{
  int ret=0;
  char c;
  if(!net) return -1;
  if(adr&&(mode&1)){
    if(!ul_net_filt_test(net,UL_CMD_SID,0)){
      if(!ul_net_filt_find(net,UL_CMD_SID)){
        ul_msg_filt_t *filt;
        /* initialize filters */
        filt=malloc(sizeof(ul_msg_filt_t));
        filt->filt_cmd=UL_CMD_SID;
        filt->filt_fnc=ul_net_desc_rec;
        if(ul_net_filt_add(net,filt)<0){
          free(filt);
          return -1;
        }
      }
    }
    ret=ul_send_query(net->ul_fd,adr,UL_CMD_SID,
          UL_BFL_NORE|UL_BFL_PRQ,NULL,0);
  }
  if(mode&2){
    c=ULNCS_SID_RQ;
    ret=ul_send_command(net->ul_fd,adr,UL_CMD_NCS,
                        UL_BFL_ARQ,&c,1);
  }
  return ret;
}

int
ul_mod_adr_set(ul_mod_info_t *mod, int new_adr)
{
  ul_net_info_t *net=mod->net;
  if(net)
    ul_net_mod_delete(mod->net,mod);
  mod->mod_adr=new_adr;
  if(net)
    return ul_net_mod_insert(net,mod);
  return 0;
}

int
ul_mod_net_set(ul_mod_info_t *mod, ul_net_info_t *net)
{
  if(mod->net)
    ul_net_mod_delete(mod->net,mod);
  if(net)
    return ul_net_mod_insert(net,mod);
  return 0;
}

int
ul_mod_sn_set(ul_mod_info_t *mod, unsigned long new_sn)
{
  if(mod->mod_sn)
    ul_sn_mod_priv_delete(&ul_sn_array,mod);
  mod->mod_sn=new_sn;
  if(new_sn)
    return ul_sn_mod_priv_insert(&ul_sn_array,mod);;
  return 0;
}

ul_mod_info_t *
ul_mod_sn_find(unsigned long mod_sn)
{
  return ul_sn_mod_priv_find(&ul_sn_array,&mod_sn);
}

int
ul_mod_new(ul_mod_info_t **pmod, ul_net_info_t *net,
           int mod_adr, unsigned long mod_sn, char *mod_des)
{
  ul_mod_info_t *mod;
  if(pmod) *pmod=NULL;
  mod=(ul_mod_info_t *)malloc(sizeof(ul_mod_info_t));
  if(!mod) return -1;
  memset(mod,0,sizeof(ul_mod_info_t));
  ul_dbuff_init(&mod->mod_st0_buf,0);
  ul_dbuff_init(&mod->mod_st1_buf,0);
  mod->mod_adr=mod_adr;
  mod->mod_sn=mod_sn;
  if(mod_des) mod->mod_des=strdup(mod_des);
  if(net) ul_net_mod_insert(net,mod);
  if(mod_sn) ul_sn_mod_priv_insert(&ul_sn_array,mod);
  if(pmod) *pmod=mod;
  ul_msg_filt_set_priv_init_array_field(&mod->msg_filts);
  return 1;
}

int
ul_mod_static_add(ul_net_info_t *net,int adr,unsigned long mod_sn)
{
  ul_mod_info_t *mod;
  if((mod=ul_mod_sn_find(mod_sn))==NULL){
    if(ul_mod_new(&mod,net,adr,mod_sn,NULL)<0) return -1;
    mod->flg=ULMI_STATIC;
  } else return -2;
  return 0;
}

void
ul_mod_free(ul_mod_info_t *mod)
{
  ul_msg_filt_t* filt;
  ul_mevent_t mevent;
  if(mod->mod_sn)
    ul_sn_mod_priv_delete(&ul_sn_array,mod);
  if(mod->net)
    ul_net_mod_priv_delete(mod->net,mod);
  ul_dbuff_destroy(&mod->mod_st0_buf);
  ul_dbuff_destroy(&mod->mod_st1_buf);
  /* notify all filters */
  while((filt=ul_msg_filt_set_priv_cut_last(&mod->msg_filts))!=NULL){
    mevent.ev_type=ULEV_MOD_FREE;
    mevent.u.mod.mod=mod;
    filt->filt_fnc(filt,&mevent,filt->context);
  }
  ul_msg_filt_set_priv_delete_all(&mod->msg_filts);
  if(mod->mod_des) free(mod->mod_des);
  if(mod->mod_type) free(mod->mod_type);
  free(mod);
}

int
ul_mod_static_remove(ul_net_info_t *net,unsigned long mod_sn)
{
  ul_mod_info_t *mod;
  if((mod=ul_mod_sn_find(mod_sn))!=NULL){
    ul_mod_free(mod);
  } else return -2;
  return 0;
}

inline void
ul_dy_sn2buf(uchar *buf,unsigned long mod_sn)
{
  buf[0]=mod_sn>>0;
  buf[1]=mod_sn>>8;
  buf[2]=mod_sn>>16;
  buf[3]=mod_sn>>24;
}

inline unsigned long
ul_dy_buf2sn(const uchar *buf)
{
  unsigned long l;
  l=(unsigned long)buf[0];
  l+=(unsigned long)buf[1]<<8;
  l+=(unsigned long)buf[2]<<16;
  l+=(unsigned long)buf[3]<<24;
  return l;
}

void
ul_dy_cycle_start(ul_net_info_t *net)
{
  ul_send_command(net->ul_fd,0,UL_CMD_GST,UL_BFL_NORE|UL_BFL_PRQ,
                  &ul_dy_cycle_subfn,1);
}

int
ul_dy_status_rq(ul_mod_info_t *mod, int subfn)
{
  uchar buf_out[8];
  ul_net_info_t *net=mod->net;
  if(!net) return -1;
  if(!subfn) subfn=0x10;
  buf_out[0]=subfn;
  ul_dy_sn2buf(&buf_out[1],mod->mod_sn);
  return ul_send_query(net->ul_fd,mod->mod_adr,UL_CMD_GST,
                         UL_BFL_NORE|UL_BFL_PRQ,buf_out,5);
}

void
ul_dy_gen_rq(ul_net_info_t *net)
{
  int adr;
  int mindx;
  ul_mod_info_t *mod;
  ul_dy_state_t *dy_state=net->dy_state;
  if(!dy_state) return;

  adr=dy_state->last_rq_adr;
  mod=ul_net_mod_find_after_indx(net,adr,&mindx);
  for(;mod!=NULL;mod=ul_net_mod_priv_at(net,++mindx)){
    if(dy_state->rq_max&&(dy_state->rq_count>=dy_state->rq_max))
      return;
    if(mod->mod_adr==adr) continue;
    adr=mod->mod_adr;
    if((!mod->mod_st0_cmd)&&(!mod->mod_st1_cmd)){
      if(!(mod->flg&ULMI_DYADR)) continue;
      mod->mod_st0_cmd=0x10;
    }
    dy_state->last_rq_adr=adr;
    if(mod->mod_st0_cmd) {
      /*printf("ul_dy_gen_rq: q st0:%2d c:%2d\n",adr,dy_state->rq_count);*/
      if(ul_dy_status_rq(mod,mod->mod_st0_cmd)>=0)
        dy_state->rq_count++;
      else {
        dy_state->last_rq_adr=adr-1;
        return;
      }
    }
    if(mod->mod_st1_cmd) {
      /*printf("ul_dy_gen_rq: q st1:%2d c:%2d\n",adr,dy_state->rq_count);*/
      if(ul_dy_status_rq(mod,mod->mod_st1_cmd)>=0)
        dy_state->rq_count++;
    }
  }
  dy_state->last_rq_adr=0;      /* all requests found */
}

void
ul_dy_dec_rq(ul_net_info_t *net)
{
  ul_dy_state_t *dy_state=net->dy_state;
  if(!dy_state) return;
  if(dy_state->rq_count>0) dy_state->rq_count--;
  /*printf("ul_dy_dec_rq:\n");*/
  if(!dy_state->last_rq_adr) return;
  if(dy_state->rq_max&&(dy_state->rq_count>=dy_state->rq_max))
    return;
  ul_dy_gen_rq(net);
}

int
ul_dy_set_adr(ul_mod_info_t *mod, int new_adr)
{ int ret;
  int old_adr=0;
  uchar buf_out[8];
  unsigned long mod_sn=mod->mod_sn;
  ul_net_info_t *net=mod->net;
  if(!net) return -1;
  if(mod_sn==0) old_adr=mod->mod_adr;
  buf_out[0]=ULNCS_SET_ADDR;    /* SN0 SN1 SN2 SN3 NEW_ADR */
  ul_dy_sn2buf(&buf_out[1],mod_sn);
  buf_out[5]=new_adr;
  ret=ul_send_command(net->ul_fd,old_adr,UL_CMD_NCS,
                      old_adr?UL_BFL_ARQ:0,buf_out,6);
  return ret;
}


void
ul_dy_do_timer(ul_htimer_fnc_data_t data) UL_ATTR_REENTRANT
{
  int mindx;
  int adr=0;
  int pri;
  int askdes_adr=0;
  int askdes_pri=0;
  ul_mod_info_t *askdes_mod=NULL;
  ul_dy_state_t *dy_state=(ul_dy_state_t*)data;
  ul_net_info_t *net=dy_state->net;
  ul_mod_info_t *mod;
  if(!net) return;
  if(!dy_state->last_rq_adr) {
    ul_dy_cycle_start(net);
  } else {
    dy_state->flg|=ULDY_CYOVR;
  }
  ul_dy_gen_rq(net);
  /* send deffered requests for module description */
  for(mindx=0;(mod=ul_net_mod_priv_at(net,mindx))!=NULL;mindx++){
    if(mod->mod_adr==adr) continue;
    adr=mod->mod_adr;
    if((pri=mod->flg&ULMI_ASKDES)>askdes_pri){
      if(mod->mod_des){
        mod->flg&=~ULMI_ASKDES;
      }else{
        askdes_pri=pri;
        askdes_adr=adr;
          askdes_mod=mod;
      }
    }
  }
  if(askdes_adr){
    askdes_mod->flg-=ULMI_ASKDES_MIN;
    ul_net_query_desc(net,askdes_adr,3);
  }
  {
    ul_htim_time_t next_expire;
    ul_htim_time_t actual_time;

    next_expire=ul_htimer_get_expire(&dy_state->timer);
    next_expire+=dy_state->period;

    ul_root_htimer_current_time(0, &actual_time);
    if((ul_htim_diff_t)(next_expire-actual_time)<=0)
      next_expire=actual_time+1;
    ul_htimer_set_expire(&dy_state->timer,next_expire);
  }
  ul_root_htimer_add(&dy_state->timer);
}

int
ul_dy_ncs_adr_rq(ul_net_info_t *net, int *padr,
                 unsigned long mod_sn, int first_dy_adr)
{
  int new_adr;
  int mindx;
  int adr=*padr;
  ul_mod_info_t *mod, *mod_sa;
  if((!mod_sn)||(mod_sn==0xffffffff)) return -1;
  if((mod=ul_mod_sn_find(mod_sn))==NULL){
    if(ul_mod_new(&mod,net,adr,mod_sn,NULL)<0) return -1;
  }
  mod_sa=ul_net_mod_find(net,adr);
  if(!mod->mod_des){
    mod->flg|=ULMI_ASKDES;
  }
  if(mod_sa==mod){
    if((adr>0)&&(adr<99)){
      ul_dy_set_adr(mod,adr);
      mod->flg|=ULMI_DYADR;
      return 1;
    }
  }
 #ifdef DEBUG
  printf("\nul_dy_ncs_adr_rq: before readdress\n");
  ul_net_mod_print(net);
 #endif /*DEBUG*/
  if((mod->flg&ULMI_STATIC)&&(mod->net==net)){
    new_adr=mod->mod_adr;
    ul_dy_set_adr(mod,new_adr);
    mod->flg|=ULMI_DYADR;
   #ifdef DEBUG
    printf("\nul_dy_ncs_adr_rq: static assigment address:%d\n",mod->mod_adr);
    ul_net_mod_print(net);
   #endif /*DEBUG*/
    *padr=new_adr;
    ul_stat_st_adr_req(net->statistics);
    return 1;
  }
  if(mod->net)
    ul_net_mod_delete(mod->net,mod);
 #ifdef DEBUG
  printf("\nul_dy_ncs_adr_rq: after delete\n");
  ul_net_mod_print(net);
 #endif /*DEBUG*/
  if(mod->flg&ULMI_STATIC){
   #ifdef DEBUG
    printf("\nul_dy_ncs_adr_rq: module with STATIC flag has moved between networks\n");
   #endif /*DEBUG*/
    mod->flg&=~ULMI_STATIC;
  }
  /* resolve free address on net for module */
  if((mod_sa==NULL)&&(adr>0)&&(adr<99)) {
    new_adr=adr;
  }else{
    new_adr=first_dy_adr;
    ul_net_mod_find_after_indx(net,new_adr-1,&mindx);
   #ifdef DEBUG
    printf("ul_dy_ncs_adr_rq: mindx=%d modules=%d\n",mindx,net->modules.count);
   #endif /*DEBUG*/
    while(mindx<net->modules.count){
     #ifdef DEBUG
      printf("ul_dy_ncs_adr_rq: mindx=%d adr=%d\n",mindx,
                        ul_net_mod_priv_at(net,mindx)->mod_adr);
     #endif /*DEBUG*/
      if(new_adr<ul_net_mod_priv_at(net,mindx)->mod_adr) break;
      if(new_adr==ul_net_mod_priv_at(net,mindx)->mod_adr) {
        if(++new_adr>=99) { new_adr=0; break; }
      }
      mindx++;
    }
  }
 #ifdef DEBUG
  printf("ul_dy_ncs_adr_rq: mod_sn=%06lX new_adr=%02d\n",mod_sn, new_adr);
 #endif /*DEBUG*/
  ul_mod_adr_set(mod,new_adr);
  ul_net_mod_insert(net,mod);
  ul_dy_set_adr(mod,new_adr);
  mod->flg|=ULMI_DYADR;
 #ifdef DEBUG
  printf("\nul_dy_ncs_adr_rq: after readdress\n");
  ul_net_mod_print(net);
 #endif /*DEBUG*/
  *padr=new_adr;
  ul_stat_dy_adr_asg(net->statistics);
  return new_adr?1:-1;
}

int
ul_dy_ncs_rec(struct ul_msg_filt *filt,struct ul_mevent* event, void *context) UL_ATTR_REENTRANT
{
  int adr, subfn, len;
  uchar *data;
  /* ul_mod_info_t *mod; */
  unsigned long mod_sn;
  ul_dy_state_t *dy_state=(ul_dy_state_t*)context;
  ul_net_info_t *net=dy_state->net;
  if(!net) return 0;
  switch(event->ev_type){
    case ULEV_MSG_REC:
      adr=ul_msg_event_sadr(event);
      len=ul_msg_event_len(event);
      data=ul_msg_event_data(event);
      if((len<5)||(!data)) break;
      subfn=data[0];
      /*printf("ul_dy_ncs_rec: rec:%2d c:%2d\n",adr,dy_state->rq_count);        */
      mod_sn=ul_dy_buf2sn(data+1);
      switch(subfn){
        case ULNCS_ADR_RQ:
          if(ul_dy_ncs_adr_rq(net,&adr,mod_sn,
             dy_state->first_dy_adr)<0) break;
          ul_net_query_desc(net,adr,3);
          break;
      }
      break;
  }
  return 0;
}

int
ul_dy_gst_proc(struct ul_msg_filt *filt,struct ul_mevent* event, void *context) UL_ATTR_REENTRANT
{
  int adr, subfn;
  int len, tail_len;
  uchar *data, *tail_data;
  ul_mod_info_t *mod;
  unsigned long mod_sn;
  ul_dy_state_t *dy_state=(ul_dy_state_t*)context;
  ul_net_info_t *net=dy_state->net;
  switch(event->ev_type){
    case ULEV_MSG_PROC:
      ul_dy_dec_rq(net);
      adr=ul_msg_event_dadr(event);
      len=ul_msg_event_len(event);
      data=ul_msg_event_data(event);
      tail_len=ul_msg_event_tail_len(event);
      tail_data=ul_msg_event_tail_data(event);
      mod=ul_net_mod_find(net,adr);
      if(!mod) break;
      if((len<5)||(!data)) break;
      subfn=data[0];
      if((tail_len<4)||(!tail_data)) break;
      mod_sn=ul_dy_buf2sn(tail_data);
      if(mod_sn!=mod->mod_sn) break;
      /* !!! mark module Present !!! */

      tail_len-=4; tail_data+=4;
      if(subfn==mod->mod_st0_cmd) {
        ul_htim_time_t actual_time;
        if(ul_dbuff_prep(&mod->mod_st0_buf,tail_len)<tail_len) break;
        memcpy(mod->mod_st0_buf.data,tail_data,tail_len);
        ul_root_htimer_current_time(0, &actual_time);
        ul_htime2mstime(&mod->mod_st0_mstime, &actual_time);
        if(mod->mod_des) mod->flg|=ULMI_PRESENT;
        mod->st_err_cnt=0;
      } else if(subfn==mod->mod_st1_cmd) {
        ul_htim_time_t actual_time;
        if(ul_dbuff_prep(&mod->mod_st1_buf,tail_len)<tail_len) break;
        memcpy(mod->mod_st1_buf.data,tail_data,tail_len);
        ul_root_htimer_current_time(0, &actual_time);
        ul_htime2mstime(&mod->mod_st1_mstime, &actual_time);
      }
      break;

    case ULEV_MSG_ERR:
      ul_dy_dec_rq(net);
      adr=ul_msg_event_dadr(event);
      len=ul_msg_event_len(event);
      data=ul_msg_event_data(event);
      mod=ul_net_mod_find(net,adr);
      if(!mod) break;
      if(mod->st_err_cnt>50) break;
      mod->st_err_cnt++;
      if((mod->st_err_cnt>=4)&&(mod->flg&ULMI_PRESENT)){
        mod->flg&=~ULMI_PRESENT;
        mod->flg&=~ULMI_DISCON;
	/* !!! notify app !!! */
      }
      if(mod->st_err_cnt>=10){
        if(mod->flg&ULMI_DYADR){
          ul_net_mod_delete(net,mod);
        }
      }
      break;
  }

  return 0;
}


int ul_net_dy_init(ul_net_info_t *net)
{
  ul_dy_state_t *dy_state;
  ul_net_stat_t *stat;

  if(net->dy_state) return -1;

  /* dy state */
  dy_state=malloc(sizeof(ul_dy_state_t));
  memset(dy_state,0,sizeof(ul_dy_state_t));
  dy_state->net=net;
  net->dy_state=dy_state;
  dy_state->first_dy_adr=10;
  dy_state->period=2000;
  dy_state->rq_max=UL_DY_RQ_MAX;
  dy_state->last_rq_adr=0;
  dy_state->rq_count=0;
  dy_state->flg=0;

  /* statistic */
  if(!net->statistics){
    stat=malloc(sizeof(ul_net_stat_t));
    memset(stat,0,sizeof(ul_net_stat_t));
    stat->net=net;
    net->statistics=stat;
    time(&stat->start_time);
    stat->dy_adr_asg_cnt=0;
    stat->st_adr_req_cnt=0;
  }

  /* initialize timer */
  {
    ul_htim_time_t actual_time;
    ul_root_htimer_current_time(0, &actual_time);
    ul_htimer_set_expire(&dy_state->timer,actual_time);
  }
  dy_state->timer.function=ul_dy_do_timer;
  dy_state->timer.data=(ul_htimer_fnc_data_t)dy_state;
  ul_root_htimer_add(&dy_state->timer);

  /* initialize filters */
  dy_state->ncs_filt.filt_cmd=UL_CMD_NCS;
  dy_state->ncs_filt.filt_fnc=ul_dy_ncs_rec;
  dy_state->ncs_filt.context=dy_state;
  ul_net_filt_add(net,&dy_state->ncs_filt);

  dy_state->gst_filt.filt_cmd=UL_CMD_GST;
  dy_state->gst_filt.filt_fnc=ul_dy_gst_proc;
  dy_state->gst_filt.context=dy_state;
  ul_net_filt_add(net,&dy_state->gst_filt);
  return 1;
}


