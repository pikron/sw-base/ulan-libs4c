/*******************************************************************
  uLan Communication - C interface library

  ul_netbase.c	- basic network and module constructions

  (C) Copyright 2001 by Pavel Pisa - Originator

  (C) Copyright 2002-2004 by Pavel Pisa - Originator

  The uLan C interface library can be used, copied and modified
  under next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_NETBASE_H
#define _UL_NETBASE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <ul_lib/ulan.h>
#include <ul_utdefs.h>
#include <ul_gsa.h>
#include <ul_dbuff.h>
#include <ul_htimer.h>
#include <ul_msg_buf.h>

struct ul_net_info;
struct ul_mod_info;
struct ul_dy_state;
struct ul_msg_filt;
struct ul_msg_buf;
struct ul_mevent;
struct ul_net_stat;

typedef int ul_msg_filt_fnc_t(struct ul_msg_filt *filt, struct ul_mevent* event, void *context) UL_ATTR_REENTRANT;

typedef struct ul_msg_filt {
  int		filt_cmd;	/* command/message type to filter, 0=all */
  ul_msg_filt_fnc_t *filt_fnc;	/* function to call */
  struct ul_net_info *net;	/* net, where is filter or module is registered */
  struct ul_mod_info *mod;	/* module ptr if per module, else NULL */
  void		*context;	/* user usable context */
} ul_msg_filt_t;

typedef struct ul_msg_filt_set {
  gsa_array_field_t filts;
} ul_msg_filt_set_t;

#define ULEV_MSG_REC	0x10	/* message has been received */
#define ULEV_MSG_PROC	0x11	/* outgoing message has been transmitted */
#define ULEV_MSG_ERR	0x12	/* message has failed to be transmitted */
#define ULEV_MOD_FREE	0x13	/* module is going to be discarded */

typedef struct ul_mevent {
  int		ev_type;
  long		ev_info;
  union	{
    struct{
      ul_msg_buf_t *buf;
    } msg;
    struct{
      struct ul_mod_info *mod;
    } mod;
  } u;
} ul_mevent_t;

#define ULMI_DYADR   0x010	/* module supports dynamic addressing */
#define ULMI_PRESENT 0x020	/* module responds to status check */
#define ULMI_DISCON  0x040	/* for disconnection processing */
#define ULMI_STATIC  0x080	/* module has assigned static address */
#define ULMI_ASKDES  0x300	/* priority of ask for description */

/* minimal step of ask description priority */
#define ULMI_ASKDES_MIN ULMI_ASKDES&(~(ULMI_ASKDES<<1))

typedef struct ul_mod_info {	/* module state information structure */
  int 		flg;
  int 		mod_adr;
  unsigned long mod_sn;
  struct ul_net_info* net;
  int		mod_state;
  char		*mod_des;
  char		*mod_type;
  int		err_cnt;
  int		st_err_cnt;
  ul_mstime_t	mod_st0_mstime;
  ul_mstime_t	mod_st1_mstime;
  int		mod_st0_cmd;
  int		mod_st1_cmd;
  ul_dbuff_t	mod_st0_buf;
  ul_dbuff_t	mod_st1_buf;
  ul_msg_filt_set_t msg_filts;
  void		*mod_log;
} ul_mod_info_t;

typedef struct ul_net_info {
  gsa_array_field_t modules;
  char		*dev_name;
  ul_fd_t	ul_fd;
  struct ul_dy_state *dy_state;
  ul_msg_filt_set_t msg_filts;
  uchar		filts_in_drv[32];
  struct ul_net_stat *statistics;
} ul_net_info_t;

#define ULDY_CYOVR   0x010

typedef struct ul_dy_state {
  int		flg;
  int		first_dy_adr;
  ul_net_info_t	*net;
  ul_htimer_t	timer;
  int		period;
  int		last_rq_adr;
  int		rq_count;
  int		rq_max;
  ul_msg_filt_t	ncs_filt;
  ul_msg_filt_t	gst_filt;
} ul_dy_state_t;

typedef struct ul_net_stat {	/* net statistic structure */
  ul_net_info_t	*net;
  time_t       start_time;
  int          dy_adr_asg_cnt;  /* dynamic address assigment */
  time_t       dy_adr_asg_last;
  int          st_adr_req_cnt;  /* static address request */
  time_t       st_adr_req_last;
} ul_net_stat_t;

extern int ul_dy_cycle_subfn;

typedef struct ul_sn_array {
  gsa_array_field_t modules;
} ul_sn_array_t;

extern ul_sn_array_t ul_sn_array;

static inline int
ul_msg_event_dadr(ul_mevent_t *event)
{
  return event->u.msg.buf->msginfo.dadr;
}

static inline int
ul_msg_event_sadr(ul_mevent_t *event)
{
  return event->u.msg.buf->msginfo.sadr;
}

static inline int
ul_msg_event_len(ul_mevent_t *event)
{
  return event->u.msg.buf->msginfo.len;
}

static inline uchar *
ul_msg_event_data(ul_mevent_t *event)
{
  return event->u.msg.buf->data.data;
}

static inline int
ul_msg_event_tail_len(ul_mevent_t *event)
{
  ul_msg_buf_t *msg_buf=event->u.msg.buf;
  if(msg_buf->tail) return msg_buf->tail->msginfo.len;
  return 0;
}

static inline uchar *
ul_msg_event_tail_data(ul_mevent_t *event)
{
  ul_msg_buf_t *msg_buf=event->u.msg.buf;
  if(msg_buf->tail) return msg_buf->tail->data.data;
  return NULL;
}

int ul_net_new(ul_net_info_t **pnet, char *dev_name);
ul_mod_info_t *ul_net_mod_find(ul_net_info_t *net, int adr);
ul_msg_filt_t *ul_mod_filt_find(ul_mod_info_t *mod, int cmd);
int ul_net_filt_add(ul_net_info_t *net, ul_msg_filt_t *filt);
int ul_mod_filt_add(ul_mod_info_t *mod, ul_msg_filt_t *filt);
void ul_filt_detach(ul_msg_filt_t *filt);
int ul_net_mod_insert(ul_net_info_t *net, ul_mod_info_t *mod);
int ul_net_mod_delete(ul_net_info_t *net, ul_mod_info_t *mod);
void ul_net_mod_print(ul_net_info_t *net);
int ul_net_do_rec_msg(ul_net_info_t *net);
int ul_mod_desc_set(ul_mod_info_t *mod, void *des_new, int des_len);
int ul_net_query_desc(ul_net_info_t *net, int adr, int mode);
ul_mod_info_t *ul_mod_sn_find(unsigned long mod_sn);
int ul_mod_new(ul_mod_info_t **pmod, ul_net_info_t *net,
           int mod_adr, unsigned long mod_sn, char *mod_des);
void ul_mod_free(ul_mod_info_t *mod);
int ul_mod_static_add(ul_net_info_t *net,int adr,unsigned long mod_sn);
int ul_mod_static_remove(ul_net_info_t *net,unsigned long mod_sn);


int ul_net_dy_init(ul_net_info_t *net);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_NETBASE_H */
