#include <uldy_base.h>
#ifdef UL_WITHOUT_HANDLE
#include "ul_drv_fnc.h"
#endif
#include <string.h>
#include "uldy_config.h"


void
ul_dy_sn2buf(uchar *buf,unsigned long mod_sn)
{
  buf[0]=mod_sn>>0;
  buf[1]=mod_sn>>8;
  buf[2]=mod_sn>>16;
  buf[3]=mod_sn>>24;
}

unsigned long
ul_dy_buf2sn(const uchar *buf)
{
  unsigned long l;
  l=(unsigned long)buf[0];
  l+=(unsigned long)buf[1]<<8;
  l+=(unsigned long)buf[2]<<16;
  l+=(unsigned long)buf[3]<<24;
  return l;
}

#ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
void uldy_proc_ulncs_data_tag(ul_fd_t ul_fd_data,ul_fd_t ul_fd_op)
{
  unsigned char uc,len,type;
  unsigned long v;

  while(1) {
    if(ul_read(ul_fd_data,&uc,1)!=1) break;
    len=uc&0x0f;
    type=(uc&0xf0);
    if (type==ULNCS_NDATA_TAG_NETWORK_BASE) {
      ul_msg_adr_t adr;
      if(ul_read(ul_fd_data,&uc,1)!=1) break;
      if(ul_read(ul_fd_data,&uc,1)!=1) break;
      adr=0;
      adr|=uc<<8;
      len-=2;
      if (ul_queryparam(ul_fd_op,UL_QP_MYADR,&v)<0)
         return;
      adr|=v&0xff;
      ul_setmyadr(ul_fd_op,adr);
    }
    if (type==ULNCS_NDATA_TAG_ROUTE_DEFAULT_GW) {
      ul_route_range_t rr;
      memset(&rr,0,sizeof(ul_route_range_t));
      if(ul_read(ul_fd_data,&uc,1)!=1) break;
      rr.gw=uc;
      rr.operation=UL_ROUTE_OP_SET;
      len--;
      ul_route(ul_fd_op,&rr);
    }
    while(len>0) {
      if(ul_read(ul_fd_data,&uc,1)!=1) break;
      len--;
    }
  }
}
#endif /* CONFIG_OC_UL_DRV_WITH_MULTI_NET */

int 
uldy_init(ULDY_PARAM_ul_dyac ULDY_PARAM_ul_fd ulf_save_sn save_sn, ulf_save_adr save_adr,
    char *idstr, uint32_t sn)
{
  ul_dyac->f_save_sn=save_sn;
  ul_dyac->f_save_adr=save_adr;
  ul_dyac->ul_sn=sn;
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_set_sn(sn);
 #endif
 #ifdef UL_WITHOUT_HANDLE
    ul_dyac->ul_idstr=ul_idstr.name;
    ul_dyac->ul_idstr_len=ul_idstr.len;
 #else
    ul_dyac->ul_idstr=idstr;
    ul_dyac->ul_idstr_len=strlen(idstr);
 #endif
  ul_dyac->boot_activated=0;
 #ifndef UL_WITHOUT_HANDLE
  ul_dyac->ul_fd=ul_fd;
  ul_dyac->ul_dysa=0;
  ul_dyac->ul_fld=0;
 #endif
 return 0;
}

#define ULDY_TMBUF_LEN 6

#ifdef UL_WITHOUT_HANDLE
UL_DYAC_VAR_LOC uchar uldy_tmpbuf[ULDY_TMBUF_LEN];
#endif /*UL_WITHOUT_HANDLE*/

int 
uldy_i_open(ULDY_PARAM_ul_dyac UL_ARGPTRTYPE ul_msginfo *msginfo)
{
 int ret;

 if(msginfo->sadr<=0){
    ret=ul_acceptmsg(ul_dyac->ul_fd, msginfo);
    if(ret<0){
      return -1;
    }
    if((msginfo->cmd!=UL_CMD_NCS)||
       (msginfo->len<1)||
       (msginfo->flg&UL_BFL_FAIL)) {
     #ifdef ul_i_close
      ul_i_close(ul_dyac->ul_fd);
     #else
      ul_abortmsg(ul_dyac->ul_fd);
     #endif
      return -1;
    }
  }else{
    if((msginfo->cmd!=UL_CMD_NCS)||
       (msginfo->len<1)||
       (msginfo->flg&UL_BFL_FAIL))
      return -1;
  }

  return 1;
}

int 
uldy_rqa(ULDY_PARAM1_ul_dyac) 
{
 #ifdef UL_WITHOUT_HANDLE
  return ul_drv_rqa();
 #else /* UL_WITHOUT_HANDLE */
  int r;
  r=ul_dyac->ul_fld&0x04?1:0;
  if(r)
    ul_dyac->ul_fld--;
  return r;
 #endif /* UL_WITHOUT_HANDLE */
}

void 
uldy_clr_rq(ULDY_PARAM1_ul_dyac)
{
 #ifdef  UL_WITHOUT_HANDLE
   ul_drv_clr_rq();
 #else  /* UL_WITHOUT_HANDLE */
  ul_dyac->ul_fld&=~0x07;
 #endif  /* UL_WITHOUT_HANDLE */
}

int 
uldy_process_msg(ULDY_PARAM_ul_dyac UL_ARGPTRTYPE ul_msginfo *imsginfo)
{
 #ifndef UL_WITHOUT_HANDLE
  ul_msginfo msginfo;
  uchar uldy_tmpbuf[ULDY_TMBUF_LEN];
 #endif  /*UL_WITHOUT_HANDLE*/

  msginfo.sadr=0;
  msginfo.cmd=0;
  if (!imsginfo) imsginfo=&msginfo;

  if (uldy_i_open(ULDY_ARG_ul_dyac imsginfo)<0)
    return -1;

 #ifdef UL_WITHOUT_HANDLE
  if (imsginfo->flg&UL_BFL_PROC) {
    ul_i_close(ul_dyac->ul_fd);
    return 0;
  }
 #endif  /*UL_WITHOUT_HANDLE*/

  if (ul_read(ul_dyac->ul_fd, uldy_tmpbuf,1)!=1) {
    ul_freemsg(ul_dyac->ul_fd);
    return -1;
  }

  switch (uldy_tmpbuf[0]) { /* ulncs */
    case ULNCS_BOOT_ACT:
      if(ul_read(ul_dyac->ul_fd,uldy_tmpbuf,4)!=4) {
        ul_freemsg(ul_dyac->ul_fd);
        return -1;
      }
      ul_freemsg(ul_dyac->ul_fd);
      if (ul_dy_buf2sn(uldy_tmpbuf) != 0)
        if (ul_dy_buf2sn(uldy_tmpbuf)!=ul_dyac->ul_sn)
          return -1;
      imsginfo->dadr=imsginfo->sadr;
      imsginfo->flg=UL_BFL_SND;
      uldy_tmpbuf[4]=ULNCS_BOOT_ACK;
      if(ul_newmsg(ul_dyac->ul_fd, imsginfo)<0) return -1;
      if (ul_write(ul_dyac->ul_fd,&uldy_tmpbuf[4],1)!=1) {
        ul_abortmsg(ul_dyac->ul_fd);
        return -1;
      }
      if (ul_write(ul_dyac->ul_fd,uldy_tmpbuf,4)!=4) {
        ul_abortmsg(ul_dyac->ul_fd);
        return -1;
      }
      ul_freemsg(ul_dyac->ul_fd);
      ul_dyac->boot_activated=1;
      break;
    case ULNCS_SID_RQ:     /* send serial num and ID string request */
      ul_freemsg(ul_dyac->ul_fd);
      imsginfo->dadr=imsginfo->sadr;
      imsginfo->flg =UL_BFL_ARQ;       // status zpravy
      //new message
      if(ul_newmsg(ul_dyac->ul_fd, imsginfo)<0) return -1;
      uldy_tmpbuf[0]=ULNCS_SID_RPLY;
      ul_dy_sn2buf(&uldy_tmpbuf[1],ul_dyac->ul_sn);
      if(ul_write(ul_dyac->ul_fd, uldy_tmpbuf, 5) != 5) {
        ul_abortmsg(ul_dyac->ul_fd);
        return -1;
      }
      if(ul_write(ul_dyac->ul_fd, 
		  ul_dyac->ul_idstr,ul_dyac->ul_idstr_len ) != ul_dyac->ul_idstr_len) {
        ul_abortmsg(ul_dyac->ul_fd);
        return -1;
      }
      ul_freemsg(ul_dyac->ul_fd);
      break;
    case ULNCS_SET_ADDR:  /* SN0 SN1 SN2 SN3 NEW_ADR */
      if(ul_read(ul_dyac->ul_fd,uldy_tmpbuf,5)!=5) {
        ul_freemsg(ul_dyac->ul_fd);
        return -1;
      }
      if (ul_dy_buf2sn(uldy_tmpbuf)!=ul_dyac->ul_sn) {
        ul_freemsg(ul_dyac->ul_fd);
        return -1;
      }
     #ifndef UL_WITHOUT_HANDLE
      ul_setmyadr(ul_dyac->ul_fd,uldy_tmpbuf[4]);
     #else
      ul_drv_set_adr(uldy_tmpbuf[4]);
     #endif
     #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
      uldy_proc_ulncs_data_tag(ul_dyac->ul_fd,ul_dyac->ul_fd);
     #endif /* CONFIG_OC_UL_DRV_WITH_MULTI_NET */
      ul_freemsg(ul_dyac->ul_fd);
      uldy_clr_rq(ULDY_ARG1_ul_dyac);
      break;
    case ULNCS_ADDR_NVSV:{ /* SN0 SN1 SN2 SN3 - save addres to EEPROM */
      if(ul_read(ul_dyac->ul_fd,uldy_tmpbuf,4)!=4) {
        ul_freemsg(ul_dyac->ul_fd);
        return -1;
      }
      ul_freemsg(ul_dyac->ul_fd);
      if (ul_dy_buf2sn(uldy_tmpbuf)!=ul_dyac->ul_sn) return -1;
     #ifndef CONFIG_OC_UL_DRV_WITH_MULTI_NET
      ul_dyac->f_save_adr(imsginfo->dadr);
     #else
      {
        ul_msg_adr_t my_adr;
        unsigned long v;
        if ((ul_queryparam(ul_dyac->ul_fd,UL_QP_FULL_NET_ADR,&v))<0)
           if (ul_queryparam(ul_dyac->ul_fd,UL_QP_MYADR,&v)<0)
             return -1;
        my_adr=v;
        ul_dyac->f_save_adr(my_adr);
      }
     #endif /* CONFIG_OC_UL_DRV_WITH_MULTI_NET */
      };break;
   case ULNCS_SET_SN:{
      if(ul_read(ul_dyac->ul_fd,uldy_tmpbuf,4)!=4) {
        ul_freemsg(ul_dyac->ul_fd);
        return -1;
      }
      ul_freemsg(ul_dyac->ul_fd);
      ul_dyac->ul_sn=ul_dy_buf2sn(uldy_tmpbuf);
     #ifdef UL_WITHOUT_HANDLE
      ul_drv_set_sn(ul_dyac->ul_sn);
     #endif
      ul_dyac->f_save_sn(ul_dyac->ul_sn);
      };break;
   #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
   case ULNCS_NDATA_RQ:{
      unsigned long v;
      ul_freemsg(ul_dyac->ul_fd);
      imsginfo->dadr=imsginfo->sadr;
      imsginfo->flg =UL_BFL_ARQ;       // status zpravy
      //new message
      if(ul_newmsg(ul_dyac->ul_fd, imsginfo)<0) return -1;
      uldy_tmpbuf[0]=ULNCS_NDATA_RPLY;
      if (ul_queryparam(ul_dyac->ul_fd,UL_QP_MY_NET_BASE,&v)<0)
        return -1;
      uldy_tmpbuf[1]=ULNCS_NDATA_TAG_NETWORK_BASE|0x2; /* type | len */
      uldy_tmpbuf[2]=0;
      uldy_tmpbuf[3]=v>>8;
      if (ul_queryparam(ul_dyac->ul_fd,UL_QP_DEF_GW_ADR,&v)<0)
        return -1;
      uldy_tmpbuf[4]=ULNCS_NDATA_TAG_ROUTE_DEFAULT_GW|0x1; /* type | len */
      uldy_tmpbuf[5]=v;
      if(ul_write(ul_dyac->ul_fd, uldy_tmpbuf, 6) != 6) {
        ul_abortmsg(ul_dyac->ul_fd);
        return -1;
      }
      ul_freemsg(ul_dyac->ul_fd);
      };break;
   #endif /* CONFIG_OC_UL_DRV_WITH_MULTI_NET */
    default:
      break;
  }
  return 0;
}

int
uldy_addr_rq(ULDY_PARAM1_ul_dyac)
{
#ifndef UL_WITHOUT_HANDLE
  ul_msginfo msginfo;
  uchar uldy_tmpbuf[ULDY_TMBUF_LEN];
#endif /*UL_WITHOUT_HANDLE*/

 #ifdef UL_WITHOUT_HANDLE
  msginfo.dadr=(ul_drv_get_dysa())&0x7F;     // dadr
 #else
  if(ul_dyac==NULL)
    return -1;
  msginfo.dadr=(ul_dyac->ul_dysa);           // dadr
  if(ul_dyac->ul_fd==UL_FD_INVALID)
    return -1;
 #endif

  msginfo.cmd =UL_CMD_NCS;    // cmd
  msginfo.flg =UL_BFL_ARQ | UL_BFL_SND;    // status zpravy
  msginfo.stamp=0;            // stamp
  if(ul_newmsg(ul_dyac->ul_fd, &msginfo)<0) return -1;
  uldy_tmpbuf[0]=ULNCS_ADR_RQ;
  ul_dy_sn2buf(&uldy_tmpbuf[1],ul_dyac->ul_sn);
  if(ul_write(ul_dyac->ul_fd, uldy_tmpbuf, 5) != 5) return -1;
  ul_freemsg(ul_dyac->ul_fd);
  return 0;
}

#ifdef CONFIG_OC_UL_DRV_SYSLESS

int
ul_iac_call_gst(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data)
{
  ul_dyac_t *ul_dyac;

  ul_dyac=(ul_dyac_t*)data->ctx;
  if (msginfo->len<1) return UL_IAC_RC_PROC;
  if (!(ibuff[0]&0xfc)) {
    ul_dyac->ul_dysa=msginfo->sadr;
    if (!(ul_dyac->ul_fld&0x04)) {
      ul_dyac->ul_fld++;
      if (ul_dyac->ul_fld&0x04) {
        if (ibuff[0]==0)
          ul_setmyadr(ul_dyac->ul_fd,0);
      }
    }
    return UL_IAC_RC_PROC;
  } else {
    if (((ibuff[0]&0xf0)==0x10) && (msginfo->len>=5)) {
      unsigned long sn;
      sn=ul_dy_buf2sn(ibuff+1);
      if(sn && (sn!=ul_dyac->ul_sn))
        return UL_IAC_RC_PROC;
      uldy_clr_rq(ul_dyac);
      ul_dy_sn2buf(data->buff, sn);
      return UL_IAC_RC_FREEMSG;
    }
  }
  return UL_IAC_RC_PROC;
}

#endif /* CONFIG_OC_UL_DRV_SYSLESS */
