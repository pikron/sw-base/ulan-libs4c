#ifndef _UL_DYAC_H
#define _UL_DYAC_H

#include <ul_lib/ulan.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef UL_ARGPTRTYPE
#define UL_ARGPTRTYPE
#endif

#ifdef UL_WITHOUT_HANDLE
 #include <cpu_def.h>
 #include <stdint.h>
 #ifndef UL_DYAC_VAR_LOC
  #define UL_DYAC_VAR_LOC __xdata
 #endif
  extern ul_idstr_t CODE ul_idstr;
#else
 #define UL_DYAC_VAR_LOC
 #include <stdint.h>
#endif

#if ((defined(SDCC) || defined(__SDCC)) && !defined(SDCC_z80)) || defined(__KEIL__) || defined(__C51__)
  #define UL_DYAC_REENTRANT __reentrant
#else
  #define UL_DYAC_REENTRANT
#endif

typedef char 	 (*ulf_save_sn)(uint32_t usn) UL_DYAC_REENTRANT; 
typedef char 	 (*ulf_save_adr)(ul_msg_adr_t uaddr) UL_DYAC_REENTRANT; 

typedef struct ul_dyac_t {
  ulf_save_sn  f_save_sn; 
  ulf_save_adr f_save_adr;
  uint32_t ul_sn;
  char *ul_idstr;
  uint8_t ul_idstr_len;
  char boot_activated;
 #ifndef UL_WITHOUT_HANDLE
  ul_fd_t ul_fd;
  ul_msg_adr_t ul_dysa;
  uint8_t ul_fld;
 #endif /* UL_WITHOUT_HANDLE */
} ul_dyac_t;

#ifdef UL_WITHOUT_HANDLE
  #define ULDY_ARG_ul_dyac
  #define ULDY_ARG1_ul_dyac
  #define ULDY_PARAM_ul_dyac
  #define ULDY_PARAM1_ul_dyac
  #define ULDY_ARG_ul_fd
  #define ULDY_PARAM_ul_fd
  #define ul_dyac (&ul_dyac_global)
  extern UL_DYAC_VAR_LOC ul_dyac_t ul_dyac_global;
#else  /*UL_WITHOUT_HANDLE*/
  #define ULDY_ARG_ul_dyac ul_dyac,
  #define ULDY_ARG1_ul_dyac ul_dyac
  #define ULDY_PARAM_ul_dyac ul_dyac_t *ul_dyac,
  #define ULDY_PARAM1_ul_dyac ul_dyac_t *ul_dyac
  #define ULDY_ARG_ul_fd ul_fd,
  #define ULDY_PARAM_ul_fd ul_fd_t ul_fd,
#endif /*UL_WITHOUT_HANDLE*/

int uldy_init(ULDY_PARAM_ul_dyac ULDY_PARAM_ul_fd ulf_save_sn save_sn, ulf_save_adr save_adr,
    char *idstr, uint32_t sn);
int uldy_process_msg(ULDY_PARAM_ul_dyac UL_ARGPTRTYPE ul_msginfo *imsginfo);
int uldy_rqa(ULDY_PARAM1_ul_dyac);
int uldy_addr_rq(ULDY_PARAM1_ul_dyac);

#ifdef CONFIG_OC_UL_DRV_SYSLESS

#include <ul_drv_iac.h>

void uldy_clr_rq(ul_dyac_t *ul_dyac);
int ul_iac_call_gst(struct ul_drv *udrv,ul_msginfo *msginfo,char *ibuff,ul_iac_data *data);

#endif /* CONFIG_OC_UL_DRV_SYSLESS */

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_DYAC_H */
