/*******************************************************************
  uLan Communication - basic library

  ul_l_oi.c	- definitions of basic uLan OI functions

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <stdlib.h>
#include <string.h>
#include <uloi_com.h>

#ifdef CONFIG_OC_ULUT
#include <ul_log.h>
extern ul_log_domain_t ulogd_uloi;
#define UL_LDOMAIN (&ulogd_uloi)
#else
#if defined(SDCC) || defined(__SDCC)
#define ul_log(x,args...) 
#else
#include <ul_lib/ul_l_log.h>
#define UL_LDOMAIN NULL
#endif
#endif

#define ULOI_DEB_OIDDES  1
#define ULOI_DEB_AOIDDES 2
int uloi_debug_flg=0;


/* definitions of basic uLan OI functions */

uloi_coninfo_t* uloi_open(char *ul_dev_name,int adr,int cmd,
			int bcmd, int timeout)
{
  uloi_con_ulan_t *con_ulan;
  ul_msginfo msginfo;
  ul_fd_t ul_fd;
  if(!timeout) timeout=10;
  if(!bcmd) bcmd=0x11;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) return NULL;
  con_ulan=malloc(sizeof(uloi_con_ulan_t));
  if(con_ulan) {
    memset(con_ulan,0,sizeof(*con_ulan));
    memset(&msginfo,0,sizeof(ul_msginfo));
    msginfo.sadr=adr;
    msginfo.cmd=bcmd;
    if(ul_addfilt(ul_fd,&msginfo)>=0)
    { con_ulan->rdfd=ul_fd;
      con_ulan->adr=adr;
      con_ulan->cmd=cmd;
      con_ulan->bcmd=bcmd;
      con_ulan->timeout=timeout;
      return &con_ulan->con;
    }
    free(con_ulan);
  }
  ul_close(ul_fd);return NULL;
}

void uloi_close(uloi_coninfo_t *coninfo)
{ 
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  ul_close(uloi_con_ulan_rdfd(coninfo));
  free(con_ulan);
}

int uloi_transfer(uloi_coninfo_t *coninfo,
		  uchar *bufin,int lenin,uchar **bufout,int *lenout)
{ int ret;
  uchar *buf;
  ul_msginfo msginfo;
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);

  if(bufin&&lenin)
  { bufin[0]=con_ulan->bcmd;
    bufin[1]=((++con_ulan->sn)&0x3F)+0x40;
    bufin[2]=con_ulan->bsn;
    ret=ul_send_command_wait(uloi_con_ulan_rdfd(coninfo),con_ulan->adr,con_ulan->cmd,
    		             con_ulan->outflg&~UL_BFL_TAIL,bufin,lenin);
    if(ret<0) return ret;
  }
  if(bufout) while(1) 
  { ret=ul_fd_wait(uloi_con_ulan_rdfd(coninfo),con_ulan->timeout);
    if(ret<=0) return ret?ret:-1;
    ret=ul_acceptmsg(uloi_con_ulan_rdfd(coninfo),&msginfo);
    if(ret<0) return ret;
    if((msginfo.cmd!=con_ulan->bcmd)
     ||(msginfo.sadr!=con_ulan->adr)
     ||(msginfo.len<3))
    { ul_freemsg(uloi_con_ulan_rdfd(coninfo));
      continue;
    }
    buf=malloc(msginfo.len);
    ret=ul_read(uloi_con_ulan_rdfd(coninfo),buf,msginfo.len);
    ul_freemsg(uloi_con_ulan_rdfd(coninfo));
    if((buf[2]&0x3F)==(con_ulan->sn&0x3F))
    { con_ulan->bsn=buf[1];
      *bufout=buf;
      *lenout=msginfo.len;
      return 1;
    }
    free(buf);
  };
  return 0;
}

int uloi_set_var(uloi_coninfo_t *coninfo,int oid, void *val, int size)
{ uchar *bufout=NULL;
  uchar *bufin;
  int lenout, lenin, ret;
  lenin=3+2+size+4;
  bufin=malloc(lenin);
  if(!bufin) return -1;
  bufin[3]=(uchar)oid;
  bufin[4]=(uchar)(oid>>8);
  memcpy(bufin+5,val,size);
  bufin[5+size+0]=ULOI_RDRQ;
  bufin[5+size+1]=0;
  bufin[5+size+2]=0;
  bufin[5+size+3]=0;
  ret=uloi_transfer(coninfo,bufin,lenin,&bufout,&lenout);
  free(bufin);
  if(ret<=0) return -1;
  if(bufout) free(bufout);
  return 0;
}

int uloi_get_var(uloi_coninfo_t *coninfo,int oid, void *meta, int meta_len, void *val, int size)
{ uchar *bufout=NULL;
  uchar *bufin;
  int lenout, lenin, ret;
  lenin=3+2+2+meta_len+2;
  bufin=malloc(lenin);
  if(!bufin) return -1;
  bufin[3]=ULOI_RDRQ;
  bufin[4]=0;
  bufin[5]=(uchar)oid;
  bufin[6]=(uchar)(oid>>8);
  if ((meta_len>0) && (meta!=NULL))
    memcpy(((uchar*)bufin)+7,meta,meta_len);
  bufin[7+meta_len]=0;
  bufin[8+meta_len]=0;
  ret=uloi_transfer(coninfo,bufin,lenin,&bufout,&lenout);
  free(bufin);
  if(ret<=0) return -1;
  ret=-1;
  if((bufout[3]==21)&&(bufout[4]==0)
   &&(bufout[5]==(uchar)oid)&&(bufout[6]==(uchar)(oid>>8)))
  {
    if (size>(lenout-7)) size=lenout-7;
    memcpy(val,bufout+7,size);
    ret=size;
  }
  if(bufout) free(bufout);
  return ret;
}

int uloi_set_var_u2(uloi_coninfo_t *coninfo,int oid,unsigned val)
{ uchar buf[2];
  buf[0]=(uchar)val;
  buf[1]=(uchar)(val>>8);
  return uloi_set_var(coninfo,oid,buf,2);
}

int uloi_get_var_u2(uloi_coninfo_t *coninfo,int oid,unsigned *val)
{ int ret;
  uchar buf[2];
  ret=uloi_get_var(coninfo,oid,NULL,0,buf,2);
  if(ret>=0)
    *val=buf[0]+(buf[1]<<8);
  return ret;
}

int uloi_send_cmd(uloi_coninfo_t *coninfo,int oid)
{ uchar buf[2];
  return uloi_set_var(coninfo,oid,buf,0);
}

int uloi_get_oids(uloi_coninfo_t *coninfo,int list,int **oids_list)
{ int *oids=NULL;
  int at_once=64;
  uchar bufin[9]={0,0,0,list,list>>8,0,0,at_once,at_once>>8};
  int lenin=sizeof(bufin);
  uchar *bufout=NULL;
  int lenout;
  int last_oid=0;
  int oid_cnt=0;
  int cnt,i;
  uchar *p;
  int ret;
  do
  { bufin[5]=last_oid;bufin[6]=last_oid>>8;
    ret=uloi_transfer(coninfo,bufin,lenin,&bufout,&lenout);
    if(ret<0) {if(oids!=NULL) free(oids);return -1;};
    cnt=lenout-5-2;
    if(cnt<=0) {if(oids!=NULL) free(oids);return -1;};
    cnt/=2;
    if(oids==NULL) oids=malloc((cnt+1)*sizeof(int));
    else oids=realloc(oids,(oid_cnt+cnt+1)*sizeof(int));
    p=bufout+5;
    i=cnt;
    if(last_oid)
    { if(last_oid!=p[0]+p[1]*256)
        {free(oids);free(bufout);return -1;};
      p+=2; i--;
    }
    while(i--)
    { last_oid=*(p++);
      last_oid+=*(p++)*0x100;
      if(!last_oid) break;
      oids[oid_cnt++]=last_oid;
    }
    oids[oid_cnt]=0;
    free(bufout);
  }
  while(cnt>=at_once);
  *oids_list=oids;
  return oid_cnt;
}

const uchar *uloi_oiddespack_getloc(const uchar *despack, int strindex)
{
  int remcnt;
  const uchar *p=despack;

  if(!p) return NULL;
  remcnt=*(p++);
  while(strindex--&&remcnt){
    if(*p+2>remcnt)
      return NULL;
    remcnt-=*p+1;
    p+=*p+1;
  }
  if(!remcnt)
    return NULL;
  if(*p+1>remcnt)
    return NULL;

  return p;
}

char *uloi_oiddespack_strdup(const uchar *despack, int strindex)
{
  const uchar *p;
  int len;
  char *str;

  p=uloi_oiddespack_getloc(despack, strindex);
  if(!p)
    return NULL;

  len=*(p++);

  str=malloc(len+1);
  if(!str)
    return NULL;

  memcpy(str,(char*)p,len);
  str[len]=0;
  return str;
}

int uloi_get_oiddes(uloi_coninfo_t *coninfo,int list, int oid, uchar **poiddespack)
{ 
  uchar bufin[9]={0,0,0,list,list>>8,oid,oid>>8,0,0};
  int lenin=sizeof(bufin);
  uchar *bufout=NULL;
  int lenout;
  int ret;
  uchar *despack;

  if(poiddespack)
    *poiddespack=NULL;

  ret=uloi_transfer(coninfo,bufin,lenin,&bufout,&lenout);

  if(ret<0) return -1;
  if(lenout<3+2+2+1) {
    ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides reply %d truncated (<8 bytes)\n",oid);
    free(bufout);return -1;
  }
  if(bufout[3]+bufout[4]*0x100!=list+1) {
    ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides reply %d doesnot match list\n",oid);
    free(bufout);return -1;
  }
  if(bufout[5]+bufout[6]*0x100!=oid) {
    ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides reply %d mitchmatch with oid %d\n",
           oid,bufout[5]+bufout[6]);
    free(bufout);return -1;
  }
  if(lenout-7<bufout[7]+1) {
    ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides reply %d truncated\n",oid);
    free(bufout);return -1;
  }

  despack=bufout+7;

  if(uloi_debug_flg&ULOI_DEB_OIDDES){
    char *desname=uloi_oiddespack_strdup(despack,0);
    char *destype=uloi_oiddespack_strdup(despack,1);
    ul_log(UL_LDOMAIN,UL_LOGL_DEB,"got oiddes:  oid=%d  deslen=%d name=\"%s\" type=\"%s\"\n",
           oid,despack[0],desname?desname:"",destype?destype:"");
    if(desname) free(desname);
    if(destype) free(destype);
  }

  if(poiddespack){
    *poiddespack=malloc(despack[0]+1);
    if(*poiddespack){
      memcpy(*poiddespack,despack,despack[0]+1);
    }else{
      ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides malloc failed for reply %d\n",oid);  
    }
  }

  free(bufout);
  return 0;
}

int uloi_get_aoiddes(uloi_coninfo_t *coninfo,int list,char *aoid, uchar **poiddespack)
{ 
  uchar *bufin;
  int lenin;
  uchar *bufout=NULL;
  int lenout;
  int ret;
  int oid;
  int aoidlen;
  uchar *despack;

  if(poiddespack)
    *poiddespack=NULL;

  aoidlen=strlen(aoid);
  if(aoidlen>100) return -1;
  bufin=malloc(7+1+aoidlen+2);
  bufin[3]=list;bufin[4]=list>>8;
  bufin[5]=ULOI_AOID;bufin[6]=ULOI_AOID>>8;
  bufin[7]=aoidlen;
  memcpy(bufin+8,aoid,aoidlen);
  bufin[7+1+aoidlen]=0;bufin[7+1+aoidlen+1]=0;
  lenin=7+1+aoidlen+2;
  ret=uloi_transfer(coninfo,bufin,lenin,&bufout,&lenout);
  free(bufin);

  if(ret<0) return -1;
  if(lenout<3+2+2+1) {
    ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides reply for \"%s\" truncated (<8 bytes)\n",aoid);
    free(bufout);return -1;
  }
  if(bufout[3]+bufout[4]*0x100!=list+1) {
    ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides reply for \"%s\" doesnot match list\n",aoid);
    free(bufout);return -1;
  }
  
  oid=bufout[5]+bufout[6]*0x100;

  if(lenout-7<bufout[7]+1) {
    ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides reply %d truncated\n",oid);
    free(bufout);return -1;
  }

  despack=bufout+7;

  if(uloi_debug_flg&ULOI_DEB_OIDDES){
    char *desname=uloi_oiddespack_strdup(despack,0);
    char *destype=uloi_oiddespack_strdup(despack,1);
    ul_log(UL_LDOMAIN,UL_LOGL_DEB,"got oiddes:  oid=%d  deslen=%d name=\"%s\" type=\"%s\"\n",
           oid,despack[0],desname?desname:"",destype?destype:"");
    if(desname) free(desname);
    if(destype) free(destype);
  }

  if(poiddespack){
    *poiddespack=malloc(despack[0]+1);
    if(*poiddespack){
      memcpy(*poiddespack,despack,despack[0]+1);
    }else{
      ul_log(UL_LDOMAIN,UL_LOGL_ERR,"oides malloc failed for reply %d\n",oid);  
    }
  }

  free(bufout);
  return oid;
}

void uloi_cfree(void *p)
{
  if (p) free(p);
}

