#include <string.h>
#include <uloi_com.h>
#include <ul_l_oitarr.h>

#define UL_L_OITARR_INCLUDE_INTERNAL

#include "ul_l_oitarr.h"

int uloi_array_rdrange_buff(ul_msgbuff_t *msgbuff, unsigned int *from_idx,
                       unsigned int *item_cnt)
{
  unsigned int arg;

  if ((msgbuff->size - msgbuff->idx - 1) < 2)
    return -1;
  arg = msgbuff->data[msgbuff->idx];
  arg+= msgbuff->data[msgbuff->idx+1]<<8;
  msgbuff->idx+=2;
  if(!(arg & ULOI_ARRAY_USE_ITEM_CNT)) {
    *from_idx = arg;
    *item_cnt = 1;
    return 0;
  }
  arg &= ~ULOI_ARRAY_USE_ITEM_CNT;
  if(!(arg & ULOI_ARRAY_USE_COMMAND)) {
    *item_cnt = arg;
    if ((msgbuff->size - msgbuff->idx - 1) < 2)
      return -1;
    arg = msgbuff->data[msgbuff->idx];
    arg+= msgbuff->data[msgbuff->idx+1]<<8;
    msgbuff->idx+=2;
    *from_idx = arg;
    return 0;
  }
  arg &= ~ULOI_ARRAY_USE_COMMAND;
  return arg;
}

int uloi_array_wrrange_buff(ul_msgbuff_t *msgbuff, const unsigned int *from_idx,
                       const unsigned int *item_cnt)
{
  unsigned int arg;
  if((*item_cnt != 1) || (*from_idx >= ULOI_ARRAY_USE_ITEM_CNT)) {
    arg=(*item_cnt) | ULOI_ARRAY_USE_ITEM_CNT;
    if ((msgbuff->size - msgbuff->idx - 1) < 2)
      return -1;
    msgbuff->data[msgbuff->idx]=arg;
    msgbuff->data[msgbuff->idx+1]=arg>>8;
    msgbuff->idx+=2;
  }
  if ((msgbuff->size - msgbuff->idx - 1) < 2)
    return -1;
  msgbuff->data[msgbuff->idx]=*from_idx;
  msgbuff->data[msgbuff->idx+1]=(*from_idx)>>8;
  msgbuff->idx+=2;
  return 0;
}

int uloi_tb_buff_wr_array_ranges_gen(int level,int dim,int cnt,uloi_array_range_t *pranges,uchar **buff, int *size)
{
  int i;

  if (level==dim)
    return 0;
  
  for(i=0;i<cnt;i++) {
    if (pranges[level].count==1) {
      (*buff)[0]=pranges[level].start;
      (*buff)[1]=pranges[level].start>>8;
      (*buff)+=2;
      (*size)-=2;
    } else {
      (*buff)[0]=pranges[level].count;
      (*buff)[1]=(pranges[level].count>>8) | 0x80;
      (*buff)[2]=pranges[level].start;
      (*buff)[3]=pranges[level].start>>8;
      (*buff)+=4;
      (*size)-=4;
    }
    uloi_tb_buff_wr_array_ranges_gen(level+1,dim,pranges[level].count,pranges,buff,size);
    if (level==0)
      break;
  }
  return 0;
}
