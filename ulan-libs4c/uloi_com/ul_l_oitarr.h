#ifndef _UL_L_OITARR_H
#define _UL_L_OITARR_H

#include <uloi_com.h>

#if defined(SDCC) || defined(__SDCC)
#define UL_L_OITARR_NO_INLINE
#endif

#ifndef UL_L_OITARR_NO_INLINE
#define UL_L_OITARR_INLINE
#endif

#ifndef ul_l_oirarr_inline
#ifdef UL_L_OITARR_INLINE
#define ul_l_oirarr_inline static inline
#else
#define ul_l_oirarr_inline
#endif
#endif

typedef struct ul_msgbuff {
  uchar *data;
  unsigned int idx;
  unsigned int size;
} ul_msgbuff_t;

int uloi_array_rdrange_buff(ul_msgbuff_t *msgbuff, unsigned int *from_idx, unsigned int *item_cnt);
int uloi_array_wrrange_buff(ul_msgbuff_t *msgbuff, const unsigned int *from_idx,
                       const unsigned int *item_cnt);

int uloi_tb_buff_wr_array_ranges_gen(int level,int dim,int cnt,uloi_array_range_t *pranges,uchar **buff, int *size);

#if !defined(UL_L_OITARR_INLINE)
int uloi_tb_buff_wr_array_ranges(int dim,uloi_array_range_t *pranges,uchar *buff, int size);
#endif /* UL_L_OITARR_INLINE */

#if defined(UL_L_OITARR_INLINE) || defined(UL_L_OITARR_INCLUDE_INTERNAL)
ul_l_oirarr_inline
int uloi_tb_buff_wr_array_ranges(int dim,uloi_array_range_t *pranges,uchar *buff, int size)
{
  uchar **pbuff=&buff;
  return uloi_tb_buff_wr_array_ranges_gen(0,dim,1,pranges,pbuff,&size);
}

#endif /*UL_L_OITARR_INLINE*/

#endif /* _UL_L_OITARR_H */
