/*******************************************************************
  uLan Communication - basic library

  ul_l_oitb.c	- functions for basic uLan OI types manipulations

  (C) Copyright 2008 by Pavel Pisa

  The uLan driver is distributed under the Gnu General Public Licence.
  See file COPYING for details.
 *******************************************************************/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <uloi_com.h>

int uloi_tb_type2size(uchar *type, int len)
{
  if(type == NULL)
    return -1;
  if(len == -1)
    len = strlen((char *)type);

  do {
    /* check for standard basic types */
    if(len < 2)
      break;
    if(strchr("uscf", type[0]) == NULL)
      break;
    if(!isdigit(type[1]))
      break;
    if((len > 2) && isalnum(type[2]))
      break;
    return type[1] - '0';
  } while(0);

  do {
    /* check for "string" type */
    if(len < 2)
      break;
    if((type[0]!='v') || (type[1]!='s'))
      break;
   #if !defined(SDCC) && !defined(__SDCC)
    return strtol((char*)(type+2),NULL,0);
   #else
    return atoi((char*)(type+2));
   #endif /* SDCC */
  } while(0);

  do {
    /* check for "e" type */
    if(type[0] != 'e')
      break;
    if((len > 1) && isalnum(type[1]))
      break;
    return 0;
  } while(0);

  do {
    /* check for "pico" type */
    if(len < 4)
      break;
    if((type[0]!='p') || (type[1]!='i') ||
       (type[2]!='c') || (type[3]!='o'))
      break;
    return 8;
  } while(0);

  do {
    /* check for "poco" type */
    if(len < 4)
      break;
    if((type[0]!='p') || (type[1]!='o') ||
       (type[2]!='c') || (type[3]!='o'))
      break;
    return 8;
  } while(0);

  do {
    /* check for "pev2c" type */
    if(len < 5)
      break;
    if((type[0]!='p') || (type[1]!='e') ||
       (type[2]!='v') || (type[3]!='2') || (type[4]!='c'))
      break;
    return 8;
  } while(0);

  return -1;
}

int uloi_tb_buff_rd_signed(signed long *pval, const void *buff, int size)
{
  signed long val = 0;
  uchar *p = (uchar *) buff;
  int i;
  if(size<=0)
    return -1;
  for(i = 0; i < size; i++) {
    val |= (long)*(p++) << (i * 8);
  }
  if(*(--p) & 0x80)
    val |= ~0L << (i * 8);
  *pval = val;
  return 0;
}

int uloi_tb_buff_rd_unsigned(unsigned long *pval, const void *buff, int size)
{
  signed long val = 0;
  uchar *p = (uchar *) buff;
  int i;
  if(size<=0)
    return -1;
  for(i = 0; i < size; i++) {
    val |= (unsigned long)*(p++) << (i * 8);
  }
  *pval = val;
  return 0;
}

int uloi_tb_buff_wr_signed(void *buff, int size, signed long val)
{
  uchar *p = (uchar *) buff;
  int i;
  if(size<=0)
    return -1;
  for(i = 0; i < size; i++) {
    *(p++) = val & 0xff;
    val >>= 8;
  }
  if(*(--p) & 0x80) {
    if(val != -1)
      return -1;
  } else {
    if(val != 0)
      return -1;
  }
  return 0;
}

int uloi_tb_buff_wr_unsigned(void *buff, int size, unsigned long val)
{
  uchar *p = (uchar *) buff;
  int i;
  if(size<=0)
    return -1;
  for(i = 0; i < size; i++) {
    *(p++) = val & 0xff;
    val >>= 8;
  }
  if(val != 0)
    return -1;
  return 0;
}

