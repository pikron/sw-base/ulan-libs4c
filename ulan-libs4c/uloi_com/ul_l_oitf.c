/*******************************************************************
  uLan Communication - basic library

  ul_l_oitf.c	- functions for float uLan OI types manipulations

  (C) Copyright 2008 by Pavel Pisa

  The uLan driver is distributed under the Gnu General Public Licence.
  See file COPYING for details.
 *******************************************************************/
#define _ISOC99_SOURCE

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <uloi_com.h>

#if defined(SDCC) || defined(__SDCC)
#define frexp frexpf
#define ldexp ldexpf
#endif /* SDCC */


int uloi_tb_buff_rd_float(float *pval, const void *buff, int size)
{
  float val;
  unsigned long m;
  int e;
  int s = 0;
  uchar *p = (uchar *) buff;
  if(size==2) {
    m = *(p++);
    m |= ((unsigned)(*p & 0x03) << 8);
    e = ((*p >> 2) & 0x1f);
    s = *p & 0x80;
    if(e)
      m |= (1 << 10);
    else
      e = 1;
    e -= 0xf + 10;
  } else if(size == 4) {
    m = *(p++);
    m |= (unsigned)*(p++) << 8;
    m |= (unsigned long)(*p & 0x7f) << 16;
    e = *(p++) & 0x80 ? 1 : 0;
    e |= (*p & 0x7f) << 1;
    s = *p & 0x80;
    if(e)
      m |= (1 << 23);
    else
      e = 1;
    e -= 0x7f + 23;
  } else {
    return -1;
  }
  if(!m) {
    val = 0;
  } else {
    val = ldexp((float)m,e);
    if(s)
      val = -val;
  }
  *pval = val;
  return 0;
}

int uloi_tb_buff_wr_float(void *buff, int size, float val)
{
  uchar *p = (uchar *) buff;
  unsigned long m = 0;
  int e = 0;
  int s = 0;

  if(val != 0.0) {
    if(val < 0.0) {
      s = -1;
      val = -val;
    } else {
      s = 1;
    }
    val = frexp(val, &e);
  }
  if(size==2) {
    if(s) {
      e += 0xe;
      if (e > 0) {
        m = ldexp(val, 11);
      } else {
        if(e >= -10)
          m = ldexp(val, 10 + e);
	e = 0;
      }
    }
    *(p++) = m & 0xff;
    *p = (m >> 8) & 0x03;
    *p |= e << 2;
    if(s < 0)
      *p |= 0x80;
  } else if(size == 4) {
   #if !defined(SDCC) && !defined(__SDCC)
    if (isnan(val)) {
      e = 0xff;
      m = ~0;
    } else
   #endif /* SDCC does not provided NAN */
    if (s) {
      e += 0x7e;
      if (e > 0) {
        m = ldexp (val, 24);
      } else {
        if(e >= -23)
          m = ldexp (val, 23 + e);
	e = 0;
      }
    }
    *(p++) = m & 0xff;
    *(p++) = (m >> 8) & 0xff;
    *(p++) = ((m >> 16) & 0x7f) | ((e & 1) << 7);
    *p = e >> 1;
    if(s < 0)
      *p |= 0x80;
  } else {
    return -1;
  }
  return 0;
}

#if 0

#include <stdio.h>

uchar hftest[][2] = {
  {0x00, 0x3c},
  {0x00, 0xc0},
  {0xff, 0x7b},
  {0x55, 0x35},
  {0x01, 0x00},
};

float my_test(char *s)
{
  uchar buff[16];
  float f, f1, f2, hf;
  int i;

  f = strtof(s, NULL);
  *(float*)buff=f;
  uloi_tb_buff_rd_float(&f1, buff, 4);
  uloi_tb_buff_wr_float(buff, 4, f);
  uloi_tb_buff_rd_float(&f2, buff, 4);
  printf("%f read as %f writes as %f\n", f, f1, f2);
  printf("%e read as %e writes as %e\n", f, f1, f2);

  uloi_tb_buff_wr_float(buff, 2, f);
  uloi_tb_buff_rd_float(&hf, buff, 2);
  printf("%e read as hf %e\n", f, hf);

  for(i=0; i < sizeof(hftest)/sizeof(hftest[0]); i++) {
    uloi_tb_buff_rd_float(&hf, hftest[i], 2);
    printf("half-float %f\n", hf);
  }
  return f1;
}

#endif
