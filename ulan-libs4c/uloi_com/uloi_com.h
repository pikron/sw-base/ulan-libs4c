#ifndef _ULOI_COM_H
#define _ULOI_COM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <ul_lib/ulan.h>
#include <uloi_def.h>

#if defined(SDCC) || defined(__SDCC)
#define ULOI_COM_NO_INLINE
#endif

#ifndef ULOI_COM_NO_INLINE
#define ULOI_COM_INLINE
#endif

#ifndef uloi_com_inline
#ifdef ULOI_COM_INLINE
#define uloi_com_inline static inline
#else
#define uloi_com_inline
#endif
#endif

struct uloi_coninfo_t;

#ifdef CONFIG_ULOI_CON_IO_OPS

typedef struct uloi_con_io_ops_t {
  int (*i_open)(struct uloi_coninfo_t *coninfo, ul_msginfo *imsginfo);
  int (*o_open)(struct uloi_coninfo_t *coninfo);
  int (*i_close)(struct uloi_coninfo_t *coninfo);
  int (*o_close)(struct uloi_coninfo_t *coninfo);
  ssize_t (*i_read)(struct uloi_coninfo_t *coninfo, void *buffer, size_t size);
  ssize_t (*o_write)(struct uloi_coninfo_t *coninfo, const void *buffer, size_t size);
  int (*set_io_mode)(struct uloi_coninfo_t *coninfo, int mode);
} uloi_con_io_ops_t;

extern uloi_con_io_ops_t uloi_con_io_ops_ulan;

#endif /*CONFIG_ULOI_CON_IO_OPS*/

typedef struct uloi_coninfo_t {
 #ifdef CONFIG_ULOI_CON_IO_OPS
  const uloi_con_io_ops_t *io_ops;
 #endif /*CONFIG_ULOI_CON_IO_OPS*/
  int error;	/* error condition occurred */
  int state;	/* internal state */
  int array_idx;/* array index */
} uloi_coninfo_t;

typedef struct uloi_con_ulan_t {
  uloi_coninfo_t con;
  int adr;	/* address of target module */
  int cmd;	/* service/cmd number for uLOI on target */
  int bcmd;	/* service/cmd number for returned messages */
  int sn;	/* sequence counter */
  int bsn;	/* sequence counter of target module */
  int outflg;	/* flags used for outgoing messages */
  ul_fd_t rdfd; /* handle for ul_drv */
  ul_fd_t wrfd; /* the auxiliary handle for ULOI direct reply */
  int timeout;	/* timeout */
} uloi_con_ulan_t;


#if !defined(ULOI_COM_INLINE)
ul_fd_t uloi_con_ulan_rdfd(uloi_coninfo_t *coninfo);
ul_fd_t uloi_con_ulan_wrfd(uloi_coninfo_t *coninfo);
int uloi_con_ulan_cmd(uloi_coninfo_t *coninfo);
int uloi_con_ulan_bcmd(uloi_coninfo_t *coninfo);
int uloi_con_ulan_set_cmd_fd(uloi_coninfo_t *coninfo, int cmd,
                                 ul_fd_t rdfd, ul_fd_t wrfd);
int uloi_con_ulan_set_outflg(uloi_coninfo_t *coninfo, int outflg);
int uloi_con_ulan_set_dadr_cmd(uloi_coninfo_t *coninfo, int dadr, int cmd);

#endif /* ULOI_COM_INLINE */

#if defined(ULOI_COM_INLINE) || defined(ULOI_COM_INCLUDE_INTERNAL)

uloi_com_inline
ul_fd_t uloi_con_ulan_rdfd(uloi_coninfo_t *coninfo)
{
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  return con_ulan->rdfd;
}

uloi_com_inline
ul_fd_t uloi_con_ulan_wrfd(uloi_coninfo_t *coninfo)
{
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  return con_ulan->wrfd;
}

uloi_com_inline
int uloi_con_ulan_cmd(uloi_coninfo_t *coninfo)
{
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  return con_ulan->cmd;
}

uloi_com_inline
int uloi_con_ulan_bcmd(uloi_coninfo_t *coninfo)
{
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  return con_ulan->bcmd;
}

uloi_com_inline
int uloi_con_ulan_set_cmd_fd(uloi_coninfo_t *coninfo, int cmd,
                                 ul_fd_t rdfd, ul_fd_t wrfd)
{
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  if(!cmd)
    cmd=UL_CMD_OISV;
  con_ulan->cmd=cmd;
  con_ulan->sn=0;
  con_ulan->bsn=0;
  con_ulan->rdfd=rdfd;
  con_ulan->wrfd=wrfd;
 #ifdef CONFIG_ULOI_CON_IO_OPS
  coninfo->io_ops=&uloi_con_io_ops_ulan;
 #endif /*CONFIG_ULOI_CON_IO_OPS*/
  return 0;
}

uloi_com_inline
int uloi_con_ulan_set_outflg(uloi_coninfo_t *coninfo, int outflg)
{
  int prev_outflg;
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  prev_outflg = con_ulan->outflg;
  con_ulan->outflg = outflg;
  return prev_outflg;
}

uloi_com_inline
int uloi_con_ulan_set_dadr_cmd(uloi_coninfo_t *coninfo, int dadr, int cmd)
{
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);
  con_ulan->adr = dadr;
  con_ulan->bcmd = cmd;
  con_ulan->cmd = 0;
  con_ulan->bsn = 0;
  return 0;
}

#endif /*ULOI_COM_INLINE*/

/* definitions of basic uLan OI functions */

uloi_coninfo_t* uloi_open(char *ul_dev_name,int adr,int cmd,
			int bcmd, int timeout);
void uloi_close(uloi_coninfo_t *coninfo);
int uloi_transfer(uloi_coninfo_t *coninfo,
		  uchar *bufin,int lenin,uchar **bufout,int *lenout);
int uloi_set_var(uloi_coninfo_t *coninfo,int oid, void *val, int size);
int uloi_get_var(uloi_coninfo_t *coninfo,int oid, void *meta, int meta_len, void *val, int size);
int uloi_set_var_u2(uloi_coninfo_t *coninfo,int oid,unsigned val);
int uloi_get_var_u2(uloi_coninfo_t *coninfo,int oid,unsigned *val);
int uloi_send_cmd(uloi_coninfo_t *coninfo,int oid);
int uloi_get_oids(uloi_coninfo_t *coninfo,int list,int **oids_list);
int uloi_get_oiddes(uloi_coninfo_t *coninfo,int list, int oid, uchar **poiddespack);
int uloi_get_aoiddes(uloi_coninfo_t *coninfo,int list,char *aoid, uchar **poiddespack);
const uchar *uloi_oiddespack_getloc(const uchar *despack, int strindex);
char *uloi_oiddespack_strdup(const uchar *despack, int strindex);

/* functions for basic uLan OI type manipulations */

int uloi_tb_type2size(uchar *type, int len);
int uloi_tb_buff_rd_signed(signed long *pval, const void *buff, int size);
int uloi_tb_buff_wr_signed(void *buff, int size, signed long val);
int uloi_tb_buff_rd_unsigned(unsigned long *pval, const void *buff, int size);
int uloi_tb_buff_wr_unsigned(void *buff, int size, unsigned long val);
int uloi_tb_buff_rd_float(float *pval, const void *buff, int size);
int uloi_tb_buff_wr_float(void *buff, int size, float val);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _ULOI_COM_H */

