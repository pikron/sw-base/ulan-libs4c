#ifndef _ULOI_DEF_H
#define _ULOI_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

/* definitions of basic uLan OI commands */

#define ULOI_AOID   10	/* name is defined in ASCII for DOIx */
#define ULOI_DOII   12	/* description of input objects */
#define ULOI_DOIO   14	/* description of output objects */
#define ULOI_QOII   16	/* ID numbers of recognized input objects */
#define ULOI_QOIO   18	/* ID numbers of recognized output objects */
#define ULOI_RDRQ   20	/* object values read request */
#define ULOI_DOITAB 22	/* description provided by single table (optional) */
#define ULOI_SNCHK  29	/* check serial number mismatch */
#define ULOI_STATUS 30	/* read instrument status */
#define ULOI_ERRCLR 31	/* clear error status */

/* definitions of basic uLan OI PDO commands */

/* PDO input CID/OID mapping */
#define ULOI_PICO		40
/* PDO output CID/OID mapping */
#define ULOI_POCO		41
/* PDO input/output meta mapping */
#define ULOI_PIOM		42
/* PDO event to CID mapping */
#define ULOI_PEV2C		43
/* PDO data change event mapping */
#define ULOI_PDC2EV             44
/* PDO input/output/meta mapping operations */
#define ULOI_PMAP_CLEAR		45
#define ULOI_PMAP_SAVE		46
/* PDO mask operations */
#define ULOI_PICO_GMASK         47
#define ULOI_PEV2C_GMASK        48

/* Support for array access is controlled by array range metadata */

typedef struct uloi_array_range {
  unsigned int start; /* for type parsing assigned to  0 */
  unsigned int count; /* for unbound array types  [] the ULOI_ARRAY_COUNT_UNBOUND is used */
} uloi_array_range_t;

#define ULOI_ARRAY_COUNT_UNBOUND ((unsigned int)~0)

/* 
 * The range is serialized as single 16-bit index for single
 * element access for indexes <0,0x7fff>. It is serialized
 * as 16-count with ULOI_ARRAY_USE_ITEM_CNT mask set and 16-bit
 * start index otherwise
 */
#define ULOI_ARRAY_USE_ITEM_CNT 0x8000
#define ULOI_ARRAY_USE_COMMAND  0x4000


#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _ULOI_DEF_H */

