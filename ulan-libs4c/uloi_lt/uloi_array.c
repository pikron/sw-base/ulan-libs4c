#include <uloi_base.h>
#include <uloi_array.h>
#include <string.h>
#include <limits.h>

int uloi_array_rdrange(ULOI_PARAM_coninfo unsigned int *from_idx,
                       unsigned int *item_cnt)
{
  unsigned int arg;
  int res;
  res = uloi_rd_uint(ULOI_ARG_coninfo &arg);
  if (res < 0)
    return res;
  if(!(arg & ULOI_ARRAY_USE_ITEM_CNT)) {
    *from_idx = arg;
    *item_cnt = 1;
    return 0;
  }
  arg &= ~ULOI_ARRAY_USE_ITEM_CNT;
  if(!(arg & ULOI_ARRAY_USE_COMMAND)) {
    *item_cnt = arg;
    res = uloi_rd_uint(ULOI_ARG_coninfo &arg);
    if (res < 0)
      return res;
    *from_idx = arg;
    return 0;
  }
  arg &= ~ULOI_ARRAY_USE_COMMAND;
  return arg;
}

int uloi_array_wrrange(ULOI_PARAM_coninfo const unsigned int *from_idx,
                       const unsigned int *item_cnt)
{
  int res;
  unsigned int arg;
  if((*item_cnt != 1) || (*from_idx >= ULOI_ARRAY_USE_ITEM_CNT)) {
    arg=(*item_cnt) | ULOI_ARRAY_USE_ITEM_CNT;
    res = uloi_wr_uint(ULOI_ARG_coninfo &arg);
    if(res < 0)
      return res;
  }
  res = uloi_wr_uint(ULOI_ARG_coninfo from_idx);
  if(res < 0)
    return res;
  return 0;
}

int uloi_array_chkrange(uloi_arraydes_t *arraydes, unsigned int *from_idx,
                       unsigned int *item_cnt, int chk_flags)
{
  int ret = 0;
  unsigned int end_idx = arraydes->array_size;

  if(*from_idx > end_idx) {
    *from_idx = end_idx;
    ret = 1;
  }

  if(*item_cnt > end_idx - *from_idx) {
    *item_cnt = end_idx - *from_idx;
    ret = 1;
  }
  return ret;
}

int uloi_array_meta4rdfnc(ULOI_PARAM_coninfo unsigned int *from_idx,
                       unsigned int *item_cnt, unsigned int array_size)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_rdrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);
  if(res < 0)
    return res;

  if(idx >= array_size)
    idx = array_size;
  if(cnt > array_size - idx)
    cnt = array_size - idx;

  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_wrrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);
  if(res < 0)
    return res;

  *from_idx = idx;
  *item_cnt = cnt;

  return 0;
}

int uloi_array_meta4wrfnc(ULOI_PARAM_coninfo unsigned int *from_idx,
                       unsigned int *item_cnt, unsigned int array_size)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_rdrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);
  if(res < 0)
    return res;

  if(idx >= array_size)
    goto size_error;
  if(cnt > array_size - idx)
    goto size_error;

  *from_idx = idx;
  *item_cnt = cnt;

  return 0;

size_error:
  if(!coninfo->error)
    coninfo->error=1;
  return -1;
}

int uloi_array_rdfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  unsigned int save_idx;
  int res;
  uloi_arraydes_t *arraydes = (uloi_arraydes_t *)context;
  char *item_context;

  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_rdrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);
  if(res < 0)
    return res;

  res = uloi_array_chkrange(arraydes, &idx, &cnt, 0);
  if(res < 0) {
    if(!coninfo->error)
      coninfo->error=1;
    return res;
  }

  item_context = (char*)arraydes->item_rdcontext;
  if(arraydes->item_size)
    item_context += idx * arraydes->item_size;
  save_idx = coninfo->array_idx;
  coninfo->array_idx = idx;

  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_wrrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);
  if(res < 0)
    return res;

  while (cnt--) {
    res = uloi_call_objdes_fnc(ULOI_ARG_coninfo arraydes->item_rdfnc, item_context);
    if(res < 0)
      return res;
    coninfo->array_idx++;
    item_context += arraydes->item_size;
  }

  coninfo->array_idx = save_idx;
  return 0;
}

int uloi_array_wrfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  unsigned int save_idx;
  int res;
  uloi_arraydes_t *arraydes = (uloi_arraydes_t *)context;
  char *item_context;

  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_rdrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);
  if(res < 0)
    return res;

  res = uloi_array_chkrange(arraydes, &idx, &cnt, 1);
  if(res < 0) {
    if(!coninfo->error)
      coninfo->error=1;
    return res;
  }

  item_context = (char*)arraydes->item_wrcontext;
  if(arraydes->item_size)
    item_context += idx * arraydes->item_size;
  save_idx = coninfo->array_idx;
  coninfo->array_idx = idx;

  while (cnt--) {
    res = uloi_call_objdes_fnc(ULOI_ARG_coninfo arraydes->item_wrfnc, item_context);
    if(res < 0)
      return res;
    coninfo->array_idx++;
    item_context += arraydes->item_size;
  }

  coninfo->array_idx = save_idx;
  return 0;
}
