#ifndef _ULOI_ARRAY_H
#define _ULOI_ARRAY_H

#include "uloi_base.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ULOI_ARRAY_FL_DYNAMIC   0x0001
#define ULOI_ARRAY_FL_APPEND    0x0002

#define ULOI_ARRAY_OP_RESIZE    1

typedef struct uloi_arraydes_t {
  unsigned int array_size;
  unsigned int item_size;
  int array_fl;
  int (*item_rdfnc)(ULOI_PARAM_coninfo void *context);
  void *item_rdcontext;
  int (*item_wrfnc)(ULOI_PARAM_coninfo void *context);
  void *item_wrcontext;
  int (*array_ops)(struct uloi_arraydes_t *arraydes, int opcode, void *value) UL_FNC_REENTRANT;
} uloi_arraydes_t;


int uloi_array_rdrange(ULOI_PARAM_coninfo unsigned int *from_idx,
                       unsigned int *item_cnt);
int uloi_array_wrrange(ULOI_PARAM_coninfo const unsigned int *from_idx,
                       const unsigned int *item_cnt);
int uloi_array_chkrange(uloi_arraydes_t *arraydes, unsigned int *from_idx,
                       unsigned int *item_cnt, int chk_flags);

int uloi_array_meta4rdfnc(ULOI_PARAM_coninfo unsigned int *from_idx,
                       unsigned int *item_cnt, unsigned int array_size);
int uloi_array_meta4wrfnc(ULOI_PARAM_coninfo unsigned int *from_idx,
                       unsigned int *item_cnt, unsigned int array_size);
                       
int uloi_array_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_array_wrfnc(ULOI_PARAM_coninfo void *context);


#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_ARRAY_H*/
