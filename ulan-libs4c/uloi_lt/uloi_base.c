#include <uloi_base.h>
#include <string.h>
#include <limits.h>

#ifdef UL_WITHOUT_HANDLE
UL_ARGPTRTYPE uchar uloi_tmpbuf[ULOI_TMBUF_LEN];
#endif

/*===========================================================*/

#if 0

#include <ul_gsa.h>
#include <ul_gsacust.h>



GSA_STATIC_CUST_DEC(uloi_objdes_array, uloi_objdes_array_t, uloi_objdes_t, uloi_oid_t,
	array, oid, uloi_oid_cmp_fnc)

inline int
uloi_oid_cmp_fnc(const uloi_oid_t *a, const uloi_oid_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

GSA_STATIC_CUST_IMP(uloi_objdes_array, uloi_objdes_array_t, uloi_objdes_t, uloi_oid_t,
	array, oid, uloi_oid_cmp_fnc, 0)

#else

int
uloi_objdes_array_bsearch_indx (const uloi_objdes_array_t * array,
				const uloi_oid_t key)
{
  unsigned a, b, c;
  int r;
  if (!array->array.items || !array->array.count){
    return -1;
  }
  a = 0;
  b = array->array.count;
  while (1){
    c = (a + b) / 2;
    r = array->array.items[c]->oid - key;
    if (!r)
      break;
    if (r <= 0)
      a = c + 1;
    else
      b = c;
    if (a == b) {
      return -a-1;
    }
  }
  return c;
}

uloi_objdes_t *
uloi_objdes_array_at (const uloi_objdes_array_t * array, unsigned indx)
{
  if (indx >= array->array.count)
    return NULL;
  return array->array.items[indx];
}

uloi_objdes_t *
uloi_objdes_array_find (const uloi_objdes_array_t * array, uloi_oid_t const * key)
{
  int indx;
  indx=uloi_objdes_array_bsearch_indx (array, *key);
  if (indx<0)
    return NULL;
  return array->array.items[indx];
}

unsigned
uloi_objdes_array_find_indx (const uloi_objdes_array_t * array,
				   uloi_oid_t const * key)
{
  int indx;
  indx=uloi_objdes_array_bsearch_indx (array, *key);
  if (indx<0)
    return -1;
  return indx;
}

unsigned
uloi_objdes_array_find_after_indx (const uloi_objdes_array_t * array,
				   uloi_oid_t const * key)
{
  int indx;
  indx=uloi_objdes_array_bsearch_indx (array, *key);
  if(indx>=0)
    indx++;
  else
    indx=-(indx+1);
  return indx;
}

#endif

/*===========================================================*/

#if defined(SDCC) || defined(__SDCC)

/* This is catastrophic. SDCC should understand, that function which accept
   parameters only over registers does not need to be reentrant for call over pointer */

int uloi_call_objdes_fnc(int (*fnc)(ULOI_PARAM_coninfo void *context), void *context) __naked
{
  __asm
	push dpl
	push dph
    #ifdef __SDCC_MODEL_LARGE
	mov	dptr,#_uloi_call_objdes_fnc_PARM_2
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	dpl,r2
	mov	dph,r3
	mov	b,a
    #else /*SDCC_MODEL_LARGE*/
	mov	dpl,_uloi_call_objdes_fnc_PARM_2+0
	mov	dph,_uloi_call_objdes_fnc_PARM_2+1
	mov	b,_uloi_call_objdes_fnc_PARM_2+2
    #endif /*SDCC_MODEL_LARGE*/
	ret
  __endasm;
}


#endif /*SDCC*/

/*===========================================================*/

int uloi_i_read_str(ULOI_PARAM_coninfo void UL_ARGPTRTYPE *str, uchar max_len)
{
  uchar len;
  if(coninfo->error)
    return -1;
  if(uloi_i_read(ULOI_ARG_coninfo str, 1)!=1)
    goto conerr;
  if(coninfo->error)
    goto conerr;
  if(max_len<(len=*(uchar*)str))
    goto conerr;
  if(uloi_i_read(ULOI_ARG_coninfo str, len)!=len)
    goto conerr;
  if(coninfo->error)
    goto conerr;
  return len;
  
  conerr:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

int uloi_o_reply_oid(ULOI_PARAM_coninfo uloi_oid_t oid)
{
  int ret;
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[2];
 #endif

  if(coninfo->error)
    return -1;

  if(!(coninfo->state&ULOI_CONINFO_OUTOPEN))
    ret=uloi_o_open(ULOI_ARG1_coninfo);

  if(coninfo->error)
    return -1;

  uloi_tmpbuf[0]=oid;
  uloi_tmpbuf[1]=oid>>8;

  if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf, 2)!=2){
    uloi_o_close(ULOI_ARG1_coninfo);
    coninfo->error=1;
    return -1;
  }
  return 1;
}

int uloi_i_resolve_objdes(ULOI_PARAM_coninfo uloi_objdes_array_t *objdes_array, uloi_objdes_t **pobjdes)
{
  int ret;
  uloi_oid_t oid;
  uloi_objdes_t *objdes;
  unsigned indx;
  uchar len;
  const uchar *p;
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[ULOI_TMBUF_LEN];
 #endif

  if(coninfo->error)
    return -1;

  ret=uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 2);
  if(coninfo->error)
    goto conerr;

  if(!ret){
    *pobjdes=NULL;
    return 0;
  }

  if(ret!=2)
    goto conerr;

  oid=uloi_tmpbuf[0]+((unsigned)uloi_tmpbuf[1]<<8);

  if(!oid){
    *pobjdes=NULL;
    return 0;
  }

  if(oid==ULOI_AOID){
    /* read ascii specified OID */
    len=uloi_i_read_str(ULOI_ARG_coninfo uloi_tmpbuf, ULOI_TMBUF_LEN);
    if(coninfo->error)
      goto conerr;

    indx=0;
    while((objdes=uloi_objdes_array_at(objdes_array,indx++))){
      if(!(p=objdes->aoid))
	continue;
      if(p[0]<3)
	continue;
      if(p[1]!=len)
	continue;
      if(!memcmp(p+2,uloi_tmpbuf,len))
	break;
    }
  }else{
    objdes=uloi_objdes_array_find(objdes_array, &oid);
  }

  if(!objdes)
    goto conerr;
  
  *pobjdes=objdes;
  return 1;

  conerr:
    *pobjdes=NULL;
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

int uloi_i_resolve_objdesindx(ULOI_PARAM_coninfo uloi_objdes_array_t *objdes_array)
{
  int ret;
  uloi_oid_t oid;
  uloi_objdes_t *objdes;
  unsigned indx;
  uchar len;
  const uchar *p;
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[ULOI_TMBUF_LEN];
 #endif

  if(coninfo->error)
    return -1;

  ret=uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 2);
  if(coninfo->error || (ret!=2))
    goto conerr;

  oid=uloi_tmpbuf[0]+((unsigned)uloi_tmpbuf[1]<<8);

  if(!oid){
    return 0;
  }

  if(oid==ULOI_AOID){
    /* read ascii specified OID */
    len=uloi_i_read_str(ULOI_ARG_coninfo uloi_tmpbuf, ULOI_TMBUF_LEN);
    if(coninfo->error)
      goto conerr;

    indx=0;
    while((objdes=uloi_objdes_array_at(objdes_array,indx++))){
      if(!(p=objdes->aoid))
	continue;
      if(p[0]<3)
	continue;
      if(p[1]!=len)
	continue;
      if(!memcmp(p+2,uloi_tmpbuf,len))
	break;
    }
  }else{
    indx=uloi_objdes_array_find_indx(objdes_array, &oid);
  }

  return indx;

  conerr:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

int uloi_i_read_count(ULOI_PARAM1_coninfo)
{
  int ret;
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[2];
 #endif

  if(coninfo->error)
    return -1;

  ret=uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 2);
  if(coninfo->error)
    goto conerr;

  if(ret!=2)
    goto conerr;

  return uloi_tmpbuf[0]+(uloi_tmpbuf[1]<<8);

  conerr:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

/*===========================================================*/

int uloi_process_msg(ULOI_PARAM_coninfo uloi_objdes_array_t *objdes_array, UL_ARGPTRTYPE ul_msginfo *imsginfo)
{
  int ret;
  uloi_objdes_t *objdes;

  coninfo->state=0;
  coninfo->error=0;

  ret=uloi_i_open(ULOI_ARG_coninfo imsginfo);
  if(coninfo->error)
    return -1;

  while(!coninfo->error){
    ret=uloi_i_resolve_objdes(ULOI_ARG_coninfo objdes_array, &objdes);

    if(ret<=0){
      break;
    }

    if(!objdes->wrfnc){
      coninfo->error=1;
      break;
    }

    /*So simple, but SDCC cannot cope without reentrant there,
      but overhead of reentrant function code is unacceptable there */
    /*objdes->wrfnc(ULOI_ARG_coninfo objdes->wrcontext);*/
    uloi_call_objdes_fnc(ULOI_ARG_coninfo objdes->wrfnc, objdes->wrcontext);
  }

  if(coninfo->state&ULOI_CONINFO_OUTOPEN)
    uloi_o_close(ULOI_ARG1_coninfo);

  uloi_i_close(ULOI_ARG1_coninfo);

  return 0;
}

int uloi_rdrq_fnc(ULOI_PARAM_coninfo void *context)
{
  uloi_objdes_array_t *objdes_array=(uloi_objdes_array_t *)context;
  int ret;
  uloi_objdes_t *objdes;

  if(uloi_o_reply_oid(ULOI_ARG_coninfo ULOI_RDRQ+1)<0)
    return -1;

  while(!coninfo->error){
    ret=uloi_i_resolve_objdes(ULOI_ARG_coninfo objdes_array, &objdes);

    if(ret<=0){
      break;
    }

    if(uloi_o_reply_oid(ULOI_ARG_coninfo objdes->oid)<0)
      break;

    if(!objdes->rdfnc){
      coninfo->error=1;
      break;
    }

    /*So simple, but SDCC cannot cope without reentrant there,
      but overhead of reentrant function code is unacceptable there */
    /*objdes->rdfnc(ULOI_ARG_coninfo objdes->rdcontext);*/
    uloi_call_objdes_fnc(ULOI_ARG_coninfo objdes->rdfnc, objdes->rdcontext);
  }

  uloi_o_reply_oid(ULOI_ARG_coninfo 0);

  if(coninfo->state&ULOI_CONINFO_OUTOPEN)
    uloi_o_close(ULOI_ARG1_coninfo);

  return 1;
}


int uloi_process_doix(ULOI_PARAM_coninfo uloi_objdes_array_t *objdes_array, uloi_oid_t reply_oid, unsigned flags)
{
  int ret;
  uloi_objdes_t *objdes;
  int len;
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[1];
 #endif

  ret=uloi_i_resolve_objdes(ULOI_ARG_coninfo objdes_array, &objdes);
  if(ret<=0)
    goto conerr;

  if((flags&1) && !objdes->rdfnc)
    goto conerr;

  if((flags&2) && !objdes->wrfnc)
    goto conerr;

  if(uloi_o_reply_oid(ULOI_ARG_coninfo reply_oid)<0)
    goto conerr;

  if(uloi_o_reply_oid(ULOI_ARG_coninfo objdes->oid)<0)
    goto conerr;

  if(!objdes->aoid){
    uloi_tmpbuf[0]=0;
    if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf, 1)!=1)
      goto conerr;
  }else{
    len=*(uchar*)objdes->aoid+1;
    if(uloi_o_write(ULOI_ARG_coninfo objdes->aoid, len)!=len)
      goto conerr;
  }

  return 1;

  conerr:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

int uloi_doii_fnc(ULOI_PARAM_coninfo void *context)
{
  uloi_objdes_array_t *objdes_array=(uloi_objdes_array_t *)context;
  return uloi_process_doix(ULOI_ARG_coninfo objdes_array, ULOI_DOII+1, 2);
}

int uloi_doio_fnc(ULOI_PARAM_coninfo void *context)
{
  uloi_objdes_array_t *objdes_array=(uloi_objdes_array_t *)context;
  return uloi_process_doix(ULOI_ARG_coninfo objdes_array, ULOI_DOIO+1, 1);
}


int uloi_process_qoix(ULOI_PARAM_coninfo uloi_objdes_array_t *objdes_array, uloi_oid_t reply_oid, unsigned flags)
{
  int indx;
  int count;
  uloi_objdes_t *objdes;

  indx=uloi_i_resolve_objdesindx(ULOI_ARG_coninfo objdes_array);

  if(indx<0){
    goto conerr;
  }

  if(uloi_o_reply_oid(ULOI_ARG_coninfo reply_oid)<0)
    goto conerr;

  if((count=uloi_i_read_count(ULOI_ARG1_coninfo))<0)
    goto conerr;

  while(count&&(objdes=uloi_objdes_array_at(objdes_array,indx++))){
    if((flags&1) && !objdes->rdfnc)
      continue;

    if((flags&2) && !objdes->wrfnc)
      continue;

    if(uloi_o_reply_oid(ULOI_ARG_coninfo objdes->oid)<0)
      goto conerr;

    count--;
  }

  if(uloi_o_reply_oid(ULOI_ARG_coninfo 0)<0)
    goto conerr;

  return 1;

  conerr:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

int uloi_qoii_fnc(ULOI_PARAM_coninfo void *context)
{
  uloi_objdes_array_t *objdes_array=(uloi_objdes_array_t *)context;
  return uloi_process_qoix(ULOI_ARG_coninfo objdes_array, ULOI_QOII+1, 2);
}

int uloi_qoio_fnc(ULOI_PARAM_coninfo void *context)
{
  uloi_objdes_array_t *objdes_array=(uloi_objdes_array_t *)context;
  return uloi_process_qoix(ULOI_ARG_coninfo objdes_array, ULOI_QOIO+1, 1);
}
