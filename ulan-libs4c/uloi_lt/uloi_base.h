#ifndef _ULOI_BASE_H
#define _ULOI_BASE_H

#include <stdlib.h>
#include <ul_lib/ulan.h>
#include <uloi_com.h>

#include "uloi_lib_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef UL_ARGPTRTYPE
#define UL_ARGPTRTYPE
#endif

#ifndef NULL_FNC
#if !defined(SDCC) && !defined(__SDCC)
  #define ULOI_CODE
  #define NULL_CODE NULL
#else /*SDCC*/
  #define ULOI_CODE __code
  #define NULL_CODE (0)
#endif /*SDCC*/
#endif /*NULL_FNC*/

#ifndef ULOI_GENOBJIDTAG
  #ifdef CONFIG_ULOI_GENOBJIDTAG
    #define ULOI_GENOBJIDTAG(_name,_oid) \
      void uloid_objidtag_note_##_name () __attribute__ ((alias (#_oid)));
      /*__NUM2ASM(uloid_objidtag_##_name,_oid)*/
  #endif /*CONFIG_ULOI_GENOBJIDTAG*/
#endif /*ULOI_GENOBJIDTAG*/

#ifndef ULOI_GENOBJIDTAG
  #define ULOI_GENOBJIDTAG(_name,_oid)
#endif /*ULOI_GENOBJIDTAG*/


#define ULOI_TMBUF_LEN 30

#ifdef UL_WITHOUT_HANDLE
  #define ULOI_ARG_coninfo
  #define ULOI_ARG1_coninfo
  #define ULOI_PARAM_coninfo
  #define ULOI_PARAM1_coninfo
  #define coninfo (&uloi_con_ulan_global.con)
  extern uloi_con_ulan_t uloi_con_ulan_global;
  extern UL_ARGPTRTYPE uchar uloi_tmpbuf[ULOI_TMBUF_LEN];
#else  /*UL_WITHOUT_HANDLE*/
  #define ul_ssize_t ssize_t
  #define ULOI_ARG_coninfo coninfo,
  #define ULOI_ARG1_coninfo coninfo
  #define ULOI_PARAM_coninfo uloi_coninfo_t *coninfo,
  #define ULOI_PARAM1_coninfo uloi_coninfo_t *coninfo
#endif /*UL_WITHOUT_HANDLE*/

#define ULOI_CONINFO_OUTOPEN 1
#define ULOI_CONINFO_CMPDIFF 2

typedef unsigned uloi_oid_t;
typedef unsigned uloi_cid_t;
typedef unsigned long uloi_gmask_t;

#define ULOI_GMASK_ALL  (~(uloi_gmask_t)0)
#define ULOI_GMASK_NONE 0

#define ULOI_CID_CODE_GMASK  0xfff1

typedef struct uloi_objdes_t {
  uloi_oid_t oid;
  const ULOI_CODE void *aoid;
  int (*rdfnc)(ULOI_PARAM_coninfo void *context);
  void *rdcontext;
  int (*wrfnc)(ULOI_PARAM_coninfo void *context);
  void *wrcontext;
} uloi_objdes_t;

#define ULOI_GENAOID2(_name,_type) \
  const ULOI_CODE struct {char len_all; \
          char len_name; \
	  char val_name[sizeof(#_name)-1]; \
          char len_type; \
	  char val_type[sizeof(_type)-1]; \
  } uloi_aoid_##_name={ \
    sizeof(#_name)+sizeof(_type), \
    sizeof(#_name)-1, \
    #_name, \
    sizeof(_type)-1, \
    _type \
  };

#define ULOI_GENOBJDES_RAW(_oid,_aoid,_rdfnc,_rdcontext,_wrfnc,_wrcontext) \
{ \
  _oid, \
  _aoid, \
  _rdfnc, \
  _rdcontext, \
  _wrfnc, \
  _wrcontext \
};

#define ULOI_GENOBJDES(_name,_oid,_type,_rdfnc,_rdcontext,_wrfnc,_wrcontext) \
ULOI_GENAOID2(_name,_type) \
const ULOI_CODE uloi_objdes_t uloid_objdes_##_name = \
ULOI_GENOBJDES_RAW(_oid,&uloi_aoid_##_name,_rdfnc,_rdcontext,_wrfnc,_wrcontext) \
ULOI_GENOBJIDTAG(_name,_oid)

#define ULOI_GENOBJDES_STD(_name,_oid,_type,_rdfnc,_rdcontext,_wrfnc,_wrcontext) \


#if !defined(SDCC) && !defined(__SDCC)

static inline
int uloi_call_objdes_fnc(ULOI_PARAM_coninfo int (*fnc)(ULOI_PARAM_coninfo void *context), void *context)
{
  return fnc(ULOI_ARG_coninfo context);
}

#else /*SDCC*/

int uloi_call_objdes_fnc(ULOI_PARAM_coninfo int (*fnc)(ULOI_PARAM_coninfo void *context), void *context) __naked;

#endif /*SDCC*/

typedef struct uloi_objdes_array_t {
  /*gsa_array_field_t;*/
  struct {
    uloi_objdes_t **items;
    unsigned count;
    unsigned alloc_count;
  } array;
} uloi_objdes_array_t;


uloi_objdes_t *
uloi_objdes_array_at (const uloi_objdes_array_t * array, unsigned indx);

uloi_objdes_t *
uloi_objdes_array_find (const uloi_objdes_array_t * array, uloi_oid_t const * key);

unsigned
uloi_objdes_array_find_indx (const uloi_objdes_array_t * array,
				   uloi_oid_t const * key);

unsigned
uloi_objdes_array_find_after_indx (const uloi_objdes_array_t * array,
				   uloi_oid_t const * key);


int uloi_i_open(ULOI_PARAM_coninfo UL_ARGPTRTYPE ul_msginfo *imsginfo);
int uloi_o_open(ULOI_PARAM1_coninfo);
int uloi_i_close(ULOI_PARAM1_coninfo);
int uloi_o_close(ULOI_PARAM1_coninfo);
ul_ssize_t uloi_i_read(ULOI_PARAM_coninfo void *buffer, size_t size);
ul_ssize_t uloi_o_write(ULOI_PARAM_coninfo const void *buffer, size_t size);
int uloi_set_io_mode(ULOI_PARAM_coninfo int mode);

#define ULOI_SET_IO_MODE_UNSPEC 0
#define ULOI_SET_IO_MODE_DATA   1
#define ULOI_SET_IO_MODE_META   2

int uloi_o_reply_oid(ULOI_PARAM_coninfo uloi_oid_t oid);

int uloi_process_msg(ULOI_PARAM_coninfo uloi_objdes_array_t *objdes_array, UL_ARGPTRTYPE ul_msginfo *imsginfo);

int uloi_doii_fnc(ULOI_PARAM_coninfo void *context);
int uloi_doio_fnc(ULOI_PARAM_coninfo void *context);
int uloi_qoii_fnc(ULOI_PARAM_coninfo void *context);
int uloi_qoio_fnc(ULOI_PARAM_coninfo void *context);
int uloi_rdrq_fnc(ULOI_PARAM_coninfo void *context);

int uloi_snchk_fnc(ULOI_PARAM_coninfo void *context);

int uloi_char_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_char_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_uchar_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_uchar_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_int_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_int_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_uint_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_uint_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_long_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_long_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_ulong_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_ulong_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_float_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_float_wrfnc(ULOI_PARAM_coninfo void *context);

#if !defined(SDCC) && !defined(__SDCC)

/* Generic typed functions generation
   int uloi_([\w]*)_wrfnc\(ULOI_PARAM_coninfo void \*context\);
   static inline int uloi_rd_\1(ULOI_PARAM_coninfo \1 *pval)\n\t{return uloi_\1_wrfnc(ULOI_ARG_coninfo pval);}
   int uloi_([\w]*)_rdfnc\(ULOI_PARAM_coninfo void \*context\);
   static inline int uloi_wr_\1(ULOI_PARAM_coninfo const \1 *pval)\n\t{return uloi_\1_rdfnc(ULOI_ARG_coninfo (void*)pval);}
*/

static inline int uloi_wr_char(ULOI_PARAM_coninfo const char *pval)
	{return uloi_char_rdfnc(ULOI_ARG_coninfo (void*)pval);}
static inline int uloi_rd_char(ULOI_PARAM_coninfo char *pval)
	{return uloi_char_wrfnc(ULOI_ARG_coninfo pval);}
static inline int uloi_wr_uchar(ULOI_PARAM_coninfo const uchar *pval)
	{return uloi_uchar_rdfnc(ULOI_ARG_coninfo (void*)pval);}
static inline int uloi_rd_uchar(ULOI_PARAM_coninfo uchar *pval)
	{return uloi_uchar_wrfnc(ULOI_ARG_coninfo pval);}
static inline int uloi_wr_int(ULOI_PARAM_coninfo const int *pval)
	{return uloi_int_rdfnc(ULOI_ARG_coninfo (void*)pval);}
static inline int uloi_rd_int(ULOI_PARAM_coninfo int *pval)
	{return uloi_int_wrfnc(ULOI_ARG_coninfo pval);}
static inline int uloi_wr_uint(ULOI_PARAM_coninfo const uint *pval)
	{return uloi_uint_rdfnc(ULOI_ARG_coninfo (void*)pval);}
static inline int uloi_rd_uint(ULOI_PARAM_coninfo uint *pval)
	{return uloi_uint_wrfnc(ULOI_ARG_coninfo pval);}
static inline int uloi_wr_long(ULOI_PARAM_coninfo const long *pval)
	{return uloi_long_rdfnc(ULOI_ARG_coninfo (void*)pval);}
static inline int uloi_rd_long(ULOI_PARAM_coninfo long *pval)
	{return uloi_long_wrfnc(ULOI_ARG_coninfo pval);}
static inline int uloi_wr_ulong(ULOI_PARAM_coninfo const ulong *pval)
	{return uloi_ulong_rdfnc(ULOI_ARG_coninfo (void*)pval);}
static inline int uloi_rd_ulong(ULOI_PARAM_coninfo ulong *pval)
	{return uloi_ulong_wrfnc(ULOI_ARG_coninfo pval);}
static inline int uloi_wr_float(ULOI_PARAM_coninfo const float *pval)
	{return uloi_float_rdfnc(ULOI_ARG_coninfo (void*)pval);}
static inline int uloi_rd_float(ULOI_PARAM_coninfo float *pval)
	{return uloi_float_wrfnc(ULOI_ARG_coninfo pval);}

#else /*SDCC*/

/* Generic macros for dumb compilers
   int uloi_([\w]*)_wrfnc\(ULOI_PARAM_coninfo void \*context\);
   #define uloi_rd_\1 uloi_\1_wrfnc
   int uloi_([\w]*)_rdfnc\(ULOI_PARAM_coninfo void \*context\);
   #define uloi_wr_\1 uloi_\1_rdfnc
*/

#define uloi_wr_char uloi_char_rdfnc
#define uloi_rd_char uloi_char_wrfnc
#define uloi_wr_uchar uloi_uchar_rdfnc
#define uloi_rd_uchar uloi_uchar_wrfnc
#define uloi_wr_int uloi_int_rdfnc
#define uloi_rd_int uloi_int_wrfnc
#define uloi_wr_uint uloi_uint_rdfnc
#define uloi_rd_uint uloi_uint_wrfnc
#define uloi_wr_long uloi_long_rdfnc
#define uloi_rd_long uloi_long_wrfnc
#define uloi_wr_ulong uloi_ulong_rdfnc
#define uloi_rd_ulong uloi_ulong_wrfnc
#define uloi_wr_float uloi_float_rdfnc
#define uloi_rd_float uloi_float_wrfnc

#endif /*SDCC*/

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_BASE_H*/
