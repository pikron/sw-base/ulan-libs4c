#include <uloi_base.h>
#include <uloi_doitab.h>
#include <uloi_array.h>
#include <string.h>
#include <limits.h>

#define ULOI_DOITAB_TABHEAD_LEN 4
#define ULOI_DOITAB_DESHEAD_LEN 5

/*
typedef struct uloi_doitab_seek_cache_t {

} uloi_doitab_seek_cache_t;
*/

int uloi_doitab_seek_objdesindx(const uloi_objdes_array_t *objdes_array,
                                unsigned int *pobjdesindx, unsigned  int *pobjdespos, unsigned int seekpos)
{
  int ret;
  uloi_objdes_t *objdes;
  unsigned int objdesindx=*pobjdesindx;
  unsigned int objdespos=*pobjdespos;
  const uchar *p;

  do {
    unsigned int objdeslen=ULOI_DOITAB_DESHEAD_LEN;

    objdes=uloi_objdes_array_at(objdes_array,objdesindx);
    if(!objdes) {
      ret=0;
      break;
    }

    if((p=objdes->aoid)!=NULL)
      objdeslen+=*p;

    if(objdespos+objdeslen>seekpos) {
      ret=1;
      break;
    }

    objdesindx++;
    objdespos+=objdeslen;
  } while(1);

  *pobjdesindx=objdesindx;
  *pobjdespos=objdespos;
  return ret;
}

int uloi_doitab_rdfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  unsigned int end;
  unsigned int offs;
  int res;
  unsigned int objdesindx;
  unsigned int objdespos;
  unsigned int len;
  uloi_doitabdes_t *doitabdes = (uloi_doitabdes_t *)context;
  const uloi_objdes_array_t *objdes_array=doitabdes->objdes_array;

  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_rdrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);

  if(res < 0)
    return res;
  if(res!=0)
    goto conerr;

  end=idx+cnt;
  if(end<idx)
    end=(unsigned int)(0-1);

  objdesindx=0;
  objdespos=ULOI_DOITAB_TABHEAD_LEN;

  if(end>objdespos) {
    unsigned int objdesindx1;
    unsigned int objdespos1;
    res=uloi_doitab_seek_objdesindx(objdes_array, &objdesindx, &objdespos, idx);
    objdespos1=objdespos;
    objdesindx1=objdesindx;
    if(res>0)
      res=uloi_doitab_seek_objdesindx(objdes_array, &objdesindx1, &objdespos1, end);
    if(res<=0){
      if(objdespos1<idx)
        cnt=0;
      else
        cnt=objdespos1-idx;
    }
  }

  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_META);
  res = uloi_array_wrrange(ULOI_ARG_coninfo &idx, &cnt);
  uloi_set_io_mode(ULOI_ARG_coninfo ULOI_SET_IO_MODE_DATA);
  if(res < 0)
    return res;

  if(!cnt)
    return 0;

  if(idx<ULOI_DOITAB_TABHEAD_LEN) {
   #ifndef UL_WITHOUT_HANDLE
    uchar uloi_tmpbuf[ULOI_DOITAB_TABHEAD_LEN];
   #endif
    memset(uloi_tmpbuf, 0, ULOI_DOITAB_TABHEAD_LEN);
    uloi_tmpbuf[0]=ULOI_DOITAB_TABHEAD_LEN-1;
    len=ULOI_DOITAB_TABHEAD_LEN-idx;
    if(len>cnt)
      len=cnt;
    if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf+idx, len)!=len)
      goto conerr;
    cnt-=len;
    idx+=len;
  }

  for (offs=idx-objdespos; cnt; offs=0, objdesindx++) {
    const uchar *p;
    uloi_objdes_t *objdes=uloi_objdes_array_at(objdes_array,objdesindx);
   #ifndef UL_WITHOUT_HANDLE
    uchar uloi_tmpbuf[ULOI_DOITAB_DESHEAD_LEN];
   #endif

    if(objdes==NULL)
      goto conerr;

    p=objdes->aoid;

    if(offs<ULOI_DOITAB_DESHEAD_LEN) {
      uloi_tmpbuf[0]=0;
      if(objdes->rdfnc)
        uloi_tmpbuf[0]|=1; /* Flag to mark read access */
      if(objdes->wrfnc)
        uloi_tmpbuf[0]|=2; /* Flag to mark write access */
      uloi_tmpbuf[1]=0;
      uloi_tmpbuf[2]=objdes->oid;	/* OID */
      uloi_tmpbuf[3]=objdes->oid>>8;
      uloi_tmpbuf[4]=(p!=NULL)?*p:0;	/* description length */
      len=ULOI_DOITAB_DESHEAD_LEN-offs;
      if(len>cnt)
        len=cnt;
      if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf+offs, len)!=len)
        goto conerr;
      cnt-=len;
      if(!cnt)
        break;
      offs=0;
    } else {
      offs-=ULOI_DOITAB_DESHEAD_LEN;
    }
    if(p==NULL)
      continue;

    len=*p-offs;
    p+=offs+1;
    if(len>cnt)
      len=cnt;
    if(uloi_o_write(ULOI_ARG_coninfo p, len)!=len)
      goto conerr;
    cnt-=len;
  }

  return 0;

  conerr:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

int uloi_doitab_fnc(ULOI_PARAM_coninfo void *context)
{
  int ret=1;

  if(uloi_o_reply_oid(ULOI_ARG_coninfo ULOI_DOITAB+1)<0)
    return -1;

  if(uloi_doitab_rdfnc(ULOI_ARG_coninfo context)) {
    ret=-1;
  }

  if(coninfo->state&ULOI_CONINFO_OUTOPEN)
    uloi_o_close(ULOI_ARG1_coninfo);

  return ret;
}
