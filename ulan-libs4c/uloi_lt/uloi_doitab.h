#ifndef _ULOI_DOITAB_H
#define _ULOI_DOITAB_H

#include <uloi_base.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct uloi_doitabdes_t {
  const uloi_objdes_array_t *objdes_array;
} uloi_doitabdes_t;

int uloi_doitab_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_doitab_fnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_DOITAB_H*/
