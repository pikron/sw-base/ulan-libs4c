#include <string.h>
#include <uloi_base.h>
#include <uloi_dynoids.h>

#define ULOI_OBJDES_ALLOC_STEP 8
#define ULOI_OBJDES_DEALLOC_STEP 32

int
uloi_objdes_array_is_empty (const uloi_objdes_array_t * array)
{
  return !array->array.count;
}

unsigned
uloi_objdes_array_first_indx (const uloi_objdes_array_t * array)
{
  return 0;
}

unsigned
uloi_objdes_array_last_indx (const uloi_objdes_array_t * array)
{
  return array->array.count - 1;
}

int
uloi_objdes_array_insert_at (uloi_objdes_array_t * array,
			     uloi_objdes_t * item, unsigned indx)
{
  unsigned acnt=array->array.alloc_count;
  unsigned cnt=array->array.count;
  uloi_objdes_t **items, **p;
  if(indx>cnt) indx=cnt;
  if((cnt+1>=acnt)||!array->array.items)
  {
    if(!array->array.items || !acnt){
      acnt=cnt+ULOI_OBJDES_ALLOC_STEP;
      items=malloc(acnt*sizeof(void*));
      if(array->array.items && items)
        memcpy(items,array->array.items,cnt*sizeof(void*));
    }else{
      if(acnt/4>ULOI_OBJDES_ALLOC_STEP)
	acnt+=acnt/4;
      else
	acnt+=ULOI_OBJDES_ALLOC_STEP;
      items=realloc(array->array.items,acnt*sizeof(void*));
    }
    if(!items) return -1;
    array->array.alloc_count=acnt;
    array->array.items=items;
  }
  else items=array->array.items;
  p=items+indx;
  memmove(p+1,p,(char*)(items+cnt)-(char*)p);
  array->array.count=cnt+1;
  *p=item;
  return 0;
}

int
uloi_objdes_array_delete_at (uloi_objdes_array_t * array, unsigned indx)
{
  unsigned acnt=array->array.alloc_count;
  unsigned cnt=array->array.count;
  uloi_objdes_t **items=array->array.items;
  uloi_objdes_t **p;
  if(indx>=cnt) return -1;
  if(cnt && !acnt){
    p=malloc(cnt*sizeof(void*));
    if(p){
      memcpy(p,items,cnt*sizeof(void*));
      array->array.alloc_count=acnt=cnt;
      array->array.items=items=p;
    }
  }
  p=items+indx;
  array->array.count=--cnt;
  memmove(p,p+1,(items+cnt-p)*sizeof(void *));
  if(acnt-cnt>ULOI_OBJDES_DEALLOC_STEP+ULOI_OBJDES_ALLOC_STEP)
  {
    acnt-=ULOI_OBJDES_DEALLOC_STEP;
    items=realloc(array->array.items,acnt*sizeof(void*));
    if(items){
      array->array.alloc_count=acnt;
      array->array.items=items;
    }
  }
  return 0;
}

void
uloi_objdes_array_delete_all (uloi_objdes_array_t * array)
{
  if(array->array.items && array->array.alloc_count)
    free(array->array.items);
  array->array.items=NULL;
  array->array.count=0;
  array->array.alloc_count=0;
}

uloi_objdes_t *
uloi_objdes_array_cut_last (uloi_objdes_array_t * array)
{
  if (uloi_objdes_array_is_empty (array))
    return ((void *) 0);
  return (uloi_objdes_t *) array->array.items[--array->array.count];
}

void
uloi_objdes_array_init_array_field (uloi_objdes_array_t * array)
{
  array->array.items=NULL;
  array->array.count=0;
  array->array.alloc_count=0;
}

int
uloi_objdes_array_insert (uloi_objdes_array_t * array, uloi_objdes_t * item)
{
  unsigned indx;
  indx=uloi_objdes_array_find_indx(array, &item->oid);
  if (indx!=(unsigned)-1)
    return -1;
  indx=uloi_objdes_array_find_after_indx(array, &item->oid);
  return uloi_objdes_array_insert_at (array, item, indx);
}

int
uloi_objdes_array_delete (uloi_objdes_array_t * array,
			  const uloi_objdes_t * item)
{
  unsigned indx;
  indx=uloi_objdes_array_find_indx(array, &item->oid);
  if (indx==(unsigned)-1)
    return -1;
  if (uloi_objdes_array_at (array, indx) != item)
    return -1;
  return uloi_objdes_array_delete_at (array, indx);
}

uloi_objdes_t *uloi_objdes_new(void)
{
  uloi_objdes_t *objdes;
  objdes=malloc(sizeof(*objdes));
  if(objdes==NULL)
    return NULL;
  memset(objdes,0,sizeof(*objdes));
  return objdes;
}

int
uloi_objdes_init(uloi_objdes_t *objdes,
  uloi_oid_t a_oid,
  const void *a_aoid,
  int (*a_rdfnc)(ULOI_PARAM_coninfo void *context),
  void *a_rdcontext,
  int (*a_wrfnc)(ULOI_PARAM_coninfo void *context),
  void *a_wrcontext)
{
  if(objdes==NULL)
    return -1;
  objdes->oid=a_oid;
  objdes->aoid=a_aoid;
  objdes->rdfnc=a_rdfnc;
  objdes->rdcontext=a_rdcontext;
  objdes->wrfnc=a_wrfnc;;
  objdes->wrcontext=a_wrcontext;
  return 0;
}

void *uloi_objdes_build_aoid(const char *a_name, unsigned name_len,
			const char *a_type, unsigned type_len)
{
  uchar *aoid;
  aoid=malloc(name_len+type_len+3);
  if(aoid==NULL)
    return NULL;
  aoid[0]=name_len+type_len+2;
  aoid[1]=name_len;
  memcpy(aoid+2,a_name,name_len);
  aoid[2+name_len]=type_len;
  memcpy(aoid+3+name_len,a_type,type_len);
  return aoid;
}

void *uloi_objdes_build_aoid_str(const char *a_name, const char *a_type)
{
  return uloi_objdes_build_aoid(a_name, strlen(a_name),
			a_type, strlen(a_type));
}

uloi_objdes_t *uloi_objdes_new_and_setup(uloi_oid_t a_oid, const char *a_name, const char *a_type,
  int (*a_rdfnc)(ULOI_PARAM_coninfo void *context),
  void *a_rdcontext,
  int (*a_wrfnc)(ULOI_PARAM_coninfo void *context),
  void *a_wrcontext)
{
  uloi_objdes_t *objdes;
  void *aoid=NULL;

  if((a_name!=NULL)||(a_type!=NULL)) {
    aoid=uloi_objdes_build_aoid_str(a_name, a_type);
    if(aoid==NULL)
      return NULL;
  }

  objdes=uloi_objdes_new();
  if(objdes==NULL) {
    if(aoid!=NULL)
      free(aoid);
    return NULL;
  }

  uloi_objdes_init(objdes, a_oid, aoid,
    a_rdfnc, a_rdcontext, a_wrfnc, a_wrcontext);

  return objdes;
}
