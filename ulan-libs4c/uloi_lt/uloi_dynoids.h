#ifndef _ULOI_DYNOIDS_H
#define _ULOI_DYNOIDS_H

#include <uloi_base.h>

#ifdef __cplusplus
extern "C" {
#endif

int
uloi_objdes_array_is_empty (const uloi_objdes_array_t * array);

unsigned
uloi_objdes_array_first_indx (const uloi_objdes_array_t * array);

unsigned
uloi_objdes_array_last_indx (const uloi_objdes_array_t * array);

int
uloi_objdes_array_insert_at (uloi_objdes_array_t * array,
			     uloi_objdes_t * item, unsigned indx);

int
uloi_objdes_array_delete_at (uloi_objdes_array_t * array, unsigned indx);

void
uloi_objdes_array_delete_all (uloi_objdes_array_t * array);

uloi_objdes_t *
uloi_objdes_array_cut_last (uloi_objdes_array_t * array);

void
uloi_objdes_array_init_array_field (uloi_objdes_array_t * array);

int
uloi_objdes_array_insert (uloi_objdes_array_t * array, uloi_objdes_t * item);

int
uloi_objdes_array_delete (uloi_objdes_array_t * array,
			  const uloi_objdes_t * item);

uloi_objdes_t *uloi_objdes_new(void);

int
uloi_objdes_init(uloi_objdes_t *objdes,
  uloi_oid_t a_oid,
  const void *a_aoid,
  int (*a_rdfnc)(ULOI_PARAM_coninfo void *context),
  void *a_rdcontext,
  int (*a_wrfnc)(ULOI_PARAM_coninfo void *context),
  void *a_wrcontext);

void *uloi_objdes_build_aoid(const char *a_name, unsigned name_len,
			const char *a_type, unsigned type_len);

void *uloi_objdes_build_aoid_str(const char *a_name, const char *a_type);

uloi_objdes_t *uloi_objdes_new_and_setup(uloi_oid_t a_oid, const char *a_name, const char *a_type,
  int (*a_rdfnc)(ULOI_PARAM_coninfo void *context),
  void *a_rdcontext,
  int (*a_wrfnc)(ULOI_PARAM_coninfo void *context),
  void *a_wrcontext);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_DYNOIDS_H*/
