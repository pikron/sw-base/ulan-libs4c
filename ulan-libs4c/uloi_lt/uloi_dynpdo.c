#include <string.h>
#include <uloi_base.h>
#include <uloi_pdoproc.h>
#include <uloi_pdoev.h>
#include <uloi_dynpdo.h>

int uloi_ciddes_array_resize(uloi_ciddes_array_t *ciddes_array, unsigned new_alloc)
{
  uloi_ciddes_t **new_items=NULL;
  uloi_ciddes_t *new_data=NULL;
  unsigned new_items_count=0;
  unsigned new_data_count=0;

  if(new_alloc) {
    new_items=malloc(sizeof(*new_items)*new_alloc);
    if(new_items==NULL)
      return -1;
    new_data=malloc(sizeof(*new_data)*new_alloc);
    if(new_data==NULL){
      free(new_items);
      return -1;
    }
  }

  if(ciddes_array->sorted.items!=NULL) {
    new_items_count = ciddes_array->sorted.count;
    if(new_items_count>new_alloc)
      new_items_count=new_alloc;
    memcpy(new_items, ciddes_array->sorted.items, sizeof(*new_items)*new_items_count);
    if(ciddes_array->sorted.alloc_count)
      free(ciddes_array->sorted.items);
  }

  if(ciddes_array->table.data!=NULL) {
    new_data_count = ciddes_array->table.count;
    if(new_data_count>new_alloc)
      new_data_count=new_alloc;
    memcpy(new_data, ciddes_array->table.data, sizeof(*new_data)*new_data_count);
    if(ciddes_array->table.alloc_count)
      free(ciddes_array->table.data);
  }

  ciddes_array->sorted.alloc_count=new_alloc;
  ciddes_array->sorted.count=new_items_count;
  ciddes_array->sorted.items=new_items;

  ciddes_array->table.alloc_count=new_alloc;
  ciddes_array->table.count=new_data_count;
  ciddes_array->table.data=new_data;

  return 0;
}

int uloi_pev2cdes_array_resize(uloi_pev2cdes_array_t *pev2cdes_array, unsigned new_alloc)
{
  uloi_pev2cdes_t *new_data=NULL;
  unsigned new_data_count=0;

  if(new_alloc) {
    new_data=malloc(sizeof(*new_data)*new_alloc);
    if(!new_data)
      return -1;
  }

  if(pev2cdes_array->table.data!=NULL) {
    new_data_count = pev2cdes_array->table.count;
    if(new_data_count>new_alloc)
      new_data_count=new_alloc;
    memcpy(new_data, pev2cdes_array->table.data, sizeof(*new_data)*new_data_count);
    if(pev2cdes_array->table.alloc_count)
      free(pev2cdes_array->table.data);
  }

  pev2cdes_array->table.alloc_count=new_alloc;
  pev2cdes_array->table.count=new_data_count;
  pev2cdes_array->table.data=new_data;

  return 0;
}
