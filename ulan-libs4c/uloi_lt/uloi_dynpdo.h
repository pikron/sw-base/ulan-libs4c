#ifndef _ULOI_DYNPDO_H
#define _ULOI_DYNPDO_H

#include <uloi_base.h>
#include <uloi_pdoproc.h>
#include <uloi_pdoev.h>

#ifdef __cplusplus
extern "C" {
#endif

int uloi_ciddes_array_resize(uloi_ciddes_array_t *ciddes_array, unsigned new_alloc);

int uloi_pev2cdes_array_resize(uloi_pev2cdes_array_t *pev2cdes_array, unsigned new_alloc);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_DYNPDO_H*/
