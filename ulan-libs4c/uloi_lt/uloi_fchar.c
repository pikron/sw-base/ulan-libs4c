#include <uloi_base.h>
#include <limits.h>

int uloi_char_rdfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[1];
 #endif
  uloi_tmpbuf[0]=*(char*)context;
  if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf, 1)!=1){
    coninfo->error=1;
    return -1;
  }
  return 0;
}

int uloi_char_wrfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[1];
 #endif
  if(uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 1)!=1){
    coninfo->error=1;
    return -1;
  }
  *(char*)context=uloi_tmpbuf[0];
  return 0;
}

int uloi_uchar_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_char_rdfnc(ULOI_ARG_coninfo context);
}

int uloi_uchar_wrfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_char_wrfnc(ULOI_ARG_coninfo context);
}
