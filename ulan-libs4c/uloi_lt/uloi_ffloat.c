#include <uloi_base.h>
#include <limits.h>
#include <ulan.h>

int uloi_float_rdfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[4];
 #endif

  uloi_tb_buff_wr_float(uloi_tmpbuf, 4, *(float*)context);

  if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf, 4)!=4){
    coninfo->error=1;
    return -1;
  }
  return 0;
}

int uloi_float_wrfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[4];
 #endif
  if(uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 4)!=4){
    coninfo->error=1;
    return -1;
  }

  uloi_tb_buff_rd_float((float *)context, uloi_tmpbuf, 4);

  return 0;
}
