#include <uloi_base.h>
#include <limits.h>

int uloi_int_rdfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[2];
 #endif
  uloi_tmpbuf[0]=*(int*)context;
  uloi_tmpbuf[1]=*(int*)context>>8;
  if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf, 2)!=2){
    coninfo->error=1;
    return -1;
  }
  return 0;
}

int uloi_int_wrfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[2];
 #endif
  if(uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 2)!=2){
    coninfo->error=1;
    return -1;
  }
 #if INT_MAX>32767
  *(int*)context=uloi_tmpbuf[0]+((int)(signed char)uloi_tmpbuf[1]<<8);
 #else
  *(int*)context=uloi_tmpbuf[0]+(uloi_tmpbuf[1]<<8);
 #endif
  return 0;
}

int uloi_uint_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_int_rdfnc(ULOI_ARG_coninfo context);
}

#if INT_MAX<=32767

int uloi_uint_wrfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_int_wrfnc(ULOI_ARG_coninfo context);
}

#else /*INT_MAX>32767*/

int uloi_uint_wrfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[2];
 #endif
  if(uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 2)!=2){
    coninfo->error=1;
    return -1;
  }
  *(unsigned int*)context=uloi_tmpbuf[0]+(uloi_tmpbuf[1]<<8);
  return 0;
}
#endif /*INT_MAX>32767*/

