#include <uloi_base.h>
#include <limits.h>

int uloi_long_rdfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[4];
 #endif
  uloi_tmpbuf[0]=*(long*)context;
  uloi_tmpbuf[1]=*(long*)context>>8;
  uloi_tmpbuf[2]=*(long*)context>>16;
  uloi_tmpbuf[3]=*(long*)context>>24;
  if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf, 4)!=4){
    coninfo->error=1;
    return -1;
  }
  return 0;
}

int uloi_long_wrfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[4];
 #endif
  if(uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 4)!=4){
    coninfo->error=1;
    return -1;
  }
 #if LONG_MAX>2147483647
  *(long*)context=uloi_tmpbuf[0]+(uloi_tmpbuf[1]<<8)+
                  ((long)uloi_tmpbuf[2]<<16)+((long)(signed char)uloi_tmpbuf[3]<<24);
 #else
  *(long*)context=uloi_tmpbuf[0]+(uloi_tmpbuf[1]<<8)+
                  ((long)uloi_tmpbuf[2]<<16)+((long)uloi_tmpbuf[3]<<24);
 #endif
  return 0;
}

int uloi_ulong_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_long_rdfnc(ULOI_ARG_coninfo context);
}

#if LONG_MAX<=2147483647

int uloi_ulong_wrfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_long_wrfnc(ULOI_ARG_coninfo context);
}

#else /*LONG_MAX>2147483647*/

int uloi_ulong_wrfnc(ULOI_PARAM_coninfo void *context)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[4];
 #endif
  if(uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 2)!=2){
    coninfo->error=1;
    return -1;
  }
  *(unsigned long*)context=uloi_tmpbuf[0]+(uloi_tmpbuf[1]<<8)+
                  ((unsigned long)uloi_tmpbuf[2]<<16)+((unsigned long)uloi_tmpbuf[3]<<24);
  return 0;
}
#endif /*LONG_MAX>2147483647*/





