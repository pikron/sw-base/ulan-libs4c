#include <uloi_base.h>

int uloi_i_open(ULOI_PARAM_coninfo UL_ARGPTRTYPE ul_msginfo *imsginfo)
{
  return coninfo->io_ops->i_open(ULOI_ARG_coninfo imsginfo);
}

int uloi_o_open(ULOI_PARAM1_coninfo)
{
  return coninfo->io_ops->o_open(ULOI_ARG1_coninfo);
}

int uloi_i_close(ULOI_PARAM1_coninfo)
{
  return coninfo->io_ops->i_close(ULOI_ARG1_coninfo);
}

int uloi_o_close(ULOI_PARAM1_coninfo)
{
  return coninfo->io_ops->o_close(ULOI_ARG1_coninfo);
}

ul_ssize_t uloi_i_read(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  return coninfo->io_ops->i_read(ULOI_ARG_coninfo buffer, size);
}

ul_ssize_t uloi_o_write(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  return coninfo->io_ops->o_write(ULOI_ARG_coninfo buffer, size);
}

int uloi_set_io_mode(ULOI_PARAM_coninfo int mode)
{
  return coninfo->io_ops->set_io_mode(ULOI_ARG_coninfo mode);
}
