#include <string.h>
#include <uloi_base.h>
#include <uloi_ioproxy.h>

int uloi_i_open_proxy(ULOI_PARAM_coninfo UL_ARGPTRTYPE ul_msginfo *imsginfo)
{
  return 1;
}

int uloi_o_open_proxy(ULOI_PARAM1_coninfo)
{
  return 1;
}

int uloi_i_close_proxy(ULOI_PARAM1_coninfo)
{
  return 0;
}

int uloi_o_close_proxy(ULOI_PARAM1_coninfo)
{
  return 0;
}

ul_ssize_t uloi_i_read_proxy_con(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  ul_ssize_t ret;
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(coninfo->error)
    return -1;

  if(con_proxy->rd.avail <= 0)
    return 0;
  if(con_proxy->rd.avail < size)
    size = con_proxy->rd.avail;
  con_proxy->rd.avail-=size;

  ret = con_proxy->rd.ref.con->io_ops->i_read(con_proxy->rd.ref.con, buffer, size);
  if(ret < 0)
    coninfo->error = 1;
  return ret;
}

ul_ssize_t uloi_o_write_proxy_con(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  ul_ssize_t ret;
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(coninfo->error)
    return -1;

  if(con_proxy->wr.avail <= 0)
    return 0;
  if(con_proxy->wr.avail < size)
    size = con_proxy->wr.avail;
  con_proxy->wr.avail-=size;

  ret = con_proxy->wr.ref.con->io_ops->o_write(con_proxy->wr.ref.con, buffer, size);
  if(ret < 0)
    coninfo->error = 1;
  return ret;
}

ul_ssize_t uloi_i_read_proxy_mem(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(coninfo->error)
    return -1;

  if(con_proxy->rd.avail <= 0)
    return 0;
  if(con_proxy->rd.avail < size)
    size = con_proxy->rd.avail;
  con_proxy->rd.avail-=size;

  memcpy(buffer, con_proxy->rd.ref.ptr, size);
  con_proxy->rd.ref.ptr = (char*)con_proxy->rd.ref.ptr + size;

  return size;
}

ul_ssize_t uloi_o_write_proxy_mem(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(coninfo->error)
    return -1;

  if(con_proxy->wr.avail <= 0)
    return 0;
  if(con_proxy->wr.avail < size)
    size = con_proxy->wr.avail;
  con_proxy->wr.avail-=size;

  memcpy(con_proxy->wr.ref.ptr, buffer, size);
  con_proxy->wr.ref.ptr = (char*)con_proxy->wr.ref.ptr + size;

  return size;
}

ul_ssize_t uloi_o_write_proxy_cmp_mem(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(coninfo->error)
    return -1;

  if(con_proxy->wr.avail <= 0)
    return 0;
  if(con_proxy->wr.avail < size)
    size = con_proxy->wr.avail;
  con_proxy->wr.avail-=size;

  if (memcmp(con_proxy->wr.ref.ptr, buffer, size)!=0)
    coninfo->state|=ULOI_CONINFO_CMPDIFF;
    
  con_proxy->wr.ref.ptr = (char*)con_proxy->wr.ref.ptr + size;

  return size;
}

ul_ssize_t uloi_i_read_proxy_aux_mem(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(coninfo->error)
    return -1;

  if(con_proxy->aux.avail <= 0)
    return 0;
  if(con_proxy->aux.avail < size)
    size = con_proxy->aux.avail;
  con_proxy->aux.avail-=size;

  memcpy(buffer, con_proxy->aux.ref.ptr, size);
  con_proxy->aux.ref.ptr = (char*)con_proxy->aux.ref.ptr + size;

  return size;
}

ul_ssize_t uloi_i_read_proxy_none(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  if(!coninfo->error)
    coninfo->error = 1;
  return -1;
}

ul_ssize_t uloi_o_write_proxy_none(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  if(!coninfo->error)
    coninfo->error = 1;
  return -1;
}

ul_ssize_t uloi_o_write_proxy_discard(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  if(coninfo->error)
    return -1;
  return size;
}

ul_ssize_t uloi_i_read_proxy_data_con_meta_aux_mem(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(con_proxy->mode == ULOI_SET_IO_MODE_META)
    return uloi_i_read_proxy_aux_mem(ULOI_ARG_coninfo buffer, size);
  else
    return uloi_i_read_proxy_con(ULOI_ARG_coninfo buffer, size);
}

ul_ssize_t uloi_i_read_proxy_data_mem_meta_aux_mem(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(con_proxy->mode == ULOI_SET_IO_MODE_META)
    return uloi_i_read_proxy_aux_mem(ULOI_ARG_coninfo buffer, size);
  else
    return uloi_i_read_proxy_mem(ULOI_ARG_coninfo buffer, size);
}

ul_ssize_t uloi_i_read_proxy_meta_aux_mem_only(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(con_proxy->mode == ULOI_SET_IO_MODE_META)
    return uloi_i_read_proxy_aux_mem(ULOI_ARG_coninfo buffer, size);
  else
    return uloi_i_read_proxy_none(ULOI_ARG_coninfo buffer, size);
}

ul_ssize_t uloi_o_write_proxy_data_con_only(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(con_proxy->mode == ULOI_SET_IO_MODE_META)
    return uloi_o_write_proxy_discard(ULOI_ARG_coninfo buffer, size);
  else
    return uloi_o_write_proxy_con(ULOI_ARG_coninfo buffer, size);
}

ul_ssize_t uloi_o_write_proxy_data_mem_only(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(con_proxy->mode == ULOI_SET_IO_MODE_META)
    return uloi_o_write_proxy_discard(ULOI_ARG_coninfo buffer, size);
  else
    return uloi_o_write_proxy_mem(ULOI_ARG_coninfo buffer, size);
}

ul_ssize_t uloi_o_write_proxy_data_cmp_mem_only(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);

  if(con_proxy->mode == ULOI_SET_IO_MODE_META)
    return uloi_o_write_proxy_discard(ULOI_ARG_coninfo buffer, size);
  else
    return uloi_o_write_proxy_cmp_mem(ULOI_ARG_coninfo buffer, size);
}

int uloi_set_io_mode_proxy(ULOI_PARAM_coninfo int mode)
{
  uloi_con_proxy_t *con_proxy = UL_CONTAINEROF(coninfo, uloi_con_proxy_t, con);
  int prevmode = con_proxy->mode;
  con_proxy->mode = mode;
  return prevmode;
}

const uloi_con_io_ops_t uloi_con_io_ops_proxy_con = {
  uloi_i_open_proxy,
  uloi_o_open_proxy,
  uloi_i_close_proxy,
  uloi_o_close_proxy,
  uloi_i_read_proxy_con,
  uloi_o_write_proxy_con,
  uloi_set_io_mode_proxy
};

const uloi_con_io_ops_t uloi_con_io_ops_proxy_mem = {
  uloi_i_open_proxy,
  uloi_o_open_proxy,
  uloi_i_close_proxy,
  uloi_o_close_proxy,
  uloi_i_read_proxy_mem,
  uloi_o_write_proxy_mem,
  uloi_set_io_mode_proxy
};

const uloi_con_io_ops_t uloi_con_io_ops_proxy_con_read_meta_aux_mem = {
  uloi_i_open_proxy,
  uloi_o_open_proxy,
  uloi_i_close_proxy,
  uloi_o_close_proxy,
  uloi_i_read_proxy_data_con_meta_aux_mem,
  uloi_o_write_proxy_none,
  uloi_set_io_mode_proxy
};

const uloi_con_io_ops_t uloi_con_io_ops_proxy_mem_read_meta_aux_mem = {
  uloi_i_open_proxy,
  uloi_o_open_proxy,
  uloi_i_close_proxy,
  uloi_o_close_proxy,
  uloi_i_read_proxy_data_mem_meta_aux_mem,
  uloi_o_write_proxy_none,
  uloi_set_io_mode_proxy
};

const uloi_con_io_ops_t uloi_con_io_ops_proxy_con_write_meta_aux_mem = {
  uloi_i_open_proxy,
  uloi_o_open_proxy,
  uloi_i_close_proxy,
  uloi_o_close_proxy,
  uloi_i_read_proxy_meta_aux_mem_only,
  uloi_o_write_proxy_data_con_only,
  uloi_set_io_mode_proxy
};

const uloi_con_io_ops_t uloi_con_io_ops_proxy_mem_write_meta_aux_mem = {
  uloi_i_open_proxy,
  uloi_o_open_proxy,
  uloi_i_close_proxy,
  uloi_o_close_proxy,
  uloi_i_read_proxy_meta_aux_mem_only,
  uloi_o_write_proxy_data_mem_only,
  uloi_set_io_mode_proxy
};

const uloi_con_io_ops_t uloi_con_io_ops_proxy_cmp_mem_write_meta_aux_mem = {
  uloi_i_open_proxy,
  uloi_o_open_proxy,
  uloi_i_close_proxy,
  uloi_o_close_proxy,
  uloi_i_read_proxy_meta_aux_mem_only,
  uloi_o_write_proxy_data_cmp_mem_only,
  uloi_set_io_mode_proxy
};

int uloi_con_io_proxy_con_setup(uloi_con_proxy_t *con_proxy,
              uloi_coninfo_t *target_con, int target_avail)
{
  memset(con_proxy, 0, sizeof(*con_proxy));
  con_proxy->con.io_ops = &uloi_con_io_ops_proxy_con;
  con_proxy->rd.ref.con = target_con;
  con_proxy->rd.avail = target_avail;
  con_proxy->wr.ref.con = target_con;
  con_proxy->wr.avail = target_avail;
  con_proxy->aux.ref.ptr = NULL;
  con_proxy->aux.avail = 0;
  return 0;
}

int uloi_con_io_proxy_mem_setup(uloi_con_proxy_t *con_proxy,
              const void *rd_ptr, int rd_avail, void *wr_ptr, int wr_avail)
{
  memset(con_proxy, 0, sizeof(*con_proxy));
  con_proxy->con.io_ops = &uloi_con_io_ops_proxy_mem;
  con_proxy->rd.ref.ptr = rd_ptr;
  con_proxy->rd.avail = rd_avail;
  con_proxy->wr.ref.ptr = wr_ptr;
  con_proxy->wr.avail = wr_avail;
  con_proxy->aux.ref.ptr = NULL;
  con_proxy->aux.avail = 0;
  return 0;
}

int uloi_con_io_proxy_con_read_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              uloi_coninfo_t *rd_con, int rd_avail, const void *aux_ptr, int aux_avail)
{
  memset(con_proxy, 0, sizeof(*con_proxy));
  con_proxy->con.io_ops = &uloi_con_io_ops_proxy_con_read_meta_aux_mem;
  con_proxy->rd.ref.con = rd_con;
  con_proxy->rd.avail = rd_avail;
  con_proxy->wr.ref.ptr = NULL;
  con_proxy->wr.avail = 0;
  con_proxy->aux.ref.ptr = aux_ptr;
  con_proxy->aux.avail = aux_avail;
  return 0;
}

int uloi_con_io_proxy_mem_read_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              const void *rd_ptr, int rd_avail, const void *aux_ptr, int aux_avail)
{
  memset(con_proxy, 0, sizeof(*con_proxy));
  con_proxy->con.io_ops = &uloi_con_io_ops_proxy_mem_read_meta_aux_mem;
  con_proxy->rd.ref.ptr = rd_ptr;
  con_proxy->rd.avail = rd_avail;
  con_proxy->wr.ref.ptr = NULL;
  con_proxy->wr.avail = 0;
  con_proxy->aux.ref.ptr = aux_ptr;
  con_proxy->aux.avail = aux_avail;
  return 0;
}


int uloi_con_io_proxy_con_write_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              uloi_coninfo_t *wr_con, int wr_avail, const void *aux_ptr, int aux_avail)
{
  memset(con_proxy, 0, sizeof(*con_proxy));
  con_proxy->con.io_ops = &uloi_con_io_ops_proxy_con_write_meta_aux_mem;
  con_proxy->rd.ref.ptr = NULL;
  con_proxy->rd.avail = 0;
  con_proxy->wr.ref.con = wr_con;
  con_proxy->wr.avail = wr_avail;
  con_proxy->aux.ref.ptr = aux_ptr;
  con_proxy->aux.avail = aux_avail;
  return 0;
}

int uloi_con_io_proxy_mem_write_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              void *wr_ptr, int wr_avail, const void *aux_ptr, int aux_avail)
{
  memset(con_proxy, 0, sizeof(*con_proxy));
  con_proxy->con.io_ops = &uloi_con_io_ops_proxy_mem_write_meta_aux_mem;
  con_proxy->rd.ref.ptr = NULL;
  con_proxy->rd.avail = 0;
  con_proxy->wr.ref.ptr = wr_ptr;
  con_proxy->wr.avail = wr_avail;
  con_proxy->aux.ref.ptr = aux_ptr;
  con_proxy->aux.avail = aux_avail;
  return 0;
}

int uloi_con_io_proxy_cmp_mem_write_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              const void *wr_ptr, int wr_avail, const void *aux_ptr, int aux_avail)
{
  memset(con_proxy, 0, sizeof(*con_proxy));
  con_proxy->con.io_ops = &uloi_con_io_ops_proxy_cmp_mem_write_meta_aux_mem;
  con_proxy->rd.ref.ptr = NULL;
  con_proxy->rd.avail = 0;
  con_proxy->wr.ref.ptr = wr_ptr;
  con_proxy->wr.avail = wr_avail;
  con_proxy->aux.ref.ptr = aux_ptr;
  con_proxy->aux.avail = aux_avail;
  return 0;
}
