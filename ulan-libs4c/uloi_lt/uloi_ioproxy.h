#ifndef _ULOI_IOPROXY_H
#define _ULOI_IOPROXY_H

#include "uloi_base.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct uloi_con_proxy_target_t {
  union {
    uloi_coninfo_t *con;
    void *ptr;
  } ref;
  int avail;
} uloi_con_proxy_target_t;

typedef struct uloi_con_proxy_target_const_t {
  union {
    uloi_coninfo_t *con;
    const void *ptr;
  } ref;
  int avail;
} uloi_con_proxy_target_const_t;

typedef struct uloi_con_proxy_t {
  uloi_coninfo_t con;
  uloi_con_proxy_target_const_t rd;
  uloi_con_proxy_target_t wr;
  uloi_con_proxy_target_const_t aux;
  int mode;
} uloi_con_proxy_t;

int uloi_con_io_proxy_con_setup(uloi_con_proxy_t *con_proxy,
              uloi_coninfo_t *target_con, int target_avail);

int uloi_con_io_proxy_mem_setup(uloi_con_proxy_t *con_proxy,
              const void *rd_ptr, int rd_avail, void *wr_ptr, int wr_avail);

int uloi_con_io_proxy_con_read_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              uloi_coninfo_t *rd_con, int rd_avail, const void *aux_ptr, int aux_avail);

int uloi_con_io_proxy_mem_read_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              const void *rd_ptr, int rd_avail, const void *aux_ptr, int aux_avail);

int uloi_con_io_proxy_con_write_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              uloi_coninfo_t *wr_con, int wr_avail, const void *aux_ptr, int aux_avail);

int uloi_con_io_proxy_mem_write_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              void *wr_ptr, int wr_avail, const void *aux_ptr, int aux_avail);

int uloi_con_io_proxy_cmp_mem_write_meta_aux_mem_setup(uloi_con_proxy_t *con_proxy,
              const void *wr_ptr, int wr_avail, const void *aux_ptr, int aux_avail);

#ifdef __cplusplus
}
#endif

#endif /*_ULOI_IOPROXY_H*/
