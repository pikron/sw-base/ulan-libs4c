#include <uloi_base.h>

#ifndef CONFIG_ULOI_CON_IO_OPS

#define uloi_i_open_ulan uloi_i_open
#define uloi_o_open_ulan uloi_o_open
#define uloi_i_close_ulan uloi_i_close
#define uloi_o_close_ulan uloi_o_close
#define uloi_i_read_ulan uloi_i_read
#define uloi_o_write_ulan uloi_o_write
#define uloi_set_io_mode_ulan uloi_set_io_mode

#endif /*CONFIG_ULOI_CON_IO_OPS*/

int uloi_i_open_ulan(ULOI_PARAM_coninfo UL_ARGPTRTYPE ul_msginfo *imsginfo)
{
 #ifndef UL_WITHOUT_HANDLE
  uchar uloi_tmpbuf[3];
  ul_msginfo msginfo;
 #endif
  ul_ssize_t ret;
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);

  if(coninfo->error)
    return -1;

  if(!imsginfo){
    ret=ul_acceptmsg(uloi_con_ulan_rdfd(coninfo), &msginfo);
    if(ret<0){
      coninfo->error=1;
      return -1;
    }
    if((msginfo.cmd!=con_ulan->cmd)||
       (msginfo.len<3)||
       (msginfo.flg&UL_BFL_FAIL))
      goto conerr;
    con_ulan->adr=msginfo.sadr;
  }else{
    if((imsginfo->cmd!=con_ulan->cmd)||
       (imsginfo->len<3)||
       (imsginfo->flg&UL_BFL_FAIL))
      goto conerr1;
    con_ulan->adr=imsginfo->sadr;
    ret=1;
  }

  if(uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 3)!=3)
    goto conerr;

  con_ulan->bcmd=uloi_tmpbuf[0];
  con_ulan->bsn=uloi_tmpbuf[1];
  /* We could need our loop-back serial number for messages tracking */
  con_ulan->sn=uloi_tmpbuf[2];

  return ret;

  conerr:
   #ifdef ul_i_close
    ul_i_close(uloi_con_ulan_rdfd(coninfo));
   #else
    ul_abortmsg(uloi_con_ulan_rdfd(coninfo));
   #endif
  conerr1:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}

int uloi_o_open_ulan(ULOI_PARAM1_coninfo)
{
 #ifndef UL_WITHOUT_HANDLE
  ul_msginfo msginfo;
  uchar uloi_tmpbuf[3];
 #endif
  ul_ssize_t ret;
  uloi_con_ulan_t *con_ulan = UL_CONTAINEROF(coninfo, uloi_con_ulan_t, con);

  if(coninfo->state&ULOI_CONINFO_OUTOPEN)
    return 0;

  msginfo.dadr=con_ulan->adr;
  msginfo.sadr=0;
  msginfo.cmd=con_ulan->bcmd;
  msginfo.flg=UL_BFL_SND|UL_BFL_ARQ;
  msginfo.len=0;
  msginfo.stamp=0; /* Only to make Valgrind happy */
  ret=ul_newmsg(uloi_con_ulan_wrfd(coninfo), &msginfo);
  if(ret<0){
    coninfo->error=1;
    uloi_i_close(ULOI_ARG1_coninfo);
    return -1;
  }

  coninfo->state|=ULOI_CONINFO_OUTOPEN;

  uloi_tmpbuf[0]=con_ulan->cmd;
  /* We do not run own sequence numbers in the device */
  uloi_tmpbuf[1]=0; /*con_ulan->sn;*/
  uloi_tmpbuf[2]=con_ulan->bsn;

  if(uloi_o_write(ULOI_ARG_coninfo uloi_tmpbuf, 3)!=3){
    coninfo->error=1;
    uloi_o_close(ULOI_ARG1_coninfo);
    return -1;
  }

  return 1;
}

int uloi_i_close_ulan(ULOI_PARAM1_coninfo)
{
#ifdef ul_i_close
  return ul_i_close(uloi_con_ulan_rdfd(coninfo));
#else
  if(coninfo->error){
    return ul_abortmsg(uloi_con_ulan_rdfd(coninfo));
  }
  return ul_freemsg(uloi_con_ulan_rdfd(coninfo));
#endif
}

int uloi_o_close_ulan(ULOI_PARAM1_coninfo)
{
  if(!(coninfo->state&ULOI_CONINFO_OUTOPEN))
    return 0;

  coninfo->state&=~ULOI_CONINFO_OUTOPEN;

#ifdef ul_o_close
  if(coninfo->error)
    return -1;
  return ul_o_close(uloi_con_ulan_wrfd(coninfo));
#else
  if(coninfo->error){
    return ul_abortmsg(uloi_con_ulan_wrfd(coninfo));
  }
  return ul_freemsg(uloi_con_ulan_wrfd(coninfo));
#endif
}

ul_ssize_t uloi_i_read_ulan(ULOI_PARAM_coninfo void *buffer, size_t size)
{
  ul_ssize_t ret;
  if(coninfo->error)
    return -1;
  ret=ul_read(uloi_con_ulan_rdfd(coninfo), buffer, size);
  if(ret<0)
    coninfo->error=1;
  return ret;
}

ul_ssize_t uloi_o_write_ulan(ULOI_PARAM_coninfo const void *buffer, size_t size)
{
  ul_ssize_t ret;
  if(coninfo->error)
    return -1;
  ret=ul_write(uloi_con_ulan_wrfd(coninfo), buffer, size);
  if(ret<0)
    coninfo->error=1;
  return ret;
}

int uloi_set_io_mode_ulan(ULOI_PARAM_coninfo int mode)
{
  return ULOI_SET_IO_MODE_UNSPEC;
}

#ifdef CONFIG_ULOI_CON_IO_OPS

uloi_con_io_ops_t uloi_con_io_ops_ulan = {
  uloi_i_open_ulan,
  uloi_o_open_ulan,
  uloi_i_close_ulan,
  uloi_o_close_ulan,
  uloi_i_read_ulan,
  uloi_o_write_ulan,
  uloi_set_io_mode_ulan
};

#endif /*CONFIG_ULOI_CON_IO_OPS*/
