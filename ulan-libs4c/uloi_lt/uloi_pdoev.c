#include <string.h>
#include "uloi_pdoev.h"

void uloi_evarr_clear_all(uloi_evarr_t *evarr)
{
  memset(evarr->bits,0,(evarr->evnum_max+CHAR_BIT)/CHAR_BIT);
}

int uloi_evarr_ffs_from(uloi_evarr_t *evarr, int evnumfrom)
{
  uloi_evarr_basetype_t *p;
  uloi_evarr_basetype_t dw;
  int evnumoffs;

  if(evnumfrom>=evarr->evnum_max)
    return -1;

  p = evarr->bits + evnumfrom/ULOI_EVARR_BASETYPE_BIT;
  dw=*(p++);

  evnumoffs=evnumfrom%ULOI_EVARR_BASETYPE_BIT;
  if(evnumoffs) {
    dw>>=evnumoffs;
  }

  do {
    if(dw)
      return evnumfrom+ffs(dw)-1;

    evnumfrom+=ULOI_EVARR_BASETYPE_BIT-evnumoffs;
    evnumoffs=0;

    if(evnumfrom>evarr->evnum_max)
      return -1;

    dw=*(p++);
  } while(1);
}

uloi_pev2cdes_t *uloi_pev2c_table_at(uloi_pev2cdes_array_t *pev2carr, unsigned indx)
{
  if(indx >= pev2carr->table.count)
    return NULL;
  return &pev2carr->table.data[indx];
}

int uloi_pev2c_table_remap(uloi_pev2cdes_array_t *pev2carr)
{
  uloi_pev2cdes_t *pev2cdes;
  unsigned idx;

  uloi_pev2c_ev_delete_all(pev2carr);
  pev2carr->table.count=0;
  for(idx=0;idx<pev2carr->table.alloc_count;idx++) {
    pev2cdes=&pev2carr->table.data[idx];
    if (pev2cdes->evnum) {
      pev2carr->table.count=idx+1;
      uloi_pev2c_ev_map(pev2carr, pev2cdes);
    }
  }
  return 0;
}

void uloi_pev2c_table_clear(uloi_pev2cdes_array_t *pev2carr)
{
  pev2carr->cid4send=NULL;
  pev2carr->cid4local=NULL;
  pev2carr->table.count=0;
  memset(pev2carr->table.data,0,pev2carr->table.alloc_count);
}

int uloi_pev2c_ev_map(uloi_pev2cdes_array_t *pev2carr, uloi_pev2cdes_t *pev2cdes)
{
  if(pev2cdes->evnum > pev2carr->evarr->evnum_max)
    return -1;

  if(pev2carr->pev2c_map!=NULL) {
    pev2cdes->next4evnum=pev2carr->pev2c_map[pev2cdes->evnum];
    pev2carr->pev2c_map[pev2cdes->evnum]=pev2cdes;
  }
  return 0;
}

int uloi_pev2c_ev_unmap(uloi_pev2cdes_array_t *pev2carr, uloi_pev2cdes_t *pev2cdes)
{
  uloi_pev2cdes_t **pp;

  pp = &pev2carr->cid4send;
  for(;*pp!=NULL; pp=&(*pp)->next4send) {
    if(*pp == pev2cdes) {
      *pp=pev2cdes->next4send;
      break;
    }
  }

  pp = &pev2carr->cid4local;
  for(;*pp!=NULL; pp=&(*pp)->next4send) {
    if(*pp == pev2cdes) {
      *pp=pev2cdes->next4send;
      break;
    }
  }

  if(pev2cdes->evnum > pev2carr->evarr->evnum_max)
    return -1;

  if(pev2carr->pev2c_map==NULL)
    return 0;

  pp = &pev2carr->pev2c_map[pev2cdes->evnum];
  for(;*pp!=NULL; pp=&(*pp)->next4evnum) {
    if(*pp==pev2cdes) {
      *pp=pev2cdes->next4evnum;
      pev2cdes->next4evnum=NULL;
      return 0;
    }
  }
  return -1;
}

void uloi_pev2c_ev_delete_all(uloi_pev2cdes_array_t *pev2carr)
{
  if(pev2carr->pev2c_map!=NULL)
    memset(pev2carr->pev2c_map,0,pev2carr->evarr->evnum_max*sizeof(uloi_pev2cdes_t *));
}

static inline
uloi_pev2cdes_t *uloi_pev2c_first4ev(uloi_pev2cdes_array_t *pev2carr, int evnum)
{
  uloi_pev2cdes_t *pdes;

  if(pev2carr->pev2c_map!=NULL)
    return pev2carr->pev2c_map[evnum];

  /* Slow code variant for case when event number to ev2c map is not used */
  pdes=pev2carr->table.data;
  while(pdes-pev2carr->table.data < pev2carr->table.count) {
    if(pdes->evnum==evnum)
      return pdes;
    pdes++;
  }
  return NULL;
}

static inline
uloi_pev2cdes_t *uloi_pev2c_next4ev(uloi_pev2cdes_array_t *pev2carr, int evnum, uloi_pev2cdes_t *pdes)
{
  if(pev2carr->pev2c_map!=NULL)
    return pdes=pdes->next4evnum;

  /* Slow code variant for case when event number to ev2c map is not used */
  while((++pdes)-pev2carr->table.data < pev2carr->table.count) {
    if(pdes->evnum==evnum)
      return pdes;
  }
  return NULL;
}

int uloi_pev2c_procev(uloi_pev2cdes_array_t *pev2carr)
{
  int evnum;
  uloi_pev2cdes_t *pdes;
  uloi_evarr_t *evarr=pev2carr->evarr;

  uloi_evarr_for_each_set(evarr, evnum) {
    uloi_evarr_clear_ev(evarr, evnum);
    for(pdes=uloi_pev2c_first4ev(pev2carr, evnum); pdes!=NULL;
        pdes=uloi_pev2c_next4ev(pev2carr, evnum, pdes)) {
      uloi_pev2cdes_t **pp;
      int ins_done=0;
      int arq_fl = pdes->flg & ULOI_PEV2CFLG_ARQ;
     #ifdef CONFIG_ULOI_WITH_GMASK
      pdes->dest_gmask = ULOI_GMASK_ALL;

      if((pdes->flg & ULOI_PEV2CFLG_USE_GMASK) &&
         (pev2carr->gmask != NULL)) {
        /* FIXME: ugly and even more ugly solution */
        unsigned pev2carr_idx = pdes - pev2carr->table.data;
        if(pev2carr_idx < pev2carr->table.count)
          pdes->dest_gmask = pev2carr->gmask[pev2carr_idx];
        if(pdes->dest_gmask==ULOI_GMASK_NONE)
          continue;
      }
     #endif /*CONFIG_ULOI_WITH_GMASK*/

      if(pdes->flg & ULOI_PEV2CFLG_LOCAL)
        pp = &pev2carr->cid4local;
      else
        pp = &pev2carr->cid4send;

      /*
       * Insert CID send request, the queue order is first broadcast, then distinct
       * address in successive order, the requests with acknowledge flag first
       * in their respective destination address group
       */
      for(;*pp!=NULL; pp=&(*pp)->next4send) {

        if(((*pp)->dadr > pdes->dadr) ||
           (arq_fl && ((*pp)->dadr == pdes->dadr) &&
            !((*pp)->flg & ULOI_PEV2CFLG_ARQ)) ) {
          pdes->next4send=*pp;
          *pp=pdes;
          pp=&pdes->next4send;
          ins_done = 1;
          break;
        }

        if((*pp)->cid == pdes->cid) {
          if(((*pp)->dadr == pdes->dadr) ||
             (!((*pp)->dadr) && !arq_fl)) {
           #ifdef CONFIG_ULOI_WITH_GMASK
            (*pp)->dest_gmask |= pdes->dest_gmask;
           #endif /*CONFIG_ULOI_WITH_GMASK*/
            ins_done = 2;
            break;
          }
        }
      }

      if(ins_done == 0) {
        /* insert new request at end of queue */
        pdes->next4send=*pp;
        *pp=pdes;
      } else if((ins_done == 1) && !pdes->dadr) {
        /* new broadcast request inserted, remove unneeded distinct ones */
        while(*pp != NULL) {
          if(((*pp)->cid == pdes->cid) && !((*pp)->flg & ULOI_PEV2CFLG_ARQ)) {
           #ifdef CONFIG_ULOI_WITH_GMASK
            pdes->dest_gmask |= (*pp)->dest_gmask;
           #endif /*CONFIG_ULOI_WITH_GMASK*/
            *pp=(*pp)->next4send;
            continue;
          }
          pp=&(*pp)->next4send;
        }
      } else if((ins_done == 1) && arq_fl) {
        /* new acknowledged request inserted, remove unacknowledged one */
        for(;*pp != NULL; pp=&(*pp)->next4send) {
          if((*pp)->dadr != pdes->dadr)
            break;
          if((*pp)->cid == pdes->cid) {
           #ifdef CONFIG_ULOI_WITH_GMASK
            pdes->dest_gmask |= (*pp)->dest_gmask;
           #endif /*CONFIG_ULOI_WITH_GMASK*/
            *pp=(*pp)->next4send;
            break;
          }
        }
      }
    }
  }
  return 0;
}

int uloi_pev2c_procsend(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, int bytes_limit)
{
  int res;
  int dadr = -1;
  uloi_pev2cdes_array_t *pev2carr=pdostate->pev2cdes_array;
  uloi_pev2cdes_t *pdes;
  int send_bytes=0;
  int data_empty=1;

  while((pdes=pev2carr->cid4send)!=NULL) {
    if(bytes_limit && (send_bytes >= bytes_limit))
      break;

    if(dadr!=pdes->dadr) {
      if(dadr>=0)
        uloi_o_close(coninfo);
      coninfo->error=0;
      dadr=pdes->dadr;
      uloi_con_ulan_set_dadr_cmd(coninfo, dadr, UL_CMD_PDO);
      uloi_con_ulan_set_outflg(coninfo, pdes->flg & ULOI_PEV2CFLG_ARQ? UL_BFL_ARQ: 0);
      res=uloi_o_open(coninfo);
      if(res<0)
        return send_bytes? send_bytes: -1;
      send_bytes+=30; /* Rough new message creation overhead in buffers and on wires */
      data_empty=1;
     #ifdef CONFIG_ULOI_WITH_GMASK
      pdostate->act_gmask = ULOI_GMASK_ALL;
     #endif /*CONFIG_ULOI_WITH_GMASK*/
    }

   #ifdef CONFIG_ULOI_WITH_GMASK
    if(pdostate->act_gmask != pdes->dest_gmask) {
      res = uloi_pdoproc_send_gmask(coninfo, pdostate, pdes->dest_gmask);
      if(res < 0)
        return -1;
      pdostate->act_gmask = pdes->dest_gmask;
    }
   #endif /*CONFIG_ULOI_WITH_GMASK*/

    if(pdes->flg & ULOI_PEV2CFLG_NODATA) {
      unsigned int cid = pdes->cid;
      unsigned char cid_len = 0;
      res = uloi_wr_uint(coninfo, &cid);
      if(res >= 0)
        send_bytes+=2;
      res = uloi_wr_uchar(coninfo, &cid_len);
      if(res >= 0)
        send_bytes+=1;
    } else {
      res = uloi_pdoproc_send_cid(coninfo, pdostate, pdes->cid);
      if(res>=0)
        send_bytes+=res;
    }

    if(res>=0)
      data_empty=0;

    pev2carr->cid4send=pdes->next4send;
  }

  if(dadr>=0) {
    if(data_empty)
      coninfo->error=1;
    uloi_o_close(coninfo);
  }

  return send_bytes;
}

int uloi_pev2c_proclocal(uloi_pdoproc_state_t *pdostate)
{
  int res;
  uloi_pev2cdes_array_t *pev2carr=pdostate->pev2cdes_array;
  uloi_pev2cdes_t *pdes;

  while((pdes=pev2carr->cid4local)!=NULL) {

   #ifdef CONFIG_ULOI_WITH_GMASK
    pdostate->act_gmask = pdes->dest_gmask;
   #endif /*CONFIG_ULOI_WITH_GMASK*/

    res = uloi_pdoproc_send_cid(NULL, pdostate, pdes->cid);
    if(res<0)
      return -1;

    pev2carr->cid4local=pdes->next4send;
  }

  return 0;
}