#ifndef _ULOI_PDOEV_H
#define _ULOI_PDOEV_H

#include <limits.h>

#include "uloi_base.h"
#include "uloi_pdoproc.h"

#ifdef __cplusplus
extern "C" {
#endif
  
typedef unsigned short uloi_evnum_t;
typedef unsigned int uloi_evarr_basetype_t;

#define ULOI_EVARR_BASETYPE_BIT (sizeof(uloi_evarr_basetype_t)*CHAR_BIT)
typedef struct uloi_evarr_t {
  uloi_evarr_basetype_t *bits;
  unsigned evnum_max;
} uloi_evarr_t;

#define ULOI_EVARR_DEFINE(M_name, M_EVNUM_MAX) \
uloi_evarr_basetype_t M_name##_bits_space[(M_EVNUM_MAX+ULOI_EVARR_BASETYPE_BIT)/ULOI_EVARR_BASETYPE_BIT]; \
uloi_evarr_t M_name = { M_name##_bits_space, M_EVNUM_MAX};

static inline void uloi_evarr_set_ev(uloi_evarr_t *evarr, int evnum)
{
  if(evnum>evarr->evnum_max)
    return;
  evarr->bits[evnum/ULOI_EVARR_BASETYPE_BIT] |= 1 <<(evnum % ULOI_EVARR_BASETYPE_BIT);
}

static inline void uloi_evarr_clear_ev(uloi_evarr_t *evarr, int evnum)
{
  if(evnum>evarr->evnum_max)
    return;
  evarr->bits[evnum/ULOI_EVARR_BASETYPE_BIT] &= ~(1 <<(evnum % ULOI_EVARR_BASETYPE_BIT));
}

static inline int uloi_evarr_ev_isset(uloi_evarr_t *evarr, int evnum)
{
  if(evnum>evarr->evnum_max)
    return 0;
  return (evarr->bits[evnum/ULOI_EVARR_BASETYPE_BIT] & (1 <<(evnum % ULOI_EVARR_BASETYPE_BIT))) != 0;
}

void uloi_evarr_clear_all(uloi_evarr_t *evarr);

int uloi_evarr_ffs_from(uloi_evarr_t *evarr, int evnumfrom);

#define uloi_evarr_for_each_set(M_evarr, M_evnum) \
  for(M_evnum=0; (M_evnum=uloi_evarr_ffs_from(M_evarr, M_evnum))>=0; M_evnum++)

typedef enum uloi_pev2cdes_flg_t {
  ULOI_PEV2CFLG_ARQ       = 0x01,
  ULOI_PEV2CFLG_LOCAL     = 0x02,
  ULOI_PEV2CFLG_NODATA    = 0x04,
  ULOI_PEV2CFLG_USE_GMASK = 0x08,
} uloi_pev2cdes_flg_t;

typedef struct uloi_pev2cdes_t {
  uloi_evnum_t evnum;
  uloi_cid_t cid;
  unsigned short dadr;
  unsigned short flg;
  struct uloi_pev2cdes_t *next4evnum;
  struct uloi_pev2cdes_t *next4send;
 #ifdef CONFIG_ULOI_WITH_GMASK
  uloi_gmask_t dest_gmask;
 #endif /*CONFIG_ULOI_WITH_GMASK*/
} uloi_pev2cdes_t;

typedef struct uloi_pev2cdes_array_t {
  struct {
    uloi_pev2cdes_t *data;
    unsigned count;
    unsigned alloc_count;
  } table;
  uloi_pev2cdes_t **pev2c_map; /*ULOI_EVNUM_MAX*/
  uloi_evarr_t *evarr;
  uloi_pev2cdes_t *cid4local;
  uloi_pev2cdes_t *cid4send;
 #ifdef CONFIG_ULOI_WITH_GMASK
  uloi_gmask_t *gmask;
 #endif /*CONFIG_ULOI_WITH_GMASK*/
} uloi_pev2cdes_array_t;

uloi_pev2cdes_t *uloi_pev2c_table_at(uloi_pev2cdes_array_t *pev2carr, unsigned indx);
int uloi_pev2c_table_remap(uloi_pev2cdes_array_t *pev2carr);
void uloi_pev2c_table_clear(uloi_pev2cdes_array_t *pev2carr);

int uloi_pev2c_ev_map(uloi_pev2cdes_array_t *pev2carr, uloi_pev2cdes_t *pev2cdes);
int uloi_pev2c_ev_unmap(uloi_pev2cdes_array_t *pev2carr, uloi_pev2cdes_t *pev2cdes);
void uloi_pev2c_ev_delete_all(uloi_pev2cdes_array_t *pev2carr);

int uloi_pev2c_procev(uloi_pev2cdes_array_t *pev2carr);
int uloi_pev2c_procsend(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, int bytes_limit);
int uloi_pev2c_proclocal(uloi_pdoproc_state_t *pdostate);

#ifdef __cplusplus
}
#endif
  
#endif /*_ULOI_PDOEV_H*/
