#include <string.h>
#include <uloi_base.h>
#include <uloi_ioproxy.h>
#include <uloi_pdoproc.h>
#include <uloi_pdoev.h>
#include <uloi_array.h>

int uloi_pico_array_rdfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  uloi_ciddes_t *ciddes;
  unsigned int ui;

  res = uloi_array_meta4rdfnc(ULOI_ARG_coninfo &idx, &cnt, pdostate->pico_ciddes_array->table.count);
  if(res < 0)
    return res;

  ciddes = uloi_pdoproc_pico_ciddes_table_at(pdostate, idx);
  if(ciddes == NULL)
    return -1;

  for (;cnt--; ciddes++) {
    ui = ciddes->cid;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    ui = ciddes->flg;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    if((ciddes->flg & ULOI_CIDFLG_META_LOC) == ULOI_CIDFLG_META_OFFS)
      ui = ciddes->meta.offs;
    else
      ui = ciddes->meta.oid;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    ui = ciddes->meta_len;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
  }
  return 0;

err:
  return res;
}

int uloi_pico_array_wrfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  uloi_ciddes_t *ciddes;
  unsigned int cid, flg, meta, meta_len;

  res = uloi_array_meta4wrfnc(ULOI_ARG_coninfo &idx, &cnt, pdostate->pico_ciddes_array->table.alloc_count);
  if(res < 0)
    return res;

  if(idx >= pdostate->pico_ciddes_array->table.count) {
    if(idx >= pdostate->pico_ciddes_array->table.alloc_count)
      return -1;
    /*FIXME*/
    pdostate->pico_ciddes_array->table.count = idx+1;
  }
  ciddes = uloi_pdoproc_pico_ciddes_table_at(pdostate, idx);
  if(ciddes == NULL)
    return -1;

  for (;cnt--; ciddes++, idx++) {
    if(idx >= pdostate->pico_ciddes_array->table.count) {
      if(idx >= pdostate->pico_ciddes_array->table.alloc_count)
        return -1;
      /*FIXME*/
      pdostate->pico_ciddes_array->table.count = idx+1;
    }

    if((res = uloi_rd_uint(ULOI_ARG_coninfo &cid)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &flg)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &meta)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &meta_len)) < 0) goto err;

    uloi_pdoproc_pico_ciddes_sorted_unmap(pdostate, ciddes);
    ciddes->cid = cid;
    ciddes->flg = flg;
    if((ciddes->flg & ULOI_CIDFLG_META_LOC) == ULOI_CIDFLG_META_OFFS)
      ciddes->meta.offs = meta;
    else
      ciddes->meta.oid = meta;
    ciddes->meta_len = meta_len;
    if (cid)
      uloi_pdoproc_pico_ciddes_sorted_map(pdostate, ciddes);
  }
  return 0;

err:
  return res;
}

int uloi_poco_array_rdfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  uloi_ciddes_t *ciddes;
  unsigned int ui;

  res = uloi_array_meta4rdfnc(ULOI_ARG_coninfo &idx, &cnt, pdostate->poco_ciddes_array->table.count);
  if(res < 0)
    return res;

  ciddes = uloi_pdoproc_poco_ciddes_table_at(pdostate, idx);
  if(ciddes == NULL)
    return -1;

  for (;cnt--; ciddes++) {
    ui = ciddes->cid;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    ui = ciddes->flg;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    if((ciddes->flg & ULOI_CIDFLG_META_LOC) == ULOI_CIDFLG_META_OFFS)
      ui = ciddes->meta.offs;
    else
      ui = ciddes->meta.oid;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    ui = ciddes->meta_len;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
  }
  return 0;

err:
  return res;
}

int uloi_poco_array_wrfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  uloi_ciddes_t *ciddes;
  unsigned int cid, flg, meta, meta_len;

  res = uloi_array_meta4wrfnc(ULOI_ARG_coninfo &idx, &cnt, pdostate->poco_ciddes_array->table.alloc_count);
  if(res < 0)
    return res;

  if(idx >= pdostate->poco_ciddes_array->table.count) {
    if(idx >= pdostate->poco_ciddes_array->table.alloc_count)
      return -1;
    /*FIXME*/
    pdostate->poco_ciddes_array->table.count = idx+1;
  }
  ciddes = uloi_pdoproc_poco_ciddes_table_at(pdostate, idx);
  if(ciddes == NULL)
    return -1;

  for (;cnt--; ciddes++, idx++) {
    if(idx >= pdostate->poco_ciddes_array->table.count) {
      if(idx >= pdostate->poco_ciddes_array->table.alloc_count)
        return -1;
      /*FIXME*/
      pdostate->poco_ciddes_array->table.count = idx+1;
    }

    if((res = uloi_rd_uint(ULOI_ARG_coninfo &cid)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &flg)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &meta)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &meta_len)) < 0) goto err;

    uloi_pdoproc_poco_ciddes_sorted_unmap(pdostate, ciddes);
    ciddes->cid = cid;
    ciddes->flg = flg;
    if((ciddes->flg & ULOI_CIDFLG_META_LOC) == ULOI_CIDFLG_META_OFFS)
      ciddes->meta.offs = meta;
    else
      ciddes->meta.oid = meta;
    ciddes->meta_len = meta_len;
    if (cid)
      uloi_pdoproc_poco_ciddes_sorted_map(pdostate, ciddes);
  }
  return 0;

err:
  return res;
}

int uloi_pev2c_array_rdfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  uloi_pev2cdes_t *pdes;
  unsigned int ui;

  res = uloi_array_meta4rdfnc(ULOI_ARG_coninfo &idx, &cnt, pdostate->pev2cdes_array->table.count);
  if(res < 0)
    return res;

  pdes = uloi_pev2c_table_at(pdostate->pev2cdes_array, idx);
  if(pdes == NULL)
    return -1;

  for (;cnt--; pdes++) {
    ui = pdes->evnum;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    ui = pdes->cid;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    ui = pdes->dadr;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
    ui = pdes->flg;
    if((res = uloi_wr_uint(ULOI_ARG_coninfo &ui)) < 0) goto err;
  }
  return 0;

err:
  return res;
}

int uloi_pev2c_array_wrfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int idx;
  unsigned int cnt;
  int res;
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  uloi_pev2cdes_t *pdes;
  unsigned int evnum, cid, dadr, flg;

  res = uloi_array_meta4wrfnc(ULOI_ARG_coninfo &idx, &cnt, pdostate->pev2cdes_array->table.alloc_count);
  if(res < 0)
    return res;

  if(idx >= pdostate->pev2cdes_array->table.count) {
    if(idx >= pdostate->pev2cdes_array->table.alloc_count)
      return -1;
    /*FIXME*/
    pdostate->pev2cdes_array->table.count = idx+1;
  }
  pdes = uloi_pev2c_table_at(pdostate->pev2cdes_array, idx);
  if(pdes == NULL)
    return -1;

  for (;cnt--; pdes++, idx++) {
    if(idx >= pdostate->pev2cdes_array->table.count) {
      if(idx >= pdostate->pev2cdes_array->table.alloc_count)
        return -1;
      /*FIXME*/
      pdostate->pev2cdes_array->table.count = idx+1;
    }
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &evnum)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &cid)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &dadr)) < 0) goto err;
    if((res = uloi_rd_uint(ULOI_ARG_coninfo &flg)) < 0) goto err;

    uloi_pev2c_ev_unmap(pdostate->pev2cdes_array, pdes);
    pdes->evnum = evnum;
    pdes->cid = cid;
    pdes->dadr = dadr;
    pdes->flg = flg;
    if (evnum)
      uloi_pev2c_ev_map(pdostate->pev2cdes_array, pdes);
  }
  return 0;

err:
  return res;
}

int uloi_pmap_clear(uloi_pdoproc_state_t *pdostate)
{
  /* pico */
  uloi_pdoproc_pico_ciddes_sorted_delete_all(pdostate);
  uloi_pdoproc_pico_ciddes_table_clear(pdostate);
  /* poco */
  uloi_pdoproc_poco_ciddes_sorted_delete_all(pdostate);
  uloi_pdoproc_poco_ciddes_table_clear(pdostate);
  /* piom */
  memset(pdostate->meta_base,0,pdostate->meta_len);
  /* pev2c */
  uloi_pev2c_ev_delete_all(pdostate->pev2cdes_array);
  uloi_pev2c_table_clear(pdostate->pev2cdes_array);
  return 0;
}
