#ifndef _ULOI_PDOMAP_H
#define _ULOI_PDOMAP_H

#include "uloi_base.h"

#ifdef __cplusplus
extern "C" {
#endif

int uloi_pico_array_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_pico_array_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_poco_array_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_poco_array_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_pev2c_array_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_pev2c_array_wrfnc(ULOI_PARAM_coninfo void *context);

int uloi_pmap_clear(uloi_pdoproc_state_t *pdostate);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_PDOMAP_H*/
