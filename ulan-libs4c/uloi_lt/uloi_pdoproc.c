#include <string.h>
#include <uloi_base.h>
#include <uloi_ioproxy.h>
#include <uloi_pdoproc.h>
#include <limits.h>

/*===========================================================*/

#if 0

#include <ul_gsa.h>
#include <ul_gsacust.h>



GSA_STATIC_CUST_DEC(uloi_ciddes_array, uloi_ciddes_array_t, uloi_ciddes_t, uloi_cid_t,
	sorted, cid, uloi_cid_cmp_fnc)

inline int
uloi_cid_cmp_fnc(const uloi_cid_t *a, const uloi_cid_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

GSA_STATIC_CUST_IMP(uloi_ciddes_array, uloi_ciddes_array_t, uloi_ciddes_t, uloi_cid_t,
	sorted, cid, uloi_cid_cmp_fnc, GSA_FAFTER)

#else

int
uloi_ciddes_array_bsearch_indx (const uloi_ciddes_array_t * array,
                                uloi_cid_t key, int mode,
                                unsigned *indx)
{
  unsigned a, b, c;
  int r;
  if (!array->sorted.items || !array->sorted.count) {
    *indx = 0;
    return 0;
  }
  a = 0;
  b = array->sorted.count;
  while (1) {
    c = (a + b) / 2;
    r = array->sorted.items[c]->cid - key;
    if (!r)
      if (!(mode & 2))
        break;
    if (r <= 0)
      a = c + 1;
    else
      b = c;
    if (a == b) {
      *indx = a;
      return mode & 2;
    }
  }
  if (mode & 1) {
    b = c;
    do {
      c = (a + b) / 2;
      r = array->sorted.items[c]->cid - key;
      if (r)
        a = c + 1;
      else
        b = c;
    } while (a != b);
    c = b;
  }
  *indx = c;
  return 1;
}

uloi_ciddes_t *
uloi_ciddes_array_at (const uloi_ciddes_array_t * array, unsigned indx)
{
  if (indx >= array->sorted.count)
    return NULL;
  return array->sorted.items[indx];
}

uloi_ciddes_t *
uloi_ciddes_array_find (const uloi_ciddes_array_t * array, uloi_cid_t * key)
{
  unsigned indx;
  if(!uloi_ciddes_array_bsearch_indx (array, *key, 1, &indx))
    return NULL;
  return array->sorted.items[indx];
}

unsigned
uloi_ciddes_array_find_first_indx (const uloi_ciddes_array_t * array,
                                   uloi_cid_t * key)
{
  unsigned indx;
  if(!uloi_ciddes_array_bsearch_indx (array, *key, 1, &indx))
    return -1;
  return indx;
}

unsigned
uloi_ciddes_array_find_after_indx (const uloi_ciddes_array_t * array,
				   uloi_cid_t * key)
{
  unsigned indx;
  if(!uloi_ciddes_array_bsearch_indx (array, *key, 2, &indx))
    return -1;
  return indx;
}

int
uloi_ciddes_array_insert_at(uloi_ciddes_array_t * array,
                            uloi_ciddes_t * item, unsigned indx)
{
  unsigned cnt=array->sorted.count;
  uloi_ciddes_t **p;

  if(indx>cnt) indx=cnt;
  if(cnt+1>=array->sorted.alloc_count)
    return -1;

  p=array->sorted.items+indx;
  memmove(p+1,p,(char*)(array->sorted.items+cnt)-(char*)p);
  array->sorted.count=cnt+1;
  *p=item;
  return 0;
}

int
uloi_ciddes_array_delete_at (uloi_ciddes_array_t * array, unsigned indx)
{

  unsigned cnt=array->sorted.count;
  uloi_ciddes_t **p;
  if(indx>=cnt) return -1;
  p=array->sorted.items+indx;
  array->sorted.count=--cnt;
  memmove(p,p+1,(array->sorted.items+cnt-p)*sizeof(void *));
  return 0;
}

void
uloi_ciddes_array_delete_all (uloi_ciddes_array_t * array)
{
  array->sorted.count = 0;
}

int
uloi_ciddes_array_insert(uloi_ciddes_array_t * array, uloi_ciddes_t * item)
{
  unsigned indx;
  uloi_ciddes_array_bsearch_indx(array, item->cid, 2, &indx);
  return uloi_ciddes_array_insert_at(array, item, indx);
}

int
uloi_ciddes_array_delete (uloi_ciddes_array_t * array,
                          const uloi_ciddes_t * item)
{
  unsigned indx;
  if (!uloi_ciddes_array_bsearch_indx(array, item->cid, 1, &indx))
    return -1;
  while (array->sorted.items[indx] != item) {
    if (++indx >= array->sorted.count)
      return -1;
    if (array->sorted.items[indx]->cid != item->cid)
      return -1;
  }
  return uloi_ciddes_array_delete_at(array, indx);
}

#endif

#if 0
uloi_ciddes_t *
uloi_ciddes_array_find_dummy(const uloi_ciddes_array_t * array, uloi_cid_t *pcid)
{
  static uloi_ciddes_t ciddes;
  ciddes.meta_len=2;
  ciddes.meta.oid = *pcid;
  return &ciddes;
}
#endif

uloi_ciddes_t *
uloi_pdoproc_pico_ciddes_table_at(uloi_pdoproc_state_t *pdostate, unsigned ciddesidx)
{
  if(ciddesidx >= pdostate->pico_ciddes_array->table.count)
    return NULL;
  return &pdostate->pico_ciddes_array->table.data[ciddesidx];
}

int uloi_pdoproc_pico_ciddes_table_remap(uloi_pdoproc_state_t *pdostate)
{
  uloi_ciddes_t *ciddes;
  unsigned idx;
  uloi_ciddes_array_delete_all (pdostate->pico_ciddes_array);
  pdostate->pico_ciddes_array->table.count=0;
  for(idx=0;idx<pdostate->pico_ciddes_array->table.alloc_count;idx++) {
    ciddes=&pdostate->pico_ciddes_array->table.data[idx];
    if (ciddes->cid) {
      pdostate->pico_ciddes_array->table.count++;
      uloi_ciddes_array_insert(pdostate->pico_ciddes_array, ciddes);
    }
  }
  return 0;
}

void uloi_pdoproc_pico_ciddes_table_clear(uloi_pdoproc_state_t *pdostate)
{
  pdostate->pico_ciddes_array->table.count=0;
  memset(pdostate->pico_ciddes_array->table.data,0,pdostate->pico_ciddes_array->table.alloc_count);
}

int uloi_pdoproc_pico_ciddes_sorted_map(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes)
{
  return uloi_ciddes_array_insert(pdostate->pico_ciddes_array, ciddes);
}

int uloi_pdoproc_pico_ciddes_sorted_unmap(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes)
{
  return uloi_ciddes_array_delete (pdostate->pico_ciddes_array, ciddes);
}

void uloi_pdoproc_pico_ciddes_sorted_delete_all(uloi_pdoproc_state_t *pdostate)
{
  uloi_ciddes_array_delete_all (pdostate->pico_ciddes_array);
}

uloi_ciddes_t *
uloi_pdoproc_poco_ciddes_table_at(uloi_pdoproc_state_t *pdostate, unsigned ciddesidx)
{
  if(ciddesidx >= pdostate->poco_ciddes_array->table.count)
    return NULL;
  return &pdostate->poco_ciddes_array->table.data[ciddesidx];
}

int uloi_pdoproc_poco_ciddes_table_remap(uloi_pdoproc_state_t *pdostate)
{
  uloi_ciddes_t *ciddes;
  unsigned idx;
  uloi_ciddes_array_delete_all (pdostate->poco_ciddes_array);
  pdostate->poco_ciddes_array->table.count=0;
  for(idx=0;idx<pdostate->poco_ciddes_array->table.alloc_count;idx++) {
    ciddes=&pdostate->poco_ciddes_array->table.data[idx];
    if (ciddes->cid) {
      pdostate->poco_ciddes_array->table.count++;
      uloi_ciddes_array_insert(pdostate->poco_ciddes_array, ciddes);
    }
  }
  return 0;
}

void uloi_pdoproc_poco_ciddes_table_clear(uloi_pdoproc_state_t *pdostate)
{
  pdostate->poco_ciddes_array->table.count=0;
  memset(pdostate->poco_ciddes_array->table.data,0,pdostate->poco_ciddes_array->table.alloc_count);
}

int uloi_pdoproc_poco_ciddes_sorted_map(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes)
{
  return uloi_ciddes_array_insert(pdostate->poco_ciddes_array, ciddes);
}

int uloi_pdoproc_poco_ciddes_sorted_unmap(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes)
{
  return uloi_ciddes_array_delete (pdostate->poco_ciddes_array, ciddes);
}

void uloi_pdoproc_poco_ciddes_sorted_delete_all(uloi_pdoproc_state_t *pdostate)
{
  uloi_ciddes_array_delete_all (pdostate->poco_ciddes_array);
}

//#define uloi_ciddes_array_find uloi_ciddes_array_find_dummy

/*===========================================================*/

#ifdef CONFIG_ULOI_WITH_GMASK
int uloi_pdoproc_rec_gmask(uloi_coninfo_t *coninfo,
         uloi_pdoproc_state_t *pdostate, int data_len)
{
  uloi_gmask_t gmask=0;
  int tobit=0;
  if(!data_len) {
    gmask = ULOI_GMASK_ALL;
  } else {
    while(data_len && (tobit < CHAR_BIT*sizeof(gmask))) {
      uchar uc = 0;
      data_len--;
      if(uloi_rd_uchar(coninfo, &uc)<0)
        return -1;
      gmask |= uc << tobit;
      tobit += 8;
    }
  }
  pdostate->act_gmask = gmask;
  return 0;
}

int uloi_pdoproc_send_gmask(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, uloi_gmask_t gmask)
{
  unsigned cid_code = ULOI_CID_CODE_GMASK;
  uchar len = 0;
  uchar uc;
  int res;
  uloi_gmask_t msk = ~(uloi_gmask_t)0;

  if(gmask != ULOI_GMASK_ALL) {
    do {
      len++;
      msk <<= 8;
    } while((len < sizeof(uloi_gmask_t)) && (gmask & msk));
  }

  res = uloi_wr_uint(coninfo, &cid_code);
  if(res < 0)
    return -1;
  res = uloi_wr_uchar(coninfo, &len);
  if(res < 0)
    return -1;

  while(len--) {
    uc = gmask;
    gmask >>= 8;
    res = uloi_wr_uchar(coninfo, &uc);
    if(res < 0)
      return -1;
  }

  return 0;
}
#endif /*CONFIG_ULOI_WITH_GMASK*/

int uloi_pdoproc_rec_data(uloi_coninfo_t *conptr,
         uloi_pdoproc_state_t *pdostate, int *lenptr)
{
  int res;
  uloi_oid_t oid;
  uloi_objdes_t *objdes;

  while(!conptr->error) {
    if(lenptr != NULL)
      if(*lenptr <= 0)
        break;
    uloi_set_io_mode(conptr, ULOI_SET_IO_MODE_META);
    res = uloi_rd_uint(conptr, &oid);
    uloi_set_io_mode(conptr, ULOI_SET_IO_MODE_DATA);
    if(res < 0)
      return -1;
    objdes = uloi_objdes_array_find(pdostate->objdes_array, &oid);
    if((objdes == NULL) || (objdes->wrfnc == NULL))
      return -1;
    res = uloi_call_objdes_fnc(conptr, objdes->wrfnc, objdes->wrcontext);
    if(res < 0)
      return -1;
  }

  return 0;
}

int uloi_pdoproc_rec_cid(uloi_coninfo_t *coninfo,
         uloi_pdoproc_state_t *pdostate, uloi_cid_t cid, int data_len)
{
  int res;
  uloi_ciddes_t *ciddes;
  uloi_con_proxy_t con_proxy;
  uchar ubuf[4];
  const void *meta_ptr=NULL;
  int meta_len;
  int skip_bytes;
  int loc;
  unsigned ciddesidx;
  int read_done_fl = 0;

  skip_bytes = data_len;

  ciddesidx = uloi_ciddes_array_find_first_indx (pdostate->pico_ciddes_array, &cid);
  for(;(ciddes = uloi_ciddes_array_at(pdostate->pico_ciddes_array, ciddesidx)) != NULL; ciddesidx++) {
    if(ciddes->cid != cid)
      break;
   #ifdef CONFIG_ULOI_WITH_GMASK
    if((ciddes->flg & ULOI_CIDFLG_USE_GMASK) &&
       (pdostate->pico_ciddes_array->gmask!=NULL))
      if(!(pdostate->pico_ciddes_array->gmask[ciddesidx]&pdostate->act_gmask))
        continue;
   #endif /*CONFIG_ULOI_WITH_GMASK*/

    meta_len = ciddes->meta_len;
    loc = ciddes->flg & ULOI_CIDFLG_META_LOC;
    switch(loc) {
      case ULOI_CIDFLG_META_OFFS:
        meta_ptr = pdostate->meta_base + ciddes->meta.offs;
        break;
     #if CONFIG_ULOI_META_DYNAMIC
      case ULOI_CIDFLG_META_PTR:
        meta_ptr = ciddes->meta.ptr;
        break;
     #endif /*CONFIG_ULOI_META_DYNAMIC*/
      case ULOI_CIDFLG_META_EM4B:
        ubuf[3] = (ciddes->meta_len >> 8) & 0xff;
      case ULOI_CIDFLG_META_EM3B:
        meta_len = (loc==ULOI_CIDFLG_META_EM3B)? 3: 4;
        ubuf[2] = ciddes->meta_len & 0xff;
      case ULOI_CIDFLG_META_OID:
        ubuf[0] = ciddes->meta.oid & 0xff;
        ubuf[1] = (ciddes->meta.oid >> 8) & 0xff;
        meta_ptr = ubuf;
        if(meta_len > 4)
          meta_len = 4;
        break;
      default:
        meta_len = 0;
    }

    if(ciddes->flg & ULOI_CIDFLG_FIXED) {
      /* Meta data and data are read from provide meta data area only */
      res = uloi_con_io_proxy_mem_setup(&con_proxy,
                    meta_ptr, meta_len, NULL, 0);
      if(res < 0)
        return -1;

      res = uloi_pdoproc_rec_data(&con_proxy.con, pdostate, &con_proxy.rd.avail);

    } else if(pdostate->buff == NULL) {
      /* Read data directly from provided connection source */
      if(read_done_fl)
        continue;
      read_done_fl = 1;

      res = uloi_con_io_proxy_con_read_meta_aux_mem_setup(&con_proxy,
                    coninfo, data_len, meta_ptr, meta_len);
      if(res < 0)
        return -1;

      res = uloi_pdoproc_rec_data(&con_proxy.con, pdostate, &con_proxy.aux.avail);

      skip_bytes = con_proxy.rd.avail;

    } else {
      /* Read data to the buffer first and process them from the buffer then  */
      if(!read_done_fl) {
        read_done_fl = 1;

        if(coninfo != NULL) {
          if(data_len >= pdostate->buff_len)
            data_len = pdostate->buff_len;

          if(uloi_i_read(coninfo, pdostate->buff, data_len) < 0)
            return -1;
        }

        skip_bytes -= data_len;
      }

      res = uloi_con_io_proxy_mem_read_meta_aux_mem_setup(&con_proxy,
                    pdostate->buff, data_len, meta_ptr, meta_len);
      if(res < 0)
        return -1;

      res = uloi_pdoproc_rec_data(&con_proxy.con, pdostate, &con_proxy.aux.avail);
    }
  }

  {
    void *ptr = ubuf;
    unsigned max_chunk = sizeof(ubuf);
    if(pdostate->buff != NULL) {
      ptr = pdostate->buff;
      max_chunk = pdostate->buff_len;
    }
    while((skip_bytes > 0) && (coninfo != NULL)) {
      int len = skip_bytes;
      if(len > max_chunk)
        len = max_chunk;
      if(uloi_i_read(coninfo, ptr, len) < 0)
        return -1;
      skip_bytes -= len;
    }
  }

  return 0;
}

int uloi_pdoproc_rec(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate)
{
  int res;
  uloi_cid_t cid;
  uchar len;

  res = uloi_rd_uint(coninfo, &cid);
  if(res < 0)
    return -1;
  res = uloi_rd_uchar(coninfo, &len);
  if(res < 0)
    return -1;

 #ifdef CONFIG_ULOI_WITH_GMASK
  if(cid==ULOI_CID_CODE_GMASK)
    return uloi_pdoproc_rec_gmask(coninfo, pdostate, len);
 #endif /*CONFIG_ULOI_WITH_GMASK*/

  return uloi_pdoproc_rec_cid(coninfo, pdostate, cid, len);
}

int uloi_pdoproc_msg(ULOI_PARAM_coninfo uloi_pdoproc_state_t *pdostate, UL_ARGPTRTYPE ul_msginfo *imsginfo)
{
  int res = 0;
  uchar res1, res2, head_len, uc;

  coninfo->state = 0;
  coninfo->error = 0;

 #ifdef CONFIG_ULOI_WITH_GMASK
  pdostate->act_gmask = ULOI_GMASK_ALL;
 #endif /*CONFIG_ULOI_WITH_GMASK*/

  /* FIXME: other open variant should be used there */
  /* res = uloi_i_open(coninfo, imsginfo);
  if(coninfo->error)
    return -1;
  */
  res = uloi_rd_uchar(coninfo, &res1);
  res = uloi_rd_uchar(coninfo, &res2);
  res = uloi_rd_uchar(coninfo, &head_len);

  if(coninfo->error)
    return -1;

  while(head_len > 0) {
    head_len--;
    res = uloi_rd_uchar(coninfo, &uc);
  }

  if(coninfo->error)
    return -1;

  while((!coninfo->error) && (res >= 0)) {
    res = uloi_pdoproc_rec(coninfo, pdostate);
  }

  if(coninfo->state&ULOI_CONINFO_OUTOPEN)
    uloi_o_close(coninfo);

  uloi_i_close(coninfo);

  return 0;
}

int uloi_pdoproc_send_cid(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, uloi_cid_t cid)
{
  int res;
  int data_len = pdostate->buff_len;
  uloi_ciddes_t *ciddes;
  uchar *data_ptr = pdostate->buff;
  uloi_oid_t oid;
  uloi_objdes_t *objdes;
  uloi_con_proxy_t con_proxy;
  uchar ubuf[4];
  int loc;
  const void *meta_ptr=NULL;
  int meta_len;

  ciddes = uloi_ciddes_array_find(pdostate->poco_ciddes_array, &cid);
  if(ciddes == NULL)
    return -1;

  meta_len = ciddes->meta_len;
  loc = ciddes->flg & ULOI_CIDFLG_META_LOC;
  switch(loc) {
    case ULOI_CIDFLG_META_OFFS:
      meta_ptr = pdostate->meta_base + ciddes->meta.offs;
      break;
   #if CONFIG_ULOI_META_DYNAMIC
    case ULOI_CIDFLG_META_PTR:
      meta_ptr = ciddes->meta.ptr;
      break;
   #endif /*CONFIG_ULOI_META_DYNAMIC*/
    case ULOI_CIDFLG_META_EM4B:
      ubuf[3] = (ciddes->meta_len >> 8) & 0xff;
    case ULOI_CIDFLG_META_EM3B:
      meta_len = (loc==ULOI_CIDFLG_META_EM3B)? 3: 4;
      ubuf[2] = ciddes->meta_len & 0xff;
    case ULOI_CIDFLG_META_OID:
      ubuf[0] = ciddes->meta.oid & 0xff;
      ubuf[1] = (ciddes->meta.oid >> 8) & 0xff;
      meta_ptr = ubuf;
      if(meta_len > 4)
        meta_len = 4;
      break;
    default:
      meta_len = 0;
  }

  if(!(ciddes->flg & ULOI_CIDFLG_IMMED)) {
    res = uloi_con_io_proxy_mem_write_meta_aux_mem_setup(&con_proxy,
              data_ptr, data_len, meta_ptr, meta_len);
    if(res < 0)
      return -1;

    while((con_proxy.aux.avail > 0) && !con_proxy.con.error) {
      uloi_set_io_mode(&con_proxy.con, ULOI_SET_IO_MODE_META);
      res = uloi_rd_uint(&con_proxy.con, &oid);
      uloi_set_io_mode(&con_proxy.con, ULOI_SET_IO_MODE_DATA);
      if(res < 0)
        return -1;

      objdes = uloi_objdes_array_find(pdostate->objdes_array, &oid);
      if((objdes == NULL) || (objdes->rdfnc == NULL))
        return -1;

      res = uloi_call_objdes_fnc(&con_proxy.con, objdes->rdfnc, objdes->rdcontext);
      if(res < 0)
        return -1;
    }

    if(con_proxy.con.error != 0)
      return -1;

    if(con_proxy.wr.avail == 0)
      return -1;  /* CID data too long */

    data_len -= con_proxy.wr.avail;
  } else {
    /* The immediate data defined by meta are send for given CID */
    if(meta_len > data_len)
      return -1;
    data_len = meta_len;
    memcpy(data_ptr, meta_ptr, data_len);
  }

  if(!(ciddes->flg & ULOI_CIDFLG_SKIP_LOCAL))
    uloi_pdoproc_rec_cid(NULL, pdostate, cid, data_len);

  if((coninfo != NULL) && !(ciddes->flg & ULOI_CIDFLG_SKIP_SEND)) {
    res = uloi_wr_uint(coninfo, &cid);
    if(res < 0)
      return -1;

    ubuf[0] = data_len;
    res = uloi_wr_uchar(coninfo, ubuf);
    if(res < 0)
      return -1;

    if(uloi_o_write(coninfo, data_ptr, data_len) != data_len)
      return -1;
  }

  return data_len+3;
}

int uloi_pdoproc_send_msg(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, uloi_cid_t *cid_list, int cid_list_len)
{
  int res;
  int i;
  uloi_cid_t cid;

 #ifdef CONFIG_ULOI_WITH_GMASK
  pdostate->act_gmask = ULOI_GMASK_ALL;
 #endif /*CONFIG_ULOI_WITH_GMASK*/

  uloi_o_open(coninfo);

  for(i = 0 ; (i < cid_list_len) && (cid = cid_list[i]); i ++) {
    res = uloi_pdoproc_send_cid(coninfo, pdostate, cid);
  }
  uloi_o_close(coninfo);

  return 0;
}

/*
int uloi_pdoproc_send(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate)
{
  int res;
  void *cid_list = NULL;
  // for each target address  {
    res = uloi_pdoproc_send_msg(coninfo, pdostate, cid_list);
  }

  return 0;
}
*/