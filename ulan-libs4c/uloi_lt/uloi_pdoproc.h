#ifndef _ULOI_PDOPROC_H
#define _ULOI_PDOPROC_H

#include "uloi_base.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CONFIG_ULOI_META_DYNAMIC 1

typedef enum uloi_ciddes_flg_t {
  ULOI_CIDFLG_META_LOC  = 0x0F,
  ULOI_CIDFLG_META_OID  = 0x00,
  ULOI_CIDFLG_META_OFFS = 0x01,
  ULOI_CIDFLG_META_PTR  = 0x02,
  ULOI_CIDFLG_META_EM3B = 0x03,
  ULOI_CIDFLG_META_EM4B = 0x04,
  ULOI_CIDFLG_FIXED     = 0x10,
  ULOI_CIDFLG_IMMED     = 0x20,
  ULOI_CIDFLG_SKIP_LOCAL= 0x40,
  ULOI_CIDFLG_SKIP_SEND = 0x80,
  ULOI_CIDFLG_USE_GMASK = 0x100,
} uloi_ciddes_flg_t;

typedef struct uloi_ciddes_t {
  uloi_cid_t cid;
  unsigned short flg;
  union {
   #if CONFIG_ULOI_META_DYNAMIC
    void *ptr;
   #endif /*CONFIG_ULOI_META_DYNAMIC*/
    unsigned short offs;
    uloi_oid_t oid;
  } meta;
  unsigned short meta_len;
} uloi_ciddes_t;

typedef struct uloi_ciddes_array_t {
  /*gsa_array_field_t;*/
  struct {
    uloi_ciddes_t **items;
    unsigned count;
    unsigned alloc_count;
  } sorted;
  struct {
    uloi_ciddes_t *data;
    unsigned count;
    unsigned alloc_count;
  } table;
 #ifdef CONFIG_ULOI_WITH_GMASK
  uloi_gmask_t *gmask;
 #endif /*CONFIG_ULOI_WITH_GMASK*/
} uloi_ciddes_array_t;

struct uloi_pev2cdes_array_t;
struct uloi_pdc2evdes_array_t;

typedef struct uloi_pdoproc_state_t {
  const uloi_objdes_array_t *objdes_array;
  uloi_ciddes_array_t *pico_ciddes_array;
  uloi_ciddes_array_t *poco_ciddes_array;
  struct uloi_pev2cdes_array_t *pev2cdes_array;
  struct uloi_pdc2evdes_array_t *pdc2evdes_array;
  uchar *meta_base;
  int meta_len;
  void *buff;
  int  buff_len;
 #ifdef CONFIG_ULOI_WITH_GMASK
  uloi_gmask_t act_gmask;
 #endif /*CONFIG_ULOI_WITH_GMASK*/
} uloi_pdoproc_state_t;

uloi_ciddes_t *uloi_pdoproc_pico_ciddes_table_at(uloi_pdoproc_state_t *pdostate, unsigned ciddesidx);
int uloi_pdoproc_pico_ciddes_table_remap(uloi_pdoproc_state_t *pdostate);
void uloi_pdoproc_pico_ciddes_table_clear(uloi_pdoproc_state_t *pdostate);
int uloi_pdoproc_pico_ciddes_sorted_map(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes);
int uloi_pdoproc_pico_ciddes_sorted_unmap(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes);
void uloi_pdoproc_pico_ciddes_sorted_delete_all(uloi_pdoproc_state_t *pdostate);

uloi_ciddes_t *uloi_pdoproc_poco_ciddes_table_at(uloi_pdoproc_state_t *pdostate, unsigned ciddesidx);
int uloi_pdoproc_poco_ciddes_table_remap(uloi_pdoproc_state_t *pdostate);
void uloi_pdoproc_poco_ciddes_table_clear(uloi_pdoproc_state_t *pdostate);
int uloi_pdoproc_poco_ciddes_sorted_map(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes);
int uloi_pdoproc_poco_ciddes_sorted_unmap(uloi_pdoproc_state_t *pdostate, uloi_ciddes_t *ciddes);
void uloi_pdoproc_poco_ciddes_sorted_delete_all(uloi_pdoproc_state_t *pdostate);

int uloi_pdoproc_msg(ULOI_PARAM_coninfo uloi_pdoproc_state_t *pdostate, UL_ARGPTRTYPE ul_msginfo *imsginfo);

int uloi_pdoproc_send_cid(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, uloi_cid_t cid);
int uloi_pdoproc_send_msg(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, uloi_cid_t *cid_list, int cid_list_len);

int uloi_pdoproc_send_gmask(uloi_coninfo_t *coninfo, uloi_pdoproc_state_t *pdostate, uloi_gmask_t gmask);

#ifdef __cplusplus
}
#endif

#endif /*_ULOI_PDOPROC_H*/
