#include <uloi_base.h>
#include <uldy_base.h>

/* FIXME Prototype should be obtained better way */
extern unsigned long ul_dy_buf2sn(const uchar *buf);

int uloi_snchk_fnc(ULOI_PARAM_coninfo void *context)
{
  int ret;
 #ifndef UL_WITHOUT_HANDLE
  ul_dyac_t *ul_dyac = (ul_dyac_t*) context;
  uchar uloi_tmpbuf[4];
 #endif

  if(coninfo->error)
    return -1;

  ret=uloi_i_read(ULOI_ARG_coninfo uloi_tmpbuf, 4);
  if(coninfo->error)
    goto conerr;

  if(ret!=4)
    goto conerr;

  if(ul_dy_buf2sn(uloi_tmpbuf) != ul_dyac->ul_sn) {
    coninfo->error=2;
    return -1;
  }

  return 1;

  conerr:
    if(!coninfo->error)
      coninfo->error=1;
    return -1;
}
