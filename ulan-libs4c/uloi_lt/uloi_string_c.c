#include <uloi_base.h>
#include <uloi_string_c.h>
#include <string.h>


int uloi_string_c_rdfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned char bsize;
  uloi_string_c_des_t *s = (uloi_string_c_des_t *)context;
  int res;

  bsize=strlen(s->text);
  res = uloi_wr_uchar(ULOI_ARG_coninfo &bsize);
  if(res < 0)
    return res;

  if(uloi_o_write(ULOI_ARG_coninfo s->text, bsize)!=bsize){
    coninfo->error=1;
    return -1;
  }
  return 0;
}

int uloi_string_c_wrfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned char bsize, c;
  uloi_string_c_des_t *s = (uloi_string_c_des_t *)context;
  int res, dsize;

  res = uloi_rd_uchar(ULOI_ARG_coninfo &bsize);
  if(res < 0)
    return res;

  dsize=0;
  if(bsize>(s->capacity-1)) {
    dsize=bsize-(s->capacity-1);
    bsize=s->capacity-1;
  }

  if(uloi_i_read(ULOI_ARG_coninfo s->text, bsize)!=bsize){
    coninfo->error=1;
    return -1;
  }

  /* insert null terminator */
  s->text[bsize]=0;

  /* dummy read */
  while(dsize) {
    res = uloi_rd_uchar(ULOI_ARG_coninfo &c);
    if(res < 0)
        return -1;
    dsize--;
  }

  return 0;
}
