#ifndef _ULOI_STRING_C_H
#define _ULOI_STRING_C_H

#include "uloi_base.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct uloi_string_c_des {
  char *text;
  int capacity;
} uloi_string_c_des_t;

int uloi_string_c_rdfnc(ULOI_PARAM_coninfo void *context);
int uloi_string_c_wrfnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_STRING_C_H*/
