#define TEST_ARRAY
#define TEST_PDO
#define TEST_STRING
#define TEST_MULTIDEV
#undef TEST_DYAC
#define TEST_DOITAB

#include <string.h>
#include <uloi_base.h>

/* Do not try to use features which are not configured */
#include "uloi_test_config.h"
#ifndef CONFIG_ULOI_CON_IO_OPS
#undef TEST_PDO
#endif
#ifndef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
#undef TEST_MULTIDEV
#endif

#ifdef TEST_ARRAY
#include <uloi_array.h>
#endif /*TEST_ARRAY*/
#ifdef TEST_PDO
#include <stdio.h>
#include <uloi_array.h>
#include <uloi_pdoproc.h>
#include <uloi_pdomap.h>
#include <uloi_pdoev.h>
#endif /*TEST_PDO*/
#ifdef TEST_STRING
#include <uloi_string_c.h>
#endif /*TEST_STRING*/
#ifdef UL_WITHOUT_HANDLE
#include <system_def.h>
#endif  /*UL_WITHOUT_HANDLE*/
#ifdef TEST_DYAC
#include <uldy_base.h>
#endif /*TEST_DYAC*/
#ifdef TEST_DOITAB
#include <uloi_doitab.h>
#endif /*TEST_DOITAB*/

char ul_idstr[] = ".mt uloi_testlt";

#ifdef TEST_DYAC
uint32_t usn;
UL_DYAC_VAR_LOC ul_dyac_t ul_dyac_global;
ul_dyac_t *ul_dyac=&ul_dyac_global;
char ul_dyac_gst_reply[4+2];
char ul_save_sn(uint32_t usn) { return 0;}
char ul_save_adr(uint8_t uaddr) { return 0;}
#endif /*TEST_DYAC*/

extern const ULOI_CODE uloi_objdes_array_t uloi_objdes_main;

#define I_TEST          230
#define I_TEST1         231
#define I_TEST2         232
#define I_TESTARR       240
#define I_TESTARR_EXEC  241
#define I_TESTSTR       242
#define I_TESTPOCO      250
#define I_TESTEVSET     255

unsigned status_val=0x1234;
int test_val;
int test_val1;
int test_val2;

int status_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_uint_rdfnc(ULOI_ARG_coninfo &status_val);
}

int errclr_wrfnc(ULOI_PARAM_coninfo void *context)
{
  if(status_val<0)
    status_val=0;
  else
    status_val++;
  return 1;
}

int exec_wrfnc(ULOI_PARAM_coninfo void *context)
{
  printf("exec_wrfnc:%d\n",coninfo->array_idx);
  return 1;
}

#ifdef TEST_ARRAY
#define TESTARR_SIZE 10
int testarr_val[TESTARR_SIZE];

uloi_arraydes_t testarr_des = {
  .array_size = TESTARR_SIZE,
  .item_size = sizeof(testarr_val[0]),
  .array_fl = 0,
  .item_rdfnc = uloi_int_rdfnc,
  .item_rdcontext = testarr_val,
  .item_wrfnc = uloi_int_wrfnc,
  .item_wrcontext = testarr_val,
};

#define TESTARR_EXEC_SIZE 10
uloi_arraydes_t testarr_exec_des = {
  .array_size = TESTARR_EXEC_SIZE,
  .item_size = 0,
  .array_fl = 0,
  .item_rdfnc = NULL_CODE,
  .item_rdcontext = NULL,
  .item_wrfnc = exec_wrfnc,
  .item_wrcontext = NULL,
};

#endif /*TEST_ARRAY*/

#ifdef TEST_STRING
#define TESTSTR_SIZE 10
char teststr_val[TESTSTR_SIZE];

uloi_string_c_des_t teststr_des = {
  .text = teststr_val,
  .capacity = sizeof(teststr_val),
};
#endif /*TEST_STRING*/

#ifdef TEST_PDO

#define ULOI_PIOM_ITEMS 100
uchar uloi_piom_items[ULOI_PIOM_ITEMS];

uloi_arraydes_t uloi_poim_des = {
  .array_size = ULOI_PIOM_ITEMS,
  .item_size = sizeof(uloi_piom_items[0]),
  .array_fl = 0,
  .item_rdfnc = uloi_uchar_rdfnc,
  .item_rdcontext = uloi_piom_items,
  .item_wrfnc = uloi_uchar_wrfnc,
  .item_wrcontext = uloi_piom_items,
};

#define ULOI_EVNUM_MAX   10
ULOI_EVARR_DEFINE(uloi_event_array,ULOI_EVNUM_MAX)

#define ULOI_PICO_ITEMS  10
#define ULOI_POCO_ITEMS  10
#define ULOI_PEV2C_ITEMS 10

uloi_ciddes_t * uloi_ciddes_main_pico_items_sorted[ULOI_PICO_ITEMS];
uloi_ciddes_t * uloi_ciddes_main_poco_items_sorted[ULOI_POCO_ITEMS];
uloi_pev2cdes_t * uloi_pev2cdes_items_map[ULOI_EVNUM_MAX];
#ifdef CONFIG_ULOI_WITH_GMASK
uloi_gmask_t uloi_pico_items_gmask[ULOI_PICO_ITEMS];
uloi_gmask_t uloi_pev2c_items_gmask[ULOI_PEV2C_ITEMS];
#endif /* CONFIG_ULOI_WITH_GMASK */
uloi_ciddes_t uloi_ciddes_main_pico_items_table[ULOI_PICO_ITEMS]={
  {.cid = 321,
   .flg = ULOI_CIDFLG_META_OID,
   .meta.oid = I_TEST,
   .meta_len = 2
  },
  {.cid = 322,
   .flg = ULOI_CIDFLG_META_OID,
   .meta.oid = I_TEST1,
   .meta_len = 2
  },
  {.cid = 322,
   .flg = ULOI_CIDFLG_META_OID,
   .meta.oid = I_TEST2,
   .meta_len = 2
  },
};
uloi_ciddes_t uloi_ciddes_main_poco_items_table[ULOI_POCO_ITEMS]={
  {.cid = 400,
   .flg = ULOI_CIDFLG_META_OID,
   .meta.oid = I_TEST,
   .meta_len = 2
  },
  {.cid = 401,
   .flg = ULOI_CIDFLG_META_OID,
   .meta.oid = I_TEST1,
   .meta_len = 2
  },
};
uloi_pev2cdes_t uloi_pev2cdes_main_items_table[ULOI_PEV2C_ITEMS]={
  {.evnum = 1,
   .cid = 400,
   .dadr = 0,
   .flg = 0
   },
  {.evnum = 2,
   .cid = 401,
   .dadr = 0,
   .flg = 0
   },
};

uloi_ciddes_array_t uloi_ciddes_main_pico={
  {
    uloi_ciddes_main_pico_items_sorted,
    0,
    sizeof(uloi_ciddes_main_pico_items_sorted)/sizeof(uloi_ciddes_main_pico_items_sorted[0]),
  },
  {
    uloi_ciddes_main_pico_items_table,
    0,
    sizeof(uloi_ciddes_main_pico_items_table)/sizeof(uloi_ciddes_main_pico_items_table[0]),
  },
#ifdef CONFIG_ULOI_WITH_GMASK
  uloi_pico_items_gmask
#endif /* CONFIG_ULOI_WITH_GMASK */
};
uloi_ciddes_array_t uloi_ciddes_main_poco={
  {
    uloi_ciddes_main_poco_items_sorted,
    0,
    sizeof(uloi_ciddes_main_poco_items_sorted)/sizeof(uloi_ciddes_main_poco_items_sorted[0]),
  },
  {
    uloi_ciddes_main_poco_items_table,
    0,
    sizeof(uloi_ciddes_main_poco_items_table)/sizeof(uloi_ciddes_main_poco_items_table[0]),
  }
};
uloi_pev2cdes_array_t uloi_pev2cdes_main={
  {
    uloi_pev2cdes_main_items_table,
    0,
    sizeof(uloi_pev2cdes_main_items_table)/sizeof(uloi_pev2cdes_main_items_table[0]),
  },
  uloi_pev2cdes_items_map,
  &uloi_event_array,
  NULL,
  NULL,
 #ifdef CONFIG_ULOI_WITH_GMASK
  uloi_pev2c_items_gmask,
 #endif /* CONFIG_ULOI_WITH_GMASK */
};

char uloi_pdostate_main_buff[127];

uloi_pdoproc_state_t uloi_pdostate_main = {
  .objdes_array = &uloi_objdes_main,
  .pico_ciddes_array = &uloi_ciddes_main_pico,
  .poco_ciddes_array = &uloi_ciddes_main_poco,
  .pev2cdes_array = &uloi_pev2cdes_main,
  .pdc2evdes_array = NULL,
  .meta_base = uloi_piom_items,
  .meta_len = sizeof(uloi_piom_items),
  .buff = uloi_pdostate_main_buff,
  .buff_len = sizeof(uloi_pdostate_main_buff),
};

#ifndef STRINGIFY
#define __STRINGIFY(x)     #x                 /* stringify without expanding x */
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */
#endif /*STRINGIFY*/

#define ULOI_GEN_ARR_SIZE(m_size) \
        "[" STRINGIFY(m_size) "]"

#ifdef CONFIG_ULOI_WITH_GMASK
#define ULOI_GMASK_TYPE_DES "u4"
#define ULOI_GET_RDFNC(type) \
        sizeof(type)==4?uloi_ulong_rdfnc:(sizeof(type)==2?uloi_uint_rdfnc:uloi_uchar_rdfnc)
#define ULOI_GET_WRFNC(type) \
        sizeof(type)==4?uloi_ulong_wrfnc:(sizeof(type)==2?uloi_uint_wrfnc:uloi_uchar_wrfnc)

uloi_arraydes_t uloi_pico_gmask_des = {
  .array_size = ULOI_PICO_ITEMS,
  .item_size = sizeof(uloi_pico_items_gmask[0]),
  .array_fl = 0,
  .item_rdfnc = uloi_ulong_rdfnc,
  .item_rdcontext = uloi_pico_items_gmask,
  .item_wrfnc = uloi_ulong_wrfnc,
  .item_wrcontext = uloi_pico_items_gmask,
};
uloi_arraydes_t uloi_pev2c_gmask_des = {
  .array_size = ULOI_PEV2C_ITEMS,
  .item_size = sizeof(uloi_pev2c_items_gmask[0]),
  .array_fl = 0,
  .item_rdfnc = ULOI_GET_RDFNC(uloi_gmask_t),
  .item_rdcontext = uloi_pev2c_items_gmask,
  .item_wrfnc = ULOI_GET_WRFNC(uloi_gmask_t),
  .item_wrcontext = uloi_pev2c_items_gmask,
};
#endif /* CONFpdostate->pev2cdes_array->table.alloc_countIG_ULOI_WITH_GMASK */

int uloi_pmap_clear_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  uloi_pmap_clear(pdostate);
  return 0;
}

int uloi_pmap_save_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_pdoproc_state_t *pdostate = (uloi_pdoproc_state_t*)context;
  /* pico */
  printf("saving into flash pico - data pointer: %p, count: %d, alloc_count:%d\n",
                  pdostate->pico_ciddes_array->table.data,
                  pdostate->pico_ciddes_array->table.count,
                  pdostate->pico_ciddes_array->table.alloc_count);
  /* poco */
  printf("saving into flash poco - data pointer: %p, count: %d, alloc_count:%d\n",
                  pdostate->poco_ciddes_array->table.data,
                  pdostate->poco_ciddes_array->table.count,
                  pdostate->poco_ciddes_array->table.alloc_count);
  /* piom */
  printf("saving into flash piom - data pointer: %p, count: %d\n",
                  pdostate->meta_base,pdostate->meta_len);
  /* pev2c */
  printf("saving into flash pev2c - data pointer: %p, count: %d, alloc_count:%d\n",
                  pdostate->pev2cdes_array->table.data,
                  pdostate->pev2cdes_array->table.count,
                  pdostate->pev2cdes_array->table.alloc_count);
 #ifdef CONFIG_ULOI_WITH_GMASK
  /* pico_gmask */
  printf("saving into flash pico_gmask - data pointer: %p, count: %d\n",
                  pdostate->pico_ciddes_array->gmask,pdostate->pico_ciddes_array->table.alloc_count);
  /* pev2c_gmask */
  printf("saving into flash pev2c_gmask - data pointer: %p, count: %d\n",
                  pdostate->pev2cdes_array->gmask,pdostate->pev2cdes_array->table.alloc_count);
 #endif /* CONFIG_ULOI_WITH_GMASK */
  return 1;
}

uloi_cid_t pdo_send_test_cid_list[]={400};
uloi_con_ulan_t uloi_con_ulan_pdo_send;
uloi_coninfo_t *coninfo_pdo_send=&uloi_con_ulan_pdo_send.con;

int uloi_pdo_send_test_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_con_ulan_set_dadr_cmd(coninfo_pdo_send,0,UL_CMD_PDO);
  uloi_pdoproc_send_msg(coninfo_pdo_send,
                        &uloi_pdostate_main,
                        pdo_send_test_cid_list,
                        sizeof(pdo_send_test_cid_list)/sizeof(pdo_send_test_cid_list[0]));
  return 0;
}

int testevset_wrfnc(ULOI_PARAM_coninfo void *context)
{
  unsigned int val;
  int r;
  r=uloi_uint_wrfnc(ULOI_ARG_coninfo &val);
  uloi_evarr_set_ev(&uloi_event_array,val);
  return r;
}

#endif /* TEST_PDO*/

/* description of input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOII =
ULOI_GENOBJDES_RAW(ULOI_DOII,NULL,NULL_CODE,NULL,uloi_doii_fnc,(void*)&uloi_objdes_main)
/* description of output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOIO =
ULOI_GENOBJDES_RAW(ULOI_DOIO,NULL,NULL_CODE,NULL,uloi_doio_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOII =
ULOI_GENOBJDES_RAW(ULOI_QOII,NULL,NULL_CODE,NULL,uloi_qoii_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOIO =
ULOI_GENOBJDES_RAW(ULOI_QOIO,NULL,NULL_CODE,NULL,uloi_qoio_fnc,(void*)&uloi_objdes_main)
/* object values read request */
const ULOI_CODE uloi_objdes_t uloid_objdes_RDRQ =
ULOI_GENOBJDES_RAW(ULOI_RDRQ,NULL,NULL_CODE,NULL,uloi_rdrq_fnc,(void*)&uloi_objdes_main)
#ifdef TEST_PDO
/* PDO input CID/OID mapping */
ULOI_GENOBJDES(PICO,ULOI_PICO,ULOI_GEN_ARR_SIZE(ULOI_PICO_ITEMS) "pico",uloi_pico_array_rdfnc,(void*)&uloi_pdostate_main,uloi_pico_array_wrfnc,(void*)&uloi_pdostate_main)
ULOI_GENOBJDES(POCO,ULOI_POCO,ULOI_GEN_ARR_SIZE(ULOI_POCO_ITEMS) "poco",uloi_poco_array_rdfnc,(void*)&uloi_pdostate_main,uloi_poco_array_wrfnc,(void*)&uloi_pdostate_main)
ULOI_GENOBJDES(PIOM,ULOI_PIOM,ULOI_GEN_ARR_SIZE(ULOI_PIOM_ITEMS) "u1",uloi_array_rdfnc,(void*)&uloi_poim_des,uloi_array_wrfnc,(void*)&uloi_poim_des)
ULOI_GENOBJDES(PEV2C,ULOI_PEV2C,ULOI_GEN_ARR_SIZE(ULOI_PEV2C_ITEMS) "pev2c",uloi_pev2c_array_rdfnc,(void*)&uloi_pdostate_main,uloi_pev2c_array_wrfnc,(void*)&uloi_pdostate_main)
ULOI_GENOBJDES(PMAP_CLEAR,ULOI_PMAP_CLEAR,"e",NULL_CODE,NULL,&uloi_pmap_clear_wrfnc,(void*)&uloi_pdostate_main)
ULOI_GENOBJDES(PMAP_SAVE,ULOI_PMAP_SAVE,"e",NULL_CODE,NULL,&uloi_pmap_save_wrfnc,(void*)&uloi_pdostate_main)
#ifdef CONFIG_ULOI_WITH_GMASK
ULOI_GENOBJDES(PICO_GMASK,ULOI_PICO_GMASK,ULOI_GEN_ARR_SIZE(ULOI_PICO_ITEMS) ULOI_GMASK_TYPE_DES,uloi_array_rdfnc,(void*)&uloi_pico_gmask_des,uloi_array_wrfnc,(void*)&uloi_pico_gmask_des)
ULOI_GENOBJDES(PEV2C_GMASK,ULOI_PEV2C_GMASK,ULOI_GEN_ARR_SIZE(ULOI_PEV2C_ITEMS) ULOI_GMASK_TYPE_DES,uloi_array_rdfnc,(void*)&uloi_pev2c_gmask_des,uloi_array_wrfnc,(void*)&uloi_pev2c_gmask_des)
#endif /* CONFIG_ULOI_WITH_GMASK */
ULOI_GENOBJDES(TESTPOCO,I_TESTPOCO,"e",NULL_CODE,NULL,&uloi_pdo_send_test_wrfnc,NULL)
ULOI_GENOBJDES(TESTEVSET,I_TESTEVSET,"u2",NULL_CODE,NULL,testevset_wrfnc,NULL)
#endif /* TEST_PDO*/

ULOI_GENOBJDES(STATUS,ULOI_STATUS,"u2",status_rdfnc,&status_val,NULL_CODE,NULL)
ULOI_GENOBJDES(ERRCLR,ULOI_ERRCLR,"e",NULL_CODE,NULL,errclr_wrfnc,&status_val)
ULOI_GENOBJDES(TEST,I_TEST,"s2",uloi_int_rdfnc,&test_val,uloi_int_wrfnc,&test_val)
ULOI_GENOBJDES(TEST1,I_TEST1,"s2",uloi_int_rdfnc,&test_val1,uloi_int_wrfnc,&test_val1)
ULOI_GENOBJDES(TEST2,I_TEST2,"s2",uloi_int_rdfnc,&test_val2,uloi_int_wrfnc,&test_val2)

#ifdef TEST_ARRAY
ULOI_GENOBJDES(TESTARR,I_TESTARR,"[]s2",uloi_array_rdfnc,&testarr_des,uloi_array_wrfnc,&testarr_des)
ULOI_GENOBJDES(TESTARR_EXEC,I_TESTARR_EXEC,"[]e",NULL_CODE,NULL,uloi_array_wrfnc,&testarr_exec_des)
#endif /*TEST_ARRAY*/

#ifdef TEST_STRING
ULOI_GENOBJDES(TESTSTR,I_TESTSTR,"vs",uloi_string_c_rdfnc,&teststr_des,uloi_string_c_wrfnc,&teststr_des)
#endif /*TEST_STRING*/

#ifdef TEST_DOITAB
uloi_doitabdes_t testdoitabdes = {
  .objdes_array=(void*)&uloi_objdes_main
};
const ULOI_CODE uloi_objdes_t uloid_objdes_DOITAB =
ULOI_GENOBJDES_RAW(ULOI_DOITAB,NULL,uloi_doitab_rdfnc,&testdoitabdes,uloi_doitab_fnc,&testdoitabdes)
#endif /*TEST_DOITAB*/

const uloi_objdes_t * ULOI_CODE uloi_objdes_main_items[]={
  &uloid_objdes_DOII,
  &uloid_objdes_DOIO,
  &uloid_objdes_QOII,
  &uloid_objdes_QOIO,
  &uloid_objdes_RDRQ,
 #ifdef TEST_DOITAB
  &uloid_objdes_DOITAB,
 #endif /*TEST_DOITAB*/
  &uloid_objdes_STATUS,
  &uloid_objdes_ERRCLR,
 #ifdef TEST_PDO
  &uloid_objdes_PICO,
  &uloid_objdes_POCO,
  &uloid_objdes_PIOM,
  &uloid_objdes_PEV2C,
  &uloid_objdes_PMAP_CLEAR,
  &uloid_objdes_PMAP_SAVE,
 #ifdef CONFIG_ULOI_WITH_GMASK
  &uloid_objdes_PICO_GMASK,
  &uloid_objdes_PEV2C_GMASK,
 #endif /* CONFIG_ULOI_WITH_GMASK */
 #endif /*TEST_PDO*/
  &uloid_objdes_TEST,
  &uloid_objdes_TEST1,
  &uloid_objdes_TEST2,
 #ifdef TEST_ARRAY
  &uloid_objdes_TESTARR,
  &uloid_objdes_TESTARR_EXEC,
 #endif /*TEST_ARRAY*/
 #ifdef TEST_STRING
  &uloid_objdes_TESTSTR,
 #endif /*TEST_STRING*/
 #ifdef TEST_PDO
  &uloid_objdes_TESTPOCO,
  &uloid_objdes_TESTEVSET,
 #endif /*TEST_PDO*/
};

const ULOI_CODE uloi_objdes_array_t uloi_objdes_main={
  {
    uloi_objdes_main_items,
    sizeof(uloi_objdes_main_items)/sizeof(uloi_objdes_main_items[0]),
    -1
  }
};

uloi_con_ulan_t uloi_con_ulan_global;

int main()
{
 #ifndef UL_WITHOUT_HANDLE
  uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
  ul_msginfo msginfo;
  ul_fd_t ul_fd;
  ul_fd_t ul_fd1;
 #endif  /*UL_WITHOUT_HANDLE*/

 #ifdef UL_WITHOUT_HANDLE
  ul_drv_set_adr(3);
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
  ul_drv_init();
  ul_fd=ul_open(0,0);
 #else  /*UL_WITHOUT_HANDLE*/
  ul_fd=ul_open(NULL,NULL);
  if(ul_fd==UL_FD_INVALID)
    return 1;
  ul_fd1=ul_open(NULL,NULL);
  if(ul_fd1==UL_FD_INVALID){
    ul_close(ul_fd);
    return 1;
  }
  int res;
 #ifdef TEST_MULTIDEV
  if(ul_setsubdev(ul_fd, 1)<0)
    return 1;
  if(ul_setsubdev(ul_fd1, 1)<0)
    return 1;
  if(ul_setmyadr(ul_fd, 12)<0)
    return 1;
 #endif /*TEST_MULTIDEV*/
  if((res=ul_setidstr(ul_fd,ul_idstr))<0) {
    perror("ul_setidstr");
    return 1;
  }
 #endif  /*UL_WITHOUT_HANDLE*/

  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);
 #ifdef TEST_PDO
  uloi_con_ulan_set_cmd_fd(coninfo_pdo_send, 0, ul_fd, ul_fd1);
 #endif

 #ifndef UL_WITHOUT_HANDLE
  memset(&msginfo, 0, sizeof(msginfo));
  msginfo.cmd=uloi_con_ulan_cmd(coninfo);
  ul_addfilt(uloi_con_ulan_rdfd(coninfo), &msginfo);
 #ifdef TEST_PDO
  msginfo.cmd=UL_CMD_PDO;
  ul_addfilt(uloi_con_ulan_rdfd(coninfo), &msginfo);
  uloi_pdoproc_pico_ciddes_table_remap(&uloi_pdostate_main);
  uloi_pdoproc_poco_ciddes_table_remap(&uloi_pdostate_main);
  uloi_pev2c_table_remap(uloi_pdostate_main.pev2cdes_array);
  uloi_evarr_clear_all(uloi_pdostate_main.pev2cdes_array->evarr);
 #endif /*TEST_PDO*/
 #endif  /*UL_WITHOUT_HANDLE*/

#ifdef TEST_DYAC
  /* uLan dyac init */
  uldy_init(ul_dyac, ul_fd, ul_save_sn,ul_save_adr,ul_idstr,usn);

  memset(&msginfo, 0, sizeof(msginfo));
  msginfo.cmd=UL_CMD_NCS;
  ul_addfilt(ul_fd, &msginfo);

  /*ul_opdata_add_iac(ul_fd,UL_CMD_GST,UL_IAC_OP_SND,ul_iac_call_gst,ul_dyac_gst_reply,sizeof(ul_dyac_gst_reply),0,ul_dyac);*/
#endif /*TEST_DYAC*/


  while(1){

    if((ul_inepoll(uloi_con_ulan_rdfd(coninfo))>0)||(ul_fd_wait(uloi_con_ulan_rdfd(coninfo),100)>0)){
     #ifndef TEST_PDO
      uloi_process_msg(ULOI_ARG_coninfo &uloi_objdes_main, NULL);
     #else /*TEST_PDO*/
      int free_fl = 0;
      do {
        if (ul_acceptmsg(uloi_con_ulan_rdfd(coninfo), &msginfo)<0)
          break;        /* No mesage reported - break */
        free_fl = 1;

        if (msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))
          break;        /* Reported message informs about fault or carried out processing */

        if(msginfo.cmd==uloi_con_ulan_cmd(coninfo)){
          if (uloi_process_msg(ULOI_ARG_coninfo (uloi_objdes_array_t*)&uloi_objdes_main, &msginfo)>=0)
            free_fl = 0;
          break;
        }

        if(msginfo.cmd==UL_CMD_PDO){
          if (uloi_pdoproc_msg(ULOI_ARG_coninfo &uloi_pdostate_main, &msginfo)>=0)
            free_fl = 0;
          break;
        }

       #ifdef TEST_DYAC
        if (msginfo.cmd==UL_CMD_NCS){
          if (uldy_process_msg(ULDY_ARG_ul_dyac &msginfo)>=0)
            free_fl = 0;
          break;
        }
       #endif /*TEST_DYAC*/
      } while(0);

      if (free_fl)
        ul_freemsg(uloi_con_ulan_rdfd(coninfo));

     #endif /*TEST_PDO*/
    }

   #ifdef TEST_PDO
    uloi_pev2c_procev(uloi_pdostate_main.pev2cdes_array);
    uloi_pev2c_proclocal(&uloi_pdostate_main);
    uloi_pev2c_procsend(coninfo_pdo_send, &uloi_pdostate_main, 300);
   #endif /*TEST_PDO*/
  }
}
