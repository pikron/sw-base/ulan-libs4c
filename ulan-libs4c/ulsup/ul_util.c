#include "stdio.h"
#include "ulan.h"
#include "ul_util.h"

int print_msg(int with_data)
{
  int ret;
  char *state;
  unsigned i;
  uchar xdata uc;

  ret=ul_acceptmsg(ul_fd,&msginfo);
  if(ret<0) 
  { printf("spy_messages : problem to accept message\n");
    return ret;
  };
  
  if(msginfo.flg&UL_BFL_FAIL) state="FAIL";
  else if(msginfo.flg&UL_BFL_PROC) {state="PROC"; with_data=0;}
  else state="REC ";
  printf("%s:(d:%d,s:%d,c:0x%X,",
         state,(unsigned)msginfo.dadr,(unsigned)msginfo.sadr,(unsigned)msginfo.cmd);
  printf("f:0x%X,l:%d)",(unsigned)msginfo.flg,(unsigned)msginfo.len);
  if(with_data) for(i=0;i<msginfo.len;i++){ 
    ret=ul_read(ul_fd,&uc,1); printf(" 0x%X",(unsigned)uc);
    if(ret!=1) {printf("\nul_read returned %d\n",ret); break;}
    /*printf("%d %d\n",i,msginfo.len);*/
  };
  while((msginfo.flg&UL_BFL_TAIL)&&(ret>=0))
  { ret=ul_actailmsg(ul_fd,&msginfo);
    if(msginfo.flg&UL_BFL_FAIL) state="fail";
    else if(msginfo.flg&UL_BFL_PROC) state="proc";
    else state="rec ";
    printf(" %s:(d:%d,s:%d,c:0x%X,",
           state,(unsigned)msginfo.dadr,(unsigned)msginfo.sadr,(unsigned)msginfo.cmd);
    printf("f:0x%X,l:%d)",(unsigned)msginfo.flg,(unsigned)msginfo.len);
    if(with_data) for(i=0;i<msginfo.len;i++)
      { ul_read(ul_fd,&uc,1); printf(" 0x%X",(unsigned)uc); };
  };
  printf("\n");
  ul_freemsg(ul_fd);
  return 0;
}
