#ifndef _UL_UTIL_H
#define _UL_UTIL_H

#include "ulan.h"

#ifdef __cplusplus
/*extern "C" {*/
#endif

int print_msg(int with_data);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _UL_UTIL_H */
